import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/SplashViewController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/local_notification_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("message recieved");
  print(message);
  LocalNotifications().notification(
      '${message.notification!.title}', '${message.notification!.body}');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  LocalNotifications().init();
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  // String? token = await FirebaseMessaging.instance.getToken();
  // print('FCM TOKEN' + token.toString());
  FirebaseMessaging.onMessage.listen((RemoteMessage event) async {
    print("message recieved");
    print(event);

    LocalNotifications().notification(
        '${event.notification!.title}', '${event.notification!.body}');
  });
  FirebaseMessaging.onMessageOpenedApp.listen((message) async {
    print("message recieved");
    print(message.data);

    LocalNotifications().notification(
        '${message.notification!.title}', '${message.notification!.body}');
  });

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarBrightness: Brightness.light,
    statusBarColor: AppColor.transparent,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarDividerColor: AppColor.transparent,
  ));
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'D2H',
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: new ScrollBehavior(),
          child: child!,
        );
      },
      theme: ThemeData(fontFamily: 'Poppins'),
      home: SplashViewController(),
    );
  }
}
