import 'dart:convert';

import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<CartDataModel> getCartDataService({
  required BuildContext context,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_CART + "${user["customerid"]}", context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get cart Data");
    print(jsonDecode(response.body)["data"]);

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Cart")) == 0) {
      print("Cart Inserted to SQL");
      await DBProvider.db.createCart(response.body.toString());
      print("SQLITE");
      print(await DBProvider.db.getCart());
    } else {
      print("Cart Updated to SQL");
      await DBProvider.db.updateCart(response.body.toString());
      print("SQLITE");
      print(await DBProvider.db.getCart());
    }

    return CartDataModel.fromJson(jsonDecode(response.body)["data"]);
    // return ProductModel.jsonToProductsList(jsonDecode(response.body)["data"]["cartdetail"]);
  } else {
    return CartDataModel(cartList: [], minimumQTY: 0);
  }
}

Future<bool> addToCartService({
  required BuildContext context,
  required List<CartModel> cartsProduct,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = CartModel.cartProductsListToJSON(cartsProduct);
  // {
  //   "customerid": "${user["customerid"]}",
  //   "id": cartID,
  //   "productid": "$productid",
  //   "qty": quantity,
  // };

  var response = await ApiCall().postListRequestHeader(
      apiUrl: ADD_TO_CART, bodyParameter: data, context: context);

  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Added to Cart");
    return true;
  } else if (jsonDecode(response.body)["status"] == 403) {
    ShowMessage().showErrorMessage(context, "Already in Cart");
    return false;
  } else {
    return false;
  }
}

Future<bool> removeFromCartService({
  required BuildContext context,
  required int cardID,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    // "customerid": "${user["customerid"]}",
    // "productid": "$productId",
  };

  var response = await ApiCall().deleteRequestHeader(
    apiUrl: REMOVE_FROM_CART + "$cardID",
    bodyParameter: data,
    context: context,
  );
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Deleted From Cart");
    return true;
  } else {
    return false;
  }
}
