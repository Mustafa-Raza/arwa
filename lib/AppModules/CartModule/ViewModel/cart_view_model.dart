import 'dart:convert';

import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/CartModule/Services/cart_services.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/Services/product_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class CartViewModel extends GetxController {
  var getCartData = CartDataModel(cartList: [], minimumQTY: 0).obs;
  RxDouble totalPrice = 0.0.obs;
  RxDouble discountedPrice = 0.0.obs;
  RxDouble totalDiscount = 0.0.obs;
  RxInt minimumProductCount = 3.obs;
  RxInt totalProductCount = 0.obs;
  RxBool changeInCartModel = false.obs;
  var cartProductList = [].obs;
  var getCartDataSaved = CartDataModel(cartList: [], minimumQTY: 0).obs;

  updatePrice({required double price, required bool isAdd}) {
    if (isAdd) {
      totalPrice.value = totalPrice.value + price;
    } else {
      totalPrice.value = totalPrice.value - price;
    }
  }

  // updateCartChang(bool value) {
  //   changeInCartModel.value = value;
  // }

  // updateProductQty({required int quantity, required int index}) {
  //   getCartData.value[index].cartQuantity = quantity;
  // }

  // updatePriceOnDeleteCart({
  //   required int index,
  // }) {
  //   totalPrice.value = totalPrice.value -
  //       (getCartData.value[index].baseprice *
  //           getCartData.value[index].cartQuantity);
  // }

  // updateTotalPriceAndDiscount({required int index, required bool isAdd}) {
  //   if (isAdd) {
  //     totalPrice.value = totalPrice.value + getCartData.value[index].baseprice;
  //     totalDiscount.value =
  //         totalDiscount.value + getCartData.value[index].discountprice;
  //   } else {
  //     totalProductCount.value =
  //         (totalProductCount.value - getCartData.value[index].cartQuantity)
  //             .toInt();
  //     totalPrice.value = totalPrice.value - getCartData.value[index].baseprice;
  //     totalDiscount.value =
  //         totalDiscount.value - getCartData.value[index].discountprice;
  //   }
  // }

  Future<CartDataModel> getCartProductsData(BuildContext context) async {
    // List<ProductModel> productsModel = [];
    CartDataModel productsModel = CartDataModel(cartList: [], minimumQTY: 0);

    if ((await DBProvider.db.checkDataExistenceByLength("Cart")) == 0) {
      productsModel = await getCartDataService(
        context: context,
      );
    } else {
      print("Cart is fetched in from local");
      //
      var localData = await DBProvider.db.getCart();
      // String value=jsonEncode(localData[0]["home"]);
      print(jsonDecode(localData[0]["cart"])["data"]["cartdetail"]);

      // productsModel = ProductModel.jsonToProductsList(
      //     jsonDecode(localData[0]["cart"])["data"]["cartdetail"]);
      productsModel =
          CartDataModel.fromJson(jsonDecode(localData[0]["cart"])["data"]);

      getCartDataService(
        context: context,
      ).then((value) => {
            print("Cart is fetched in background"),
            getCartData.value = value,
            getCartDataSaved.value = value,
          });
    }

    getCartData.value = productsModel;
    getCartDataSaved.value = productsModel;
    setPriceAndDiscount();
    return productsModel;
  }

  Future<CartDataModel> getUpdatedCartProductsData(BuildContext context) async {
    CartDataModel productsModel = CartDataModel(cartList: [], minimumQTY: 0);

    productsModel = await getCartDataService(
      context: context,
    );
    getCartData.value = productsModel;
    getCartDataSaved.value = productsModel;
    setPriceAndDiscount();
    return productsModel;
  }

  onSaveCart(
      {required BuildContext context, required Function callBack}) async {
    showLoadingIndicator(context: context);
    var user = await DatabaseHandler().getCurrentUser();
    List<CartModel> cartList = CartModel.productsToCartProductsListToJSON(
        getCartData.value.cartList, "${user["customerid"]}");
    print(cartList);

    bool result =
        await addToCartService(context: context, cartsProduct: cartList);

    hideOpenDialog(context: context);
    if (result) {
      await getUpdatedCartProductsData(context);
    }
    callBack(result);
  }

  setPriceAndDiscount() {
    totalPrice.value = 0.0;
    totalDiscount.value = 0.0;
    totalProductCount.value = 0;
    // discountedPrice.value = 0.0;
    for (int i = 0; i < getCartData.value.cartList.length; i++) {
      totalProductCount.value =
          (totalProductCount.value + getCartData.value.cartList[i].cartQuantity)
              .toInt();
      totalPrice.value = totalPrice.value +
          (getCartData.value.cartList[i].baseprice *
              getCartData.value.cartList[i].cartQuantity);
      totalDiscount.value = totalDiscount.value +
          (getCartData.value.cartList[i].discountprice *
              getCartData.value.cartList[i].cartQuantity);
      // discountedPrice.value = totalPrice.value - totalDiscount.value;
    }
  }
}
