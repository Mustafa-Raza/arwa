import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/CartModule/Services/cart_services.dart';
import 'package:arwa/AppModules/CartModule/ViewModel/cart_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/Components/ProductCountTile.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class CartListTile extends StatefulWidget {
  final ProductModel productModel;
  final int index;

  CartListTile({Key? key, required this.index, required this.productModel})
      : super(key: key);

  @override
  _CartListTileState createState() => _CartListTileState();
}

class _CartListTileState extends State<CartListTile> {
  final cartVM = Get.put(CartViewModel());
  bool isDeleting = false;
  bool isSavingCart = false;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      // Specify a key if the Slidable is dismissible.
      // key: const ValueKey(0),

      // The start action pane is the one at the left or the top side.
      startActionPane: ActionPane(
        // A motion is a widget used to control how the pane animates.
        motion: const ScrollMotion(),

        // A pane can dismiss the Slidable.
        // dismissible: DismissiblePane(onDismissed: () {}),
        extentRatio: 0.3,
        closeThreshold: 0.5,
        openThreshold: 0.1,

        // All actions are defined in the children parameter.
        children: [
          InkWell(
            child: Container(
              color: Color(0xFF75C650),
              width: 93,
              child: !isSavingCart
                  ? Center(
                      child: Image(
                        image: AssetImage("assets/cart/save.png"),
                        width: 23,
                        height: 23,
                      ),
                    )
                  : SpinKitView(
                      themeIsDark: false,
                    ),
            ),
            onTap: () async {
              setState(() {
                isSavingCart = !isSavingCart;
              });
              print("Add to Cart");
              print(widget.productModel.cartid);
              print(widget.productModel.productid);
              print(widget.productModel.cartQuantity);
              var user = await DatabaseHandler().getCurrentUser();
              bool result = await addToCartService(
                context: context,
                cartsProduct: [
                  CartModel(
                      customerid: "${user["customerid"]}",
                      cartId: widget.productModel.cartid,
                      productid: widget.productModel.productid,
                      qty: widget.productModel.cartQuantity)
                ],
              );

              setState(() {
                isSavingCart = !isSavingCart;
              });
            },
          ),
        ],
      ),
// The end action pane is the one at the right or the bottom side.

      endActionPane: ActionPane(
        motion: ScrollMotion(),
        extentRatio: 0.3,
        closeThreshold: 0.5,
        openThreshold: 0.1,
        children: [
          InkWell(
            child: Container(
              color: Color(0xFFFF2D5F),
              width: 93,
              child: !isDeleting
                  ? Center(
                      child: Image(
                        image: AssetImage("assets/cart/Trash-alt.png"),
                        width: 23,
                        height: 23,
                      ),
                    )
                  : SpinKitView(
                      themeIsDark: false,
                    ),
            ),
            onTap: () async {
              setState(() {
                isDeleting = !isDeleting;
              });

              var result = await removeFromCartService(
                  context: context,
                  cardID: cartVM.getCartData.value.cartList[widget.index].cartid);
              setState(() {
                isDeleting = !isDeleting;
              });
              if (result) {
                await cartVM.getUpdatedCartProductsData(context);
              }
            },
          ),
        ],
      ),
      //
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Container(
          height: 100,
          width: AppConfig(context).width,
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 44,
                    width: 44,
                    decoration: BoxDecoration(
                      color: Color(0xFFF2F9FF),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: cacheImageView(
                        circlularPadding: 0,
                        image: widget.productModel.image,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.customText(
                            overFlowText: TextOverflow.ellipsis,
                            title: widget.productModel.productdesc +
                                " " +
                                widget.productModel.packsize,
                            size: 14,
                            fontWeight: FontWeights.medium,
                            color: AppColor.DarkText),
                        // AppText.customText(
                        //     overFlowText: TextOverflow.ellipsis,
                        //     title:
                        //     widget.productModel.packsize ,
                        //     size: 12,
                        //     fontWeight: FontWeights.medium,
                        //     color: AppColor.DarkText),
                        // if( widget.index!=0)
                        SizedBox(
                          height: 5,
                        ),

                        AppText.customText(
                            title:
                                "Product ID: ${widget.productModel.productid}",
                            size: 12,
                            fontWeight: FontWeights.regular,
                            color: AppColor.textGREY),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 66,
                  ),
                  ProductCountTile(
                    plusImage: "assets/cart/minusCart.png",
                    onDelete: () async {
                      showLoadingIndicator(context: context);

                      var result = await removeFromCartService(
                          context: context,
                          cardID:
                              cartVM.getCartData.value.cartList[widget.index].cartid);
                      hideOpenDialog(context: context);
                      if (result) {
                        await cartVM.getUpdatedCartProductsData(context);
                        // await cartVM.getCartProductsData(context);
                      }
                    },
                    value: cartVM.getCartData.value.cartList[widget.index].cartQuantity,
                    isCart: true,
                    onChange: (value) {
                      onChangeProductCount(value);
                    },
                  ),
                  Spacer(),
                  AppText.customText(
                      title: "AED ${widget.productModel.baseprice}",
                      size: 14,
                      fontWeight: FontWeights.medium,
                      color: AppColor.DarkText),

                  // SizedBox(
                  //   width: 20,
                  // ),
                ],
              ),
              Spacer(),
              if (widget.index == 0)
                Padding(
                  padding: const EdgeInsets.only(left: 66, right: 20),
                  child: Divider(
                    height: 1,
                    color: Colors.grey.shade300,
                    thickness: 1,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  onChangeProductCount(int value) {
    // if (value != cartVM.getCartData.value[widget.index].cartQuantity)
    if (value > cartVM.getCartData.value.cartList[widget.index].cartQuantity) {
      setState(() {
        cartVM.getCartData.value.cartList[widget.index].cartQuantity = value;
      });
      print(cartVM.getCartData.value.cartList[widget.index].cartid);
      print(cartVM.getCartData.value.cartList[widget.index].cartQuantity);
      cartVM.updatePrice(
          isAdd: true, price: cartVM.getCartData.value.cartList[widget.index].baseprice);
    } else {
      setState(() {
        cartVM.getCartData.value.cartList[widget.index].cartQuantity = value;
      });
      print(cartVM.getCartData.value.cartList[widget.index].cartid);
      print(cartVM.getCartData.value.cartList[widget.index].cartQuantity);
      cartVM.updatePrice(
          isAdd: false,
          price: cartVM.getCartData.value.cartList[widget.index].baseprice);
    }
    // if (value != cartVM.getCartData.value[widget.index].cartQuantity)
    //   cartVM.updateTotalPriceAndDiscount(
    //       index: widget.index,
    //       isAdd: value > cartVM.getCartData.value[widget.index].cartQuantity);
    // cartVM.updateProductQty(quantity: value, index: widget.index);
  }
}
