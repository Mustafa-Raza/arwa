import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

onChangeDiscardPop({required BuildContext context,required Function callBack}) async {
  final size = MediaQuery.of(context).size;
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Container(
            height: 180,
            decoration: BoxDecoration(
              color: AppColor.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  AppText.customText(
                      title: "Are you sure?",
                      color: AppColor.primaryColor,
                      size: 20,
                      fontWeight: FontWeights.bold),
                  SizedBox(
                    height: 5,
                  ),
                  AppText.customText(
                      alignment: TextAlign.center,
                      title: "Do you want to save Cart Changing!",
                      color: AppColor.DarkText,
                      size: 14,
                      fontWeight: FontWeights.bold),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          callBack(false);

                        },
                        child: Container(
                          height: 45,
                          width: size.height / 7,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "No",
                                color: AppColor.primaryColor,
                                size: 17,
                                fontWeight: FontWeights.bold),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          callBack(true);
                        },
                        child: Container(
                          height: 45,
                          width: size.height / 7,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColor.primaryColor,
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "Yes",
                                color: AppColor.white,
                                size: 14,
                                fontWeight: FontWeights.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
