import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/CartModule/View/discard_changing_alert.dart';
import 'package:arwa/AppModules/CartModule/ViewModel/cart_view_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/OrderModule/ViewController/PaymentAndAddressViewController.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:arwa/AppModules/CartModule/View/CartListTile.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MyCartViewController extends StatefulWidget {
  const MyCartViewController({Key? key}) : super(key: key);

  @override
  _MyCartViewControllerState createState() => _MyCartViewControllerState();
}

class _MyCartViewControllerState extends State<MyCartViewController> {
  final categoryController = Get.put(HomeCategoryController());
  final cartVM = Get.put(CartViewModel());
  Future<CartDataModel>? getCartProducts;

  // Future<List<ProductModel>>? getCartProducts;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCartProducts = cartVM.getCartProductsData(context);
  }

  onBackNavigate() async {
    cartVM.getCartProductsData(context);
    Navigator.pop(context);
    // if (cartVM.changeInCartModel.value) {
    //   // if (cartVM.changeInCartModel.value) {
    //
    //   print(cartVM.changeInCartModel.value);
    //
    //   onChangeDiscardPop(
    //       context: context,
    //       callBack: (value) {
    //         if (value) {
    //           cartVM.onSaveCart(
    //               context: context,
    //               callBack: (value) {
    //                 if (value) Navigator.pop(context);
    //               });
    //         } else {
    //           Navigator.pop(context);
    //         }
    //       });
    // } else {
    //   print("No change");
    //   Navigator.pop(context);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: primaryButton(
              buttonTitle: "Review Payment & Address",
              callback: () {
                int count = 0;
                print("Review Payment & Address");
                print(cartVM.minimumProductCount.value);
                print(cartVM.totalProductCount.value);
                if (cartVM.getCartData.value.cartList.isNotEmpty) {
                  for (int i = 0;
                      i < cartVM.getCartData.value.cartList.length;
                      i++) {
                    count = count +
                        cartVM.getCartData.value.cartList[i].cartQuantity;
                  }
                  if (count >= cartVM.getCartData.value.minimumQTY) {
                    cartVM.onSaveCart(
                        context: context,
                        callBack: (value) {
                          List<CartModel> cartList =
                              CartModel.productsToCartProductsListToJSON(
                                  cartVM.getCartData.value.cartList, "");

                          Get.to(
                              () => PaymentAndAddressViewController(
                                    cartList: cartList,
                                    totalBill:
                                        "${cartVM.totalPrice.value - cartVM.totalDiscount.value}",
                                  ),
                              transition: Transition.rightToLeft);
                        });
                  } else {
                    ShowMessage().showErrorMessage(context,
                        "Sorry! Product Quantity is lower than Minimum Required ");
                  }
                } else {
                  ShowMessage().showErrorMessage(context, "No Product in Cart");
                }
              },
              width: AppConfig(context).width - 60,
              btnColor: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              txtColor: AppColor.white,
              height: 50,
              fontweight: FontWeights.medium,
            ),
          ),
        ),
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: WillPopScope(
            onWillPop: () async {
              onBackNavigate();
              return false;
            },
            child: Container(
              width: AppConfig(context).width,
              height: AppConfig(context).height,
              child: Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                  ),
                  width: AppConfig(context).width,
                  // height: AppConfig(context).height -50,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: InnerAppBar(
                          callback: () {

                            onBackNavigate();
                          },
                          defaultBackNavigation: false,
                          title: "Cart",
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              FutureBuilder<CartDataModel>(
                                  // future: productVM.getProductsData(context,widget.brandCode),
                                  future: getCartProducts,
                                  builder: (context, snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      return Center(
                                        child: SizedBox(
                                            width:
                                                AppConfig(context).width - 40,
                                            height: AppConfig(context).height,
                                            child: FavShimmerView()),
                                      );
                                    }
                                    return Obx(() => cartVM
                                            .getCartData.value.cartList.isEmpty
                                        ? Center(
                                            child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: AppText.customText(
                                                title: "No product in Cart",
                                                color: AppColor.greyText),
                                          ))
                                        : SizedBox(
                                            // height: 300,
                                            child: ListView.builder(
                                              itemCount: cartVM.getCartData
                                                  .value.cartList.length,
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          20, 0, 20, 23),
                                                  child: CartListTile(
                                                    index: index,
                                                    productModel: cartVM
                                                        .getCartData
                                                        .value
                                                        .cartList[index],
                                                  ),
                                                );
                                              },
                                            ),
                                          ));
                                  }),
                              SizedBox(
                                height: 10,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: DottedLine(
                                  direction: Axis.horizontal,
                                  lineLength: double.infinity,
                                  lineThickness: 1.0,
                                  dashLength: 5.0,
                                  dashColor: Colors.grey.shade300,
                                  dashRadius: 0.0,
                                  dashGapLength: 5.0,
                                  dashGapColor: Colors.transparent,
                                  dashGapRadius: 0.0,
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    AppText.customText(
                                        title: "Sub Total",
                                        size: 14,
                                        fontWeight: FontWeights.medium,
                                        color: AppColor.textGREY),
                                    Spacer(),
                                    Obx(
                                      () => AppText.customText(
                                          title:
                                              "AED ${double.parse((cartVM.totalPrice.value).toStringAsFixed(2))}",
                                          size: 14,
                                          fontWeight: FontWeights.medium,
                                          color: AppColor.DarkText),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    AppText.customText(
                                        title: "Delivery Fee",
                                        size: 14,
                                        fontWeight: FontWeights.medium,
                                        color: AppColor.textGREY),
                                    Spacer(),
                                    AppText.customText(
                                        title: "AED 0.0",
                                        size: 14,
                                        fontWeight: FontWeights.medium,
                                        color: AppColor.DarkText),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    AppText.customText(
                                        title: "Discount",
                                        size: 14,
                                        fontWeight: FontWeights.medium,
                                        color: AppColor.textGREY),
                                    Spacer(),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Color(0xFFF2F9FF),
                                        borderRadius: BorderRadius.circular(13),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 3),
                                        child: Obx(
                                          () => AppText.customText(
                                            title:
                                                "-AED ${double.parse((cartVM.totalDiscount.value).toStringAsFixed(2))}",
                                            size: 14,
                                            fontWeight: FontWeights.medium,
                                            color: Color(int.parse(
                                                "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    AppText.customText(
                                        title: "Total",
                                        size: 16,
                                        fontWeight: FontWeights.semi_bold,
                                        color: AppColor.DarkText),
                                    AppText.customText(
                                        title: "(incl.VAT)",
                                        size: 12,
                                        fontWeight: FontWeights.medium,
                                        color: AppColor.textGREY),
                                    Spacer(),
                                    Obx(
                                      () => AppText.customText(
                                          title:
                                              "AED ${double.parse((cartVM.totalPrice.value - cartVM.totalDiscount.value).toStringAsFixed(2))}",
                                          size: 16,
                                          fontWeight: FontWeights.semi_bold,
                                          color: AppColor.DarkText),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    Image(
                                      image:
                                          AssetImage("assets/cart/voucher.png"),
                                      height: 17,
                                      width: 17,
                                      color: Color(int.parse(
                                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    AppText.customText(
                                        title: "Apply Voucher Code",
                                        size: 12,
                                        fontWeight: FontWeights.medium,
                                        color: Color(int.parse(
                                            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: DottedLine(
                                  direction: Axis.horizontal,
                                  lineLength: double.infinity,
                                  lineThickness: 1.0,
                                  dashLength: 5.0,
                                  dashColor: Colors.grey.shade300,
                                  dashRadius: 0.0,
                                  dashGapLength: 5.0,
                                  dashGapColor: Colors.transparent,
                                  dashGapRadius: 0.0,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Row(
                                  children: [
                                    AppText.customText(
                                        title:
                                            "By completing this order, I agree to all ",
                                        size: 10,
                                        fontWeight: FontWeights.semi_bold,
                                        color: AppColor.textGREY),
                                    AppText.customText(
                                        title: "terms & conditions.",
                                        size: 10,
                                        fontWeight: FontWeights.medium,
                                        color: Color(int.parse(
                                            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
