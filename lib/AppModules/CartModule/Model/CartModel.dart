import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';

class CartDataModel {
  CartDataModel({
  required this.cartList,required this.minimumQTY,
  });

  List<ProductModel> cartList;
  int minimumQTY;


  factory CartDataModel.fromJson(Map<String, dynamic> json) =>
      CartDataModel(
        cartList:  ProductModel.jsonToProductsList(json["cartdetail"]),
         minimumQTY : json["minimum"],

      );

}
class CartModel {
  CartModel({
    this.customerid = "",
    this.cartId = 0,
    required this.productid,
    required this.qty,
  });

  String customerid;
  int cartId;
  String productid;
  int qty;

  factory CartModel.fromJson(Map<String, dynamic> json) =>
      CartModel(
        customerid: json["customerid"],
        cartId: json["id"],
        productid: json["productid"],
        qty: json["qty"],
      );

  static List<CartModel> jsonToCartProductsList(List<dynamic> emote) =>
      emote.map<CartModel>((item) => CartModel.fromJson(item)).toList();

  static List<Map<String, dynamic>> cartProductsListToJSON(
      List<CartModel> products) =>
      products.map<Map<String, dynamic>>((item) => cartToJson(item)).toList();

  static List<CartModel> productsToCartProductsListToJSON(
      List<dynamic> products, String customerID) =>
      products
          .map<CartModel>((item) => productToJsonCart(item, customerID))
          .toList();
}

Map<String, dynamic> cartToJson(CartModel cart) =>
    {
      if (cart.customerid != "") "customerid": cart.customerid,
      if (cart.customerid != "") "id": cart.cartId,
      "productid": cart.productid,
      "qty": cart.qty,
    };

CartModel productToJsonCart(ProductModel product, String customerID) =>
    CartModel(
        customerid: customerID,
        cartId: product.cartid,
        productid: product.productid,
        qty: product.cartQuantity);
