import 'package:geocoding/geocoding.dart';
Future<SocialLocation>   getLocation(double lat,double lang)async{
  List<Placemark> placemarks = await placemarkFromCoordinates(lat, lang);
  Placemark place = placemarks[0];
  // print(placemarks);

  // String address=place.country.toString();
  // print(place.country.toString());
  // print(place.locality.toString());
  return SocialLocation(country: place.country.toString(), city: place.locality.toString());
}
class SocialLocation{
  String country;
  String city;
  SocialLocation({required this.country,required this.city});
}