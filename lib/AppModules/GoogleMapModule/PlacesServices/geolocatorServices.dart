import 'package:arwa/AppModules/GoogleMapModule/PlacesServices/GoogleApiServiceModel.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'package:http/http.dart' as http;
import 'dart:convert' as convert;


class PlacesService {
  final key = 'AIzaSyBKY5QU0Fzrj6ZsZoNAmQy7Ia_8NRDbAPs';

  Future<List<PlaceSearch>> getAutocomplete(String search) async {
    var se = search;
    var url =
    // 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$search&types=(geocode)&key=$key';
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$search&key=$key';
    var response = await http.get(Uri.parse(url));
    print("hello      .................................");
    print(response.body);
    var json = convert.jsonDecode(response.body);
    print(json);
    var jsonResults = json['predictions'] as List;
    print(jsonResults);
    return jsonResults.map((place) => PlaceSearch.fromJson(place)).toList();
  }

  Future<Place> getPlace(String placeId) async {
    var url =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$key';
    var response = await http.get(Uri.parse(url));
    var json = convert.jsonDecode(response.body);
    var jsonResult = json['result'] as Map<String,dynamic>;
    return Place.fromJson(jsonResult);
  }

  Future<List<Place>> getPlaces(double lat, double lng,String placeType) async {
    var url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?location=$lat,$lng&type=$placeType&rankby=distance&key=$key';
    var response = await http.get(Uri.parse(url));
    var json = convert.jsonDecode(response.body);
    var jsonResults = json['results'] as List;
    return jsonResults.map((place) => Place.fromJson(place)).toList();
  }
}
class MarkerService {

  LatLngBounds bounds(Set<Marker> markers) {
    return createBounds(markers.map((m) => m.position).toList());
  }

  LatLngBounds createBounds(List<LatLng> positions) {
    final southwestLat = positions.map((p) => p.latitude).reduce((value, element) => value < element ? value : element); // smallest
    final southwestLon = positions.map((p) => p.longitude).reduce((value, element) => value < element ? value : element);
    final northeastLat = positions.map((p) => p.latitude).reduce((value, element) => value > element ? value : element); // biggest
    final northeastLon = positions.map((p) => p.longitude).reduce((value, element) => value > element ? value : element);
    return LatLngBounds(
        southwest: LatLng(southwestLat, southwestLon),
        northeast: LatLng(northeastLat, northeastLon)
    );
  }

  Marker createMarkerFromPlace(Place place, bool center) {
    var markerId = place.name;
    if (center) markerId = 'center';

    return Marker(
        markerId: MarkerId(markerId),
        draggable: false,
        visible: (center) ? false : true,
        infoWindow: InfoWindow(
            title: place.name, snippet: place.vicinity),
        position: LatLng(place.geometry.location.lat,
            place.geometry.location.lng)
    );
  }


}
class GeolocatorService {

  Future<Position> getCurrentLocation() async {
    return  await Geolocator.getCurrentPosition();
  }
}