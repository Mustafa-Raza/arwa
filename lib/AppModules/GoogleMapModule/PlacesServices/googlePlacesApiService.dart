import 'dart:async';

import 'package:arwa/AppModules/GoogleMapModule/PlacesServices/GoogleApiServiceModel.dart';
import 'package:arwa/AppModules/GoogleMapModule/PlacesServices/geolocatorServices.dart';
import 'package:geolocator/geolocator.dart';
// import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get.dart';
// import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleApiPlace extends GetxController {
  final geoLocatorService = GeolocatorService();
  final placesService = PlacesService();
  final markerService = MarkerService();

  //Variables
  Position currentLocation = Position(
      longitude: 655,
      latitude: 666,
      timestamp: DateTime.now(),
      accuracy: 0.0,
      altitude: 0.0,
      heading: 0.0,
      speed: 0.0,
      speedAccuracy: 0.0);
  // List<PlaceSearch>
  var searchResults = <PlaceSearch>[].obs;
  StreamController<Place> selectedLocation = StreamController<Place>();
  StreamController<LatLngBounds> bounds = StreamController<LatLngBounds>();
  Place selectedLocationStatic =
      Place(geometry: Geometry(location: Location(lat: 98, lng: 98)));
  String placeType = "";
  List<Place> placeResults = <Place>[].obs;
  List<Marker> markers = <Marker>[];

  ApplicationBloc() {
    setCurrentLocation();
  }

  setCurrentLocation() async {
    currentLocation = await geoLocatorService.getCurrentLocation();
    selectedLocationStatic = Place(
      geometry: Geometry(
        location: Location(
            lat: currentLocation.latitude, lng: currentLocation.longitude),
      ),
    );
  }

  searchPlaces(String searchTerm) async {
    searchResults.value = await placesService.getAutocomplete(searchTerm);
    print("list of places data");
    print(searchResults);
  }

  setSelectedLocation(String placeId) async {
    var sLocation = await placesService.getPlace(placeId);
    // selectedLocation.add(sLocation);
    // selectedLocationStatic = sLocation;
    print("sLocation");
    print(sLocation.geometry.location.lat);
    print(sLocation.geometry.location.lng);
    searchResults.value = [];
  }

  clearSelectedLocation() {
    selectedLocation.isClosed;
    selectedLocationStatic;
    searchResults.value = [];
    placeType = "";
  }

  togglePlaceType(String value, bool selected) async {
    if (selected) {
      placeType = value;
    } else {
      placeType = "";
    }

    if (placeType != null) {
      var places = await placesService.getPlaces(
          selectedLocationStatic.geometry.location.lat,
          selectedLocationStatic.geometry.location.lng,
          placeType);
      markers = [];
      if (places.length > 0) {
        var newMarker = markerService.createMarkerFromPlace(places[0], false);
        markers.add(newMarker);
      }

      var locationMarker =
          markerService.createMarkerFromPlace(selectedLocationStatic, true);
      markers.add(locationMarker);

      var _bounds = markerService.bounds(Set<Marker>.of(markers));
      bounds.add(_bounds);
    }
  }

  @override
  void dispose() {
    selectedLocation.close();
    bounds.close();
    super.dispose();
  }
}
