import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GMapViewHelper {
  final key = "AIzaSyDK8AOLTBydNP8zMFR3CA7GXtrPQSPH9wo";

// List of coordinates to join
  List<LatLng> polylineCoordinates = [];

  GMapViewHelper();

  Widget buildMapView(
      {required BuildContext context,
      required Map<MarkerId, Marker> markers,
      Map<PolylineId, Polyline> polyLines = const <PolylineId, Polyline>{},
      required MapCreatedCallback onMapCreated,
      required ArgumentCallback<LatLng> onTap,
      required LatLng currentLocation}) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: GoogleMap(

        zoomGesturesEnabled: true,
        // zoomControlsEnabled: true,
        onMapCreated: onMapCreated,
        onTap: onTap,
        markers: Set<Marker>.of(
          markers.values,
        ),
        polylines: Set<Polyline>.of(polyLines.values),
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        initialCameraPosition: CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 12.0,
        ),
      ),
    );
  }

  static Marker createMaker({
    required String markerIdVal,
    required BitmapDescriptor icon,
    required double lat,
    required InfoWindow,
    required double lng,
    required GestureTapCallback onTap,
  }) {
    final MarkerId markerId = MarkerId(markerIdVal);
    BitmapDescriptor? customIcon;
    var byteData ;
    rootBundle.load('assets/paymentAndAddress/location.png').then((value) =>
    {
      byteData=value,
    });
// make sure to initialize before map loading
//     BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(12, 12)),
//             "assets/home/appBar/map_pin.png")
//         .then((d) {
//       customIcon = d;
//       print("customIcon");
//       print(customIcon);
//     });
   //  int i=1;
   // while(i==1){
   //   if(customIcon==null){
   //     i=1;
   //   }else{
   //     i=2;
   //   }
   // }

    final Marker marker = Marker(
        infoWindow: InfoWindow,
         icon: icon,
        // icon: BitmapDescriptor.fromBytes(byteData),
        // icon: BitmapDescriptor.fromJson('assets/paymentAndAddress/location.png'),
        // icon: BitmapDescriptor.defaultMarker,
        markerId: markerId,
        position: LatLng(lat, lng),

        // icon: BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)), icon).then((onValue) {
        //   myIcon = onValue;
        // });,
        onTap: onTap);

    return marker;
  }

  // static Polyline createPolyline ({
  //
  //   required String polylineIdVal,
  //   required final router,
  //   required LatLng formLocation,
  //   required LatLng toLocation,
  // }){
  //
  //   List<LatLng> polylineCoordinates = [];
  //
  //   final PolylineId polylineId = PolylineId(polylineIdVal);
  //
  //   if (router.points.isNotEmpty) {
  //     router.points.forEach((PointLatLng point) {
  //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
  //       print( polylineCoordinates.length);
  //       print("length");
  //     });
  //     print( polylineCoordinates);
  //   }
  //
  //   final Polyline polyline = Polyline(
  //     polylineId: polylineId,
  //     consumeTapEvents: true,
  //     color: Colors.blue,
  //     width: 6,
  //     points: polylineCoordinates,
  //   );
  //   return polyline;
  //
  // }

  void cameraMove({
    required LatLng fromLocation,
    required LatLng toLocation,
    required GoogleMapController mapController,
  }) async {
    var _latFrom = fromLocation.latitude;
    var _lngFrom = fromLocation.longitude;
    var _latTo = toLocation.latitude;
    var _lngTo = toLocation.longitude;
    var sLat, sLng, nLat, nLng;

    if (_latFrom <= _latTo) {
      sLat = _latFrom;
      nLat = _latTo;
    } else {
      sLat = _latTo;
      nLat = _latFrom;
    }

    if (_lngFrom <= _lngTo) {
      sLng = _lngFrom;
      nLng = _lngTo;
    } else {
      sLng = _lngTo;
      nLng = _lngFrom;
    }

    return mapController.animateCamera(
      CameraUpdate?.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(sLat, sLng),
          northeast: LatLng(nLat, nLng),
        ),
        16.0,
      ),
    );
  }
}
