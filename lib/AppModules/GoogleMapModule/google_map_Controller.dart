import 'dart:async';

import 'package:arwa/AppModules/GoogleMapModule/getLocationService.dart';
import 'package:arwa/AppModules/GoogleMapModule/google_map_helper.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:geocoding/geocoding.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

// import 'package:geolocator/geolocator.dart';
import 'package:flutter/cupertino.dart';

// import 'package:flutter/material.dart';
// // import 'package:geolocator/geolocator.dart';
// import 'package:getwidget/components/image/gf_image_overlay.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class GoogleMapViewController extends StatefulWidget {
  bool isSecondary;
  bool isFloatingBTN;
  LatLng latLng;
  Function onGetCurrentLocation;

  GoogleMapViewController(
      {required this.latLng,
      required this.isSecondary,
      this.isFloatingBTN = false,
      required this.onGetCurrentLocation});

  @override
  State<GoogleMapViewController> createState() =>
      GoogleMapViewControllerState();
}

class GoogleMapViewControllerState extends State<GoogleMapViewController> {
  GMapViewHelper googleMapHelper = GMapViewHelper();

  List<LatLng> polylineCoordinates = [];
  List<LatLng> markerslist = [];
  GoogleMapController? _mapController;
  Map<PolylineId, Polyline> polylines = {};
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};
  String? newLocationName;
  String _placemark = '';
  String? currentLocationName;
  LocationData? _locationData;
  Location location = new Location();

  bool? _serviceEnabled;
  PermissionStatus? _permissionGranted;
  Timer? timer;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer!.cancel();
  }

  @override
  void initState() {
    super.initState();
    _initCurrentLocation();
    onLocationListener();
  }

  onLocationListener() {
    print("Listen to location");
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (widget.latLng != LatLng(0.0, 0.0)) {
        _mapController?.animateCamera(
          CameraUpdate?.newCameraPosition(
            CameraPosition(
              target: LatLng(widget.latLng.latitude, widget.latLng.longitude),
              zoom: 16.0,
            ),
          ),
        );
        addMarker(
            "Address", LatLng(widget.latLng.latitude, widget.latLng.longitude));
      }
    });
  }

  Future<void> _initCurrentLocation() async {
    try {
      print("current location");
      // currentLocation = await _locationService.getCurrentPosition(
      //     desiredAccuracy: LocationAccuracy.best);

      // List<Placemark> placemarks =
      // await _locationService.placemarkFromCoordinates(
      //     currentLocation?.latitude, currentLocation?.longitude);
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled!) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled!) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }

      _locationData = await location.getLocation();
      print(_locationData);

      // vr placemarks =
      getLocation(_locationData!.latitude!, _locationData!.longitude!);

      _locationData = await location.getLocation();
      // if (placemarks != null && placemarks.isNotEmpty) {
      //   final Placemark pos = placemarks[0];
      //   setState(() {
      //     _placemark = pos.name! ;
      //     // + ', ' + pos.thoroughfare;
      //     print(_placemark);
      //     currentLocationName = _placemark;
      //     print("name"+currentLocationName!);
      //   });
      // }
      widget.onGetCurrentLocation(_locationData);
      if (_locationData != null) {
        addMarker("mylocation",
            LatLng(_locationData!.latitude!, _locationData!.longitude!));
        if (!widget.isSecondary) moveCameraToMyLocation();
      }
    } catch (e) {
      print(e);
    }
  }

  addMarker(String location, LatLng loclatlang) async {
    BitmapDescriptor? myIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(48, 48)),
        "assets/paymentAndAddress/location.png");
    //     .then((onValue) {
    //   myIcon = onValue;
    // });
    final MarkerId _markerFrom = MarkerId(location);
    LatLng fromLocation = loclatlang;

    setState(() {
      _markers[_markerFrom] = GMapViewHelper.createMaker(
        InfoWindow: InfoWindow(
          title: location,
          onTap: () {
            // Get.to(StylistProfilePage(stylist_id: location,));
          },
        ),
        onTap: () {},
        markerIdVal: location,
        icon: myIcon,
        lat: fromLocation.latitude,
        lng: fromLocation.longitude,
      );
    });
  }

  void moveCameraToMyLocation() {
    _mapController?.animateCamera(
      CameraUpdate?.newCameraPosition(
        CameraPosition(
          target: widget.isSecondary
              ? widget.latLng
              : LatLng(_locationData!.latitude!, _locationData!.longitude!),
          zoom: 16.0,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: widget.isFloatingBTN
          ? Padding(
              padding: const EdgeInsets.only(bottom: 120),
              child: FloatingActionButton(
                backgroundColor: Colors.white,
                onPressed: () {
                  moveCameraToMyLocation();
                },
                child: Icon(
                  Icons.my_location_outlined,
                  color: AppColor.buttonPrimaryColor,
                  size: 30,
                ),
              ),
            )
          : SizedBox(
              width: 0,
              height: 0,
            ),
      body: googleMapHelper.buildMapView(
          onMapCreated: _onMapCreated,
          onTap: (argument) {},
          context: context,
          markers: _markers,
          currentLocation: LatLng(28.588221, 77.249786)),
    );
  }

  void _onMapCreated(GoogleMapController controller) async {
    this._mapController = controller;
    if (widget.isSecondary) {
      addMarker("Job Location", widget.latLng);
      moveCameraToMyLocation();
    } else {
      addMarker("Branch Location",
          LatLng(_locationData!.latitude!, _locationData!.latitude!));
    }
  }

  static Marker createMaker({
    required String markerIdVal,
    required String icon,
    required double lat,
    required InfoWindow,
    required double lng,
    GestureTapCallback? onTap,
  }) {
    final MarkerId markerId = MarkerId(markerIdVal);
    BitmapDescriptor? myIcon;

    var byteData;
    rootBundle.load('assets/paymentAndAddress/location.png').then((value) => {
          byteData = value,
        });
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            "assets/paymentAndAddress/location.png")
        .then((onValue) {
      myIcon = onValue;
    });
    final Marker marker = Marker(
        infoWindow: InfoWindow,
        icon: BitmapDescriptor.fromBytes(byteData),
        // icon: BitmapDescriptor.fromBytes(byteData),
        // icon: myIcon!,
        // icon: icon!=null?icon!:BitmapDescriptor.defaultMarker,
        markerId: markerId,
        position: LatLng(lat, lng),
        onTap: onTap);

    return marker;
  }
}
