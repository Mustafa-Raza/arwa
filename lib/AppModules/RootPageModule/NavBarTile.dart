import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class NavBarTile extends StatefulWidget {
  final Color color;
  final bool isSelected;
  final bool leftCurve;
  final bool rightCurve;
  final String title;
  final Widget icon;
final VoidCallback callback;
  NavBarTile(
      {Key? key, required this.color,this.leftCurve=false,this.rightCurve=false,required this.callback, required this.isSelected, required this.title, required this.icon})
      : super(key: key);

  @override
  _NavBarTileState createState() => _NavBarTileState();
}

class _NavBarTileState extends State<NavBarTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        widget.callback();

      },
      child
          : SizedBox(

        height: 46,
        width: AppConfig(context).width / 5,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            widget.icon,
            SizedBox(
              height: 5,
            ),
            AppText.customText(title: widget.title,color:widget.isSelected? widget.color:Color(0xFF999B9F) , size: 8),
          ],
        ),
      ),
    );
  }
}
