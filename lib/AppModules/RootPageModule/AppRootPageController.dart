import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:arwa/AppModules/CategoriesModule/ViewController/AllCategoriesViewController.dart';
import 'package:arwa/AppModules/HistoryModule/ViewController/OrderHistoryViewController.dart';
import 'package:arwa/AppModules/HomeModule/ViewController/HomeViewController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyProfileModule/ViewController/MyProfileViewController.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/ViewController/OfferAndDealsViewController.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/MyWalletViewController.dart';
import 'package:arwa/AppModules/RootPageModule/NavBarTile.dart';
import 'package:arwa/AppModules/RootPageModule/src/circular_notch_and_corner_clipper.dart';
import 'package:arwa/AppModules/RootPageModule/src/circular_notched_and_cornered_shape.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppRootPageController extends StatefulWidget {
  final int currentIndex;

  AppRootPageController({Key? key, this.currentIndex = 2}) : super(key: key);

  @override
  _AppRootPageControllerState createState() => _AppRootPageControllerState();
}

class _AppRootPageControllerState extends State<AppRootPageController> {
  int currentItem = 2;
  List<Widget> _buildScreens = [
    AllCategoryViewController(),
    OfferAndDealsViewController(
      inner: false,
    ),
    HomeViewController(),
    MyWalletViewController(),
    MyProfileViewController(),
  ];

  final categoryController = Get.put(HomeCategoryController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      currentItem = widget.currentIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // backgroundColor: Colors.grey.shade300,
      // backgroundColor: Color(0xFFFFFFFF),
      floatingActionButton: Obx(
        () => FloatingActionButton(
          elevation: 0,
          backgroundColor: categoryController.selectedBrand != null
              ? Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
              : AppColor.primaryColor,
          onPressed: () {
            setState(() {
              currentItem = 2;
            });
          },
          heroTag: ImageIcon(
            AssetImage("assets/home/NavBar/home.png"),
            size: 24,
            color: Colors.white,
          ),
          child: ImageIcon(
            AssetImage("assets/home/NavBar/home.png"),
            size: 24,
            color: Colors.white,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: new BottomAppBar(
        // hasNotch: true,
        clipBehavior: Clip.hardEdge,

        color: Colors.white,
        notchMargin: 8,
        elevation: 15,
        shape: CircularNotchedAndCorneredRectangle(
            notchSmoothness: NotchSmoothness.defaultEdge,
            gapLocation: GapLocation.center,
            leftCornerRadius: 15,
            rightCornerRadius: 15),
        // elevation: 100,
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            NavBarTile(
              callback: () {
                setState(() {
                  currentItem = 0;
                });
              },
              // leftCurve: true,
              // rightCurve: true,
              color: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              icon: ImageIcon(
                AssetImage("assets/home/NavBar/Category.png"),
                size: 24,
                color: currentItem == 0
                    ? Color(int.parse(
                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                    : Color(0xFF999B9F),
              ),
              title: "Categories",
              isSelected: currentItem == 0 ? true : false,
            ),
            NavBarTile(
              callback: () {
                setState(() {
                  currentItem = 1;
                });
              },
              color: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              icon: ImageIcon(
                AssetImage("assets/home/NavBar/deals.png"),
                size: 24,
                color: currentItem == 1
                    ? Color(int.parse(
                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                    : Color(0xFF999B9F),
              ),
              title: "Deals",
              isSelected: currentItem == 1 ? true : false,
            ),
            SizedBox(
              height: 60,
              width: AppConfig(context).width / 5,
            ),
            NavBarTile(
              callback: () {
                setState(() {
                  currentItem = 3;
                });
              },
              color: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              icon: ImageIcon(
                AssetImage("assets/home/NavBar/wallet.png"),
                size: 24,
                color: currentItem == 3
                    ? Color(int.parse(
                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                    : Color(0xFF999B9F),
              ),
              title: "Wallet",
              isSelected: currentItem == 3 ? true : false,
            ),
            NavBarTile(
              callback: () {
                setState(() {
                  currentItem = 4;
                });
              },
              color: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              icon: ImageIcon(
                AssetImage("assets/home/NavBar/profile.png"),
                size: 24,
                color: currentItem == 4
                    ? Color(int.parse(
                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                    : Color(0xFF999B9F),
              ),
              isSelected: currentItem == 4 ? true : false,
              title: "Profile",
            ),
          ],
        ),
      ),

      body: _buildScreens[currentItem],
    );
  }
}
