import 'dart:convert';
import 'dart:io';

import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class ApiCall {
  createAccount(
      {required String apiUrl,
      required Map data,
        bool authorization=false,
      required BuildContext context}) async {
    try {
      var request = http.MultipartRequest(
        "POST",
        Uri.parse(apiUrl),
      );
      if(authorization) {
        ///TODO: get token to local
        var user = await DatabaseHandler().getCurrentUser();
        var token = await DatabaseHandler().getToken();
        print("userid");
        print(user["id"]);
        print(user["customerid"]);
        print(data["email"]);
        print(data["username"]);
        print(token);

        request.headers["Authorization"] = "Bearer ${token}";
        request.fields["id"] =user["id"].toString();
        request.fields["customerid"] =user["customerid"].toString();
        // request.fields["customerid"] =user["customerid"];
        // request.fields["username"] =user["username"];

      }
      request.headers['accept'] = '*/*';
      request.headers["Content-Type"] = 'multipart/form-data';


      request.fields["city"] = data["city"];
      request.fields["companyname"] = data["companyname"];
      // request.fields["contact"] = data["contact"];
      // request.fields["country"] = data["country"];
      // request.fields["dob"] = data["dob"];
      request.fields["email"] = data["email"];
      request.fields["firstname"] = data["firstname"];
      request.fields["gender"] = data["gender"];
      request.fields["lastname"] = data["lastname"];
      if(!authorization)
      request.fields["password"] = data["password"];

      request.fields["username"] = data["username"];
      request.fields["contact"] = data["contact"];
      request.fields["country"] = "AE";
      request.fields["profileimage"] = data["profileimage"];
      // var pic =
      // await http.MultipartFile.fromPath("file", imgFile.path.trim());
      // request.files.add(pic);
      print("test");
      var response = await request.send();
      print("test");
      var result = await http.Response.fromStream(response);
      print(json.decode(result.body));
      return result;

    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
// constValues().toast("$error", context);
// EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  postRequest(
      {required String apiUrl,
      required Map bodyParameter,
      required BuildContext context}) async {
    try {
      final response = await http.post(
        Uri.parse("$apiUrl"),
        headers: {
          'accept': '*/*',
          'Content-Type': 'application/json-patch+json'
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);
      print(jsonDecode(response.body));

      return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  postRequest1(
      {required String apiUrl,
      required Map bodyParameter,
      required BuildContext context}) async {
    try {
      final response = await http.post(
        Uri.parse(""),
        headers: {
          'accept': '*/*',
          'Content-Type': 'application/json-patch+json'
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);
      // print(jsonDecode(response.body));
      return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  postRequestHeader(
      {required String apiUrl,
      required Map bodyParameter,
      required BuildContext context}) async {
    ///TODO: get token to local
    var token = await DatabaseHandler().getToken();
    print(token);
    try {
      final response = await http.post(
        Uri.parse("$apiUrl"),
        headers: {
          "Accept": "*/*",
          'Content-Type': 'application/json-patch+json',
          "Authorization": "Bearer ${token.toString()}",
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);

       return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }
  deleteRequestHeader(
      {required String apiUrl,
        required Map bodyParameter,
      required BuildContext context}) async {
    ///TODO: get token to local
    var token = await DatabaseHandler().getToken();
    print(token);
    try {
      final response = await http.delete(
        Uri.parse("$apiUrl"),
        headers: {
          "Accept": "*/*",
          // 'Content-Type': 'application/json-patch+json',
          "Authorization": "Bearer ${token.toString()}",
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);

       return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  postListRequestHeader(
      {required String apiUrl,
      required List<dynamic> bodyParameter,
      required BuildContext context}) async {
    ///TODO: get token to local
    var token = await DatabaseHandler().getToken();
    print(token);
    try {
      final response = await http.post(
        Uri.parse("$apiUrl"),
        headers: {
          "Accept": "*/*",
          'Content-Type': 'application/json-patch+json',
          "Authorization": "Bearer ${token.toString()}",
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);

      return response;
      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  putRequest(
      {required String apiUrl,
      required Map bodyParameter,
      required BuildContext context}) async {
    try {
      ///TODO: get token to local
      // var token = await DatabaseHandler().getToken();
      final response = await http.put(
        Uri.parse(""),
        headers: {
          "Accept": "application/json",
          // "Authorization": "Bearer ${token.toString()}",
        },
        body: jsonEncode(bodyParameter),
      );
      print(response.statusCode);
      print(jsonDecode(response.body));
      if (response.statusCode == 200) {
        return response;
      } else {
        throw Exception('Failed to create album.');
      }
      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      // constValues().toast("$error", context);
      // EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
  }

  getRequest({required String apiUrl, required BuildContext context}) async {
    try {
      final response = await http.get(
        Uri.parse("$apiUrl"),
        // Send authorization headers to the backend.
        headers: {
          "accept": '*/*',
          // "Authorization": "Bearer ${token.toString()}",
        },
      );
      final responseJson = jsonDecode(response.body);
      print(responseJson);

      print(responseJson["message"]);
      return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
// constValues().toast("$error", context);
// EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
    // return Album.fromJson(responseJson);
  }
  getRequestHeader({required String apiUrl, required BuildContext context}) async {
    try {
      ///TODO: get token to local
      var token = await DatabaseHandler().getToken();
      print(token);
      final response = await http.get(
        Uri.parse("$apiUrl"),
        // Send authorization headers to the backend.
        headers: {
          "accept": '*/*',
          "Authorization": "Bearer ${token.toString()}",
        },
      );
      final responseJson = jsonDecode(response.body);
      print(responseJson);

      print(responseJson["message"]);
      return response;

      //Connection timed out
    } on SocketException {
      ShowMessage().showErrorMessage(context, "No Internet Connection");
      return null;
      print('No Internet connection 😑');
    } on HttpException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
      print("Couldn't find the post 😱");
      return null;
    } on FormatException catch (error) {
      // ShowMessage().showErrorMessage(context, "$error");
// constValues().toast("$error", context);
// EasyLoading.dismiss();
      print("Bad response format 👎");
      return null;
    } catch (e) {
      if (e.toString().contains("Connection timed out")) {
        return true;
      }
    }
    // return Album.fromJson(responseJson);
  }

  Future uploadFile(
      {required String url,
      required String name,
      required File imgFile}) async {
    ///TODO: get token to local
    // var token = await DatabaseHandler().getToken();
    // print(token);
    var request = http.MultipartRequest(
      "POST",
      Uri.parse(url),
    );
    request.headers['accept'] = '*/*';
    request.headers["Content-Type"] = 'multipart/form-data';
    // request.headers["Authorization"] ='Bearer ${token}';
    // request.fields["name"] = name;
    var pic = await http.MultipartFile.fromPath("file", imgFile.path.trim());
    request.files.add(pic);
    var response = await request.send();
    var result = await http.Response.fromStream(response);
    print(json.decode(result.body));
    return result;
    // return jsonDecode(result.body);
    // print(response.toString());
    // var responseData = await response.stream.toBytes();
    // var responseString = String.fromCharCodes(responseData);
    // print(responseString);
    // print(response. + "my code");
    // return responseData;
  }
}
