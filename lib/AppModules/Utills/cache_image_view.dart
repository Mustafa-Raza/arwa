import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget cacheImageView({required String image, BoxFit boxfit = BoxFit.fill,double circlularPadding=10,Color imageColor=Colors.transparent}) {
  return CachedNetworkImage(
    imageUrl: "${image}",
    // color: imageColor,
    fit: boxfit,
    progressIndicatorBuilder: (context, url, downloadProgress) => Padding(
      padding:  EdgeInsets.all(circlularPadding),
      child: SizedBox(
          height: 40,
          width: 40,
          child: CircularProgressIndicator(

              color: AppColor.primaryColor, value: downloadProgress.progress)),
    ),
    errorWidget: (context, url, error) => Icon(Icons.error),
  );
}
