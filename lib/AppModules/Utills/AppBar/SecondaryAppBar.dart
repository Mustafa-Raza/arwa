import 'package:arwa/AppModules/CartModule/ViewController/MyCartViewController.dart';
import 'package:arwa/AppModules/CartModule/ViewModel/cart_view_model.dart';
import 'package:arwa/AppModules/NotificationModule/ViewController/NotificationViewController.dart';
import 'package:arwa/AppModules/Utills/AppBar/BrightnessType.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecondaryAppBar extends StatelessWidget {
  final String title;
  final BrightnessType brightnessType;

  SecondaryAppBar(
      {Key? key,
      required this.title,
      this.brightnessType = BrightnessType.dark})
      : super(key: key);

  final cartVM = Get.put(CartViewModel());
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: 32,
      child: Row(
        children: [
          InkWell(
            radius: 50,
            onTap: () {
              // Navigator.pop(context);
              Get.to(()=>NotificationViewController(),transition: Transition.rightToLeft);
            },
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 20,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                  image: AssetImage("assets/home/notification-bing.png"),
                  width: 30,
                  height: 30,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Spacer(),
          InkWell(
              child: AppText.customText(
            title: title,
            color: brightnessType == BrightnessType.dark
                ? AppColor.DarkText
                : Colors.white,
            fontWeight: FontWeights.semi_bold,
            size: 16,
          )),
          Spacer(),
          InkWell(
            onTap: () {
              Get.to(() => MyCartViewController(),
                  transition: Transition.rightToLeft);
            },
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 20,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                SizedBox(
                  width: 30,
                  height: 30,
                  child: Stack(
                    alignment: Alignment.topRight,
                    children: [
                      Image(
                        image: AssetImage(
                            "assets/home/cart.png"),
                        width: 30,
                        height: 30,
                        fit: BoxFit.fill,
                      ),
                      Obx(
                            () => CircleAvatar(
                          radius: 3,
                          backgroundColor: cartVM
                              .getCartData
                              .value.cartList
                              .isNotEmpty
                              ? Colors.pink
                              : Colors.transparent,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
