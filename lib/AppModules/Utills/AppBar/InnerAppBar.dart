import 'package:arwa/AppModules/Utills/AppBar/BrightnessType.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InnerAppBar extends StatelessWidget {
  final String title;
  final VoidCallback ?callback;
  final BrightnessType brightnessType;
final bool defaultBackNavigation;
  InnerAppBar(
      {Key? key,
      required this.title,
        this.callback,
       this.defaultBackNavigation=true,
      this.brightnessType = BrightnessType.dark})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          InkWell(
              radius: 50,
              onTap: () {
                if(defaultBackNavigation){
                  Navigator.pop(context);
                }else{
                  callback!();
                }

              },
              child: Icon(
                Icons.arrow_back_ios_rounded,
                color: brightnessType == BrightnessType.dark
                    ? AppColor.DarkText
                    : Colors.white,
                size: 20,
              )),
          Spacer(),
          InkWell(
              child: AppText.customText(
            title: title,
            color: brightnessType == BrightnessType.dark
                ? AppColor.DarkText
                : Colors.white,
            fontWeight: FontWeights.semi_bold,
            size: 16,
          )),
          Spacer(),
          SizedBox(
            height: 16,
            width: 16,
          ),
        ],
      ),
    );
  }
}
