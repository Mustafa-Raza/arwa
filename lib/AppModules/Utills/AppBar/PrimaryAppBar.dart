import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class PrimaryAppBar extends StatelessWidget {
  final BrightnessType brightnessType;
  final bool isStarIcon;

  PrimaryAppBar(
      {Key? key,
      this.brightnessType = BrightnessType.Light,
      this.isStarIcon = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 19),
      child: SizedBox(
        height: 32,
        child: Row(
          children: [
            Image(
              image: AssetImage("assets/home/appBar/map_pin.png"),
              height: 16.11,
              width: 16.11,
              color: brightnessType == BrightnessType.Light
                  ? AppColor.white
                  : AppColor.DarkText,
            ),
            SizedBox(
              width: 6.71,
            ),
            InkWell(
                child: AppText.customText(
              title: "Current Location",
              color: brightnessType == BrightnessType.Light
                  ? AppColor.white
                  : AppColor.DarkText,
              fontWeight: FontWeights.medium,
              size: 14,
            )),
            SizedBox(
              width: 6.5,
            ),
            Image(
              image: AssetImage("assets/home/appBar/caret_down.png"),
              height: 15,
              width: 15,
              color: brightnessType == BrightnessType.Light
                  ? AppColor.white
                  : AppColor.DarkText,
            ),
            Spacer(),
            if (brightnessType == BrightnessType.Light && isStarIcon)
              Image(
                image: AssetImage("assets/home/appBar/star.png"),
                height: 32,
                width: 32,
              ),
            SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () {
              },
              child: Image(
                image: AssetImage("assets/home/appBar/notification_circle.png"),
                height: 32,
                width: 32,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Image(
              image: AssetImage("assets/home/appBar/user.png"),
              height: 32,
              width: 32,
            ),
          ],
        ),
      ),
    );
  }
}

enum BrightnessType {
  Light,
  Dark,
}
