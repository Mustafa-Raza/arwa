import 'package:flutter/material.dart';
class AppColor {
  // appcolors._();
  static const Color transparent = Color(0xFF00000000);
// ! App BTN Colors
  static const Color buttonPrimaryColor= Color(0xFF186B6D);
  static const Color unActiveNavColor= Color(0xFF999B9F);
  static const Color buttonTXTPrimaryColor= Colors.white;

  static const Color buttonSecondaryColor= Colors.white;
  static const Color extDARKt= Color(0xFF1D2733);
  static const Color textGREY= Color(0xFF999B9F);
  static const Color textDARKRed= Color(0xFF89182E);
  static const Color border= Color(0xFFCECECE);
  static const Color facebookButtonColor= Color(0xFF3A5998);
  static const Color googleButtonColor= Color(0xFFEA4335);
  static const Color appleButtonColor= Color(0xFF1D2733);
  static const Color authFieldColor= Color(0xFFF9F9FC);
// ! App Colors

  static const Color primaryColor = Color(0xFF0759AA);
  static const Color homeGreen = Color(0xFF75C650);
  static const Color Secondarycolor = Color(0xFF2bd2ca);
  static const Color Backgroundcolor = Color(0xFFf5f6f8);
  static const Color homeSearchShadowColor = Color(0xFF0001A);
  static const Color discountColor = Color(0xFFF1123F);
// !Rendom Colors
  static const Color white = Color(0xFFffffff);
  static const Color black = Color(0xFF000000);
  static const Color green = Color(0xFF33cc66);
  static const Color red = Color(0xFFff3333);
// ! Text Colors
  static const Color DarkText = Color(0xFF1D2733);
  static const Color greenishText = Color(0xFF21a886);
  static const Color greyText = Color(0xFFb4b3bb);
  static const Color onBoardText = Color(0xFF999B9F);
  BoxShadow boxShadow=BoxShadow(
    color: Colors.grey.shade200,blurRadius: 15,offset: Offset(0,8),
    // color: Color(0xFF0000001A),blurRadius: 15,offset: Offset(0,8),
  );
}

class FontWeights{
  static const FontWeight regular =FontWeight.w400;
  static const FontWeight medium =FontWeight.w500;
  static const FontWeight semi_bold =FontWeight.w600;
  static const FontWeight bold =FontWeight.w700;
  static const FontWeight extra_bold =FontWeight.w800;
}
class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
