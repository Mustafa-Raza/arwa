import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class SpinKitView extends StatelessWidget {
  final categoryController = Get.put(HomeCategoryController());
  bool themeIsDark = true;
  double size;

  SpinKitView({
    required this.themeIsDark,
    this.size = 35,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          // height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: themeIsDark
              ? Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
              : Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // SpinKitFadingFour(
              // SpinKitFoldingCube(
              // SpinKitHourGlass(
              SpinKitFadingCircle(
                size: size,
                color: themeIsDark
                    ? Colors.white
                    : Color(int.parse(
                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              ),
            ],
          ),
        ));
  }
}

void showLoadingIndicator(
    {required BuildContext context, bool isDark = false}) {
  showDialog(

    barrierDismissible: false,
    useRootNavigator: false,
    context: context,
    builder: (BuildContext context) {
      return  Padding(
        padding:  EdgeInsets.symmetric(horizontal: AppConfig(context).width/2-50),
        child: Dialog(
          insetPadding: EdgeInsets.zero,

          clipBehavior: Clip.antiAliasWithSaveLayer,

          child: Container(
            height: 50,
            color: isDark ?   AppColor.primaryColor:Colors.white,
            width: 50,
            child: SpinKitFadingCircle(
              size: 35,
              color: isDark ? Colors.white : AppColor.primaryColor,
            ),
          ),
        ),
      );
      //   SizedBox(
      //   height: 50,
      //
      //   width: 50,
      //   child: AlertDialog(
      //     content: Center(
      //       child: Container(
      //         height: 50,
      //         color: isDark ?   AppColor.primaryColor:Colors.white,
      //         width: 50,
      //         child: SpinKitFadingCircle(
      //           size: 35,
      //           color: isDark ? Colors.white : AppColor.primaryColor,
      //         ),
      //       ),
      //     ),
      //   ),
      // );

    },
  );
}

void hideOpenDialog({
  required BuildContext context,
}) {
  Navigator.of(context).pop();
}

Widget spinKitButton(BuildContext context, double height, double width,Color color) {
  return Container(
    height: height,
    width: width,
    decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: color),
    child: Center(
      child: SpinKitFadingCircle(
        size: 25,
        color: Colors.white,
      ),
    ),
  );
}
