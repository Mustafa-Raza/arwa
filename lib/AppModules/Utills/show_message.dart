
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';

import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class ShowMessage {

  showMessage(
    BuildContext context,
    String message,
  ) {
    return showToast(
      '$message',
      textStyle: TextStyle(color: AppColor.primaryColor),
      context: context,
      animation: StyledToastAnimation.slideFromTop,
      backgroundColor:Colors.white,

      dismissOtherToast: true,
      position: StyledToastPosition.top,
    );
  }

  showErrorMessage(BuildContext context, String message) {
    return showToast(
      '$message',
      context: context,
      animation: StyledToastAnimation.slideFromTop,
      backgroundColor: Colors.red,
      dismissOtherToast: true,
      position: StyledToastPosition.top,
    );
  }
}
