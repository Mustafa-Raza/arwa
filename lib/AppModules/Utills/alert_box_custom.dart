import 'dart:io';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/google_login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

onLogoutPop(BuildContext context) {
  final size = MediaQuery.of(context).size;
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Container(
            height: 160,
            decoration: BoxDecoration(
              color: AppColor.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  AppText.customText(
                      title: "Are you sure?",
                      color: AppColor.primaryColor,
                      size: 20,
                      fontWeight: FontWeights.semi_bold),
                  SizedBox(
                    height: 5,
                  ),
                  AppText.customText(
                      title: "Do you want to Logout !",
                      color: AppColor.primaryColor,
                      size: 14,
                      fontWeight: FontWeights.semi_bold),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 45,
                          width: size.height / 6.76,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "No",
                                color: AppColor.primaryColor,
                                size: 17,
                                fontWeight: FontWeights.semi_bold),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await deleteFCMService(context: context);
                          await DatabaseHandler().deleteUser("currentUser");
                          await DatabaseHandler().deleteUser("token");
                          await DBProvider.db.deleteAllTableData();

                          await  GoogleLoginService().onGoogleLogout();
                          Get.offAll(() => LoginViewController());
                          // Navigator.pushReplacement(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => LoginView()),
                          // );
                        },
                        child: Container(
                          height: 45,
                          width: size.height / 7,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColor.primaryColor,
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "Yes",
                                color: AppColor.white,
                                size: 17,
                                fontWeight: FontWeights.semi_bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ),
        );
      });
}

onExitPop(BuildContext context) async {
  final size = MediaQuery.of(context).size;
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Container(
            height: size.height / 5.07,
            decoration: BoxDecoration(
              color: AppColor.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  AppText.customText(
                      title: "Are you sure?",
                      color: AppColor.primaryColor,
                      size: 20,
                      fontWeight: FontWeights.bold),
                  SizedBox(
                    height: 5,
                  ),
                  AppText.customText(
                      title: "Do you want to Exit!",
                      color: AppColor.primaryColor,
                      size: 14,
                      fontWeight: FontWeights.bold),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 45,
                          width: size.height / 7,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "No",
                                color: AppColor.primaryColor,
                                size: 17,
                                fontWeight: FontWeights.bold),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          exit(0);
                        },
                        child: Container(
                          height: 45,
                          width: size.height / 7,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColor.primaryColor,
                            border: Border.all(
                                color: AppColor.primaryColor, width: 1.5),
                          ),
                          child: Center(
                            child: AppText.customText(
                                title: "Yes",
                                color: AppColor.primaryColor,
                                size: 14,
                                fontWeight: FontWeights.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
