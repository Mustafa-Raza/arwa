import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppText {
  static Widget logoText({required BuildContext context}) {
    return Text(
      "Arwa",
      style: TextStyle(
        fontFamily: 'Poppins',
        color: Colors.white,
        fontWeight: FontWeight.w800,
        fontSize: 30,
      ),
    );
  }

  static Widget customText(
      {TextAlign alignment = TextAlign.start,
      required String title,
      Color color = Colors.black,
      TextOverflow overFlowText = TextOverflow.visible,
      double size = 15,
      double letterSpacce = 0,
      FontWeight fontWeight = FontWeight.w500}) {
    return Text(
      title,
      textAlign: alignment,
      overflow: overFlowText,
      style: TextStyle(

        letterSpacing: letterSpacce,
        fontFamily: 'Poppins',
        color: color,
        fontWeight: fontWeight,
        fontSize: size,
      ),
    );
  }

  static Widget customTextButton(
      {TextAlign alignment = TextAlign.start,
      required String title,
      Color color = Colors.black,
      double size = 15,
      required VoidCallback callBack,
      FontWeight fontWeight = FontWeight.w500}) {
    return TextButton(
      onPressed: () {
        callBack();
      },
      child: Text(
        title,
        textAlign: alignment,
        style: TextStyle(
          fontFamily: 'Poppins',
          color: color,
          fontWeight: fontWeight,
          fontSize: size,
        ),
      ),
    );
  }
}
