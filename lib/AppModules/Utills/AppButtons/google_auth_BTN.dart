import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Widget googleAuthButton({
  Color txtColor = Colors.white,
  Color btnColor = AppColor.primaryColor,
  required VoidCallback callback,
  double height = 50,
  fontsized = 18.0,
  fontweight = FontWeight.w600,
  double width = 300,
}) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(8),
    child: MaterialButton(
      height: height,
      minWidth: width,
      elevation: 0,
      color: btnColor,
      // shape: StadiumBorder(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 10,
          ),
          Image(
            image: AssetImage("assets/auth/google.png"),
            width: 30,
            height: 30,
          ),
          Spacer(),
          AppText.customText(
              size: 14,
              title: "Sign in with Google",
              color: txtColor,
              fontWeight: fontweight),
          Spacer(),
          SizedBox(
            width: 40,
          ),
        ],
      ),
      onPressed: () {
        callback();
      },
    ),
  );
}
