import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';

Widget primaryButton({
  required buttonTitle,
  Color txtColor = Colors.black,
  Color btnColor = AppColor.primaryColor,
  required VoidCallback callback,
  double height = 50,
  fontsized = 18.0,
  fontweight = FontWeight.w600,
  double width = 300,
}) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(8),
    child: MaterialButton(
      height: height,
      minWidth: width,
      elevation: 0,
      color: btnColor,
      // shape: StadiumBorder(),
      child: AppText.customText(
          size: 14,
          title: buttonTitle,
          color: txtColor,
          fontWeight: fontweight),
      onPressed: () {
        callback();
      },
    ),
  );
}
