import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';

Widget iconButton({
  required buttonTitle,
  Color txtColor = Colors.black,
  Color btnColor = AppColor.primaryColor,
  required VoidCallback callback,
  double height = 50,
  padding,
  fontSize = 18.0,
  double width = 300,
  FontWeight fontWeight=FontWeights.semi_bold,
  bool isEnableICON = false,
}) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(6),
    child: MaterialButton(
      height: height,
      minWidth: width,
      elevation: 0,
      color: btnColor,
      // shape: StadiumBorder(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (isEnableICON)
            Image(
              image: AssetImage("assets/onBoard/enablebtn.png"),
              height: 20,
            ),
          if (isEnableICON)
            SizedBox(
              width: 20,
            ),
          AppText.customText(
              title: buttonTitle,
              color: txtColor,
              fontWeight: fontWeight),
          if (!isEnableICON)
            SizedBox(
              width: 9,
            ),
          if (!isEnableICON)
            Icon(
              Icons.arrow_forward_rounded,
              color: txtColor,
              size: 20,
            ),
        ],
      ),
      onPressed: () {
        callback();
      },
    ),
  );
}

Widget iconButtonOnBoard({
  required buttonTitle,
  Color txtColor = Colors.black,
  Color btnColor = AppColor.primaryColor,
  required VoidCallback callback,
  double height = 50,
  padding,
  fontSize = 18.0,
  double width = 300,
  bool isEnableICON = false,
  bool isGetStarted = false,
}) {
  return Padding(
    padding: padding ?? EdgeInsets.only(left: 5, right: 5, top: 0),
    child: InkWell(
      onTap: (){
        callback();
      },
      child: Container(
        width: width,
        height: 35,
        decoration: BoxDecoration(
          color: btnColor,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (isEnableICON)
              Image(
                image: AssetImage("assets/onBoard/enablebtn.png"),
                height: 20,
              ),
            if (isEnableICON)
              SizedBox(
                width: 20,
              ),
            if(isGetStarted)
              AppText.customText(
                title: buttonTitle,
                color: txtColor,size: 14,
                fontWeight: FontWeights.medium  ,),
            if(!isGetStarted)
            AppText.customText(
                title: buttonTitle,
                color: txtColor,size: 16,
                fontWeight: FontWeights.semi_bold  ,),
            if (!isEnableICON)
              SizedBox(
                width: 9,
              ),
            if (!isEnableICON)
              Icon(
                Icons.arrow_forward_rounded,
                color: txtColor,
                size: 20,
              ),
          ],
        ),
      ),
    )
  );
}
