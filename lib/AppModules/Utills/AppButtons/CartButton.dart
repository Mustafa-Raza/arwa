import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Widget cartButton({
  required buttonTitle,
  required bool isLoading,
  Color txtColor = Colors.black,
  Color btnColor = AppColor.primaryColor,
  required VoidCallback callback,
  double height = 50,
  required int cartList,

  fontsized = 18.0,
  fontweight = FontWeight.w600,
  double width = 300,

}) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(8),
    child: MaterialButton(
      height: height,
      minWidth: width,
      elevation: 0,
      color: btnColor,
      // shape: StadiumBorder(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(width: 10,),
          Spacer(),
          Image(image: AssetImage("assets/home/cart.png"),width: 24,height: 24,color: Colors.white,),
          SizedBox(width: 10,),
          isLoading? SizedBox(
            height: 30,
            width: 100,
            child: SpinKitFadingCircle(
              size: 30,

              color: Colors.white ,
            ),
          ):
          AppText.customText(
              size: 14,
              title: buttonTitle,
              color: txtColor,
              fontWeight: fontweight),
          Spacer(),
          CircleAvatar(
            radius: 12,
            backgroundColor: Colors.white,
            child:     AppText.customText(
                size: 12,
                title:cartList.toString(),
                color: AppColor.DarkText,
                fontWeight: FontWeights.medium),
          )
        ],
      ),
      onPressed: () {
        callback();
      },
    ),
  );
}
