import 'dart:io';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database!;

    // If database don't exists, create one
    _database = await initDB();

    return _database!;
  }

  Future<bool> databaseExists() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'arwa.db');
    return databaseFactory.databaseExists(path);
  }

  // Create the database and the Employee table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'arwa.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE User('
          'id INTEGER PRIMARY KEY,'
          'user TEXT'
          ')');
      await db.execute('CREATE TABLE Home('
          'id INTEGER PRIMARY KEY,'
          'home TEXT'
          ')');
      await db.execute('CREATE TABLE Brand('
          'id INTEGER PRIMARY KEY,'
          'brands TEXT'
          ')');
      await db.execute('CREATE TABLE Product('
          'id INTEGER PRIMARY KEY,'
          'products TEXT'
          ')');
      await db.execute('CREATE TABLE Offer('
          'id INTEGER PRIMARY KEY,'
          'offers TEXT'
          ')');
      // await db.execute('CREATE TABLE Favourite('
      //     'id INTEGER PRIMARY KEY,'
      //     'favourite TEXT'
      //     ')');
      await db.execute('CREATE TABLE Favourite('
          'id INTEGER PRIMARY KEY,'
          'productid TEXT,'
          'productdesc TEXT,'
          'country TEXT,'
          'brandcode TEXT,'
          'flavourcode TEXT,'
          'packsize TEXT,'
          'baseprice REAL,'
          'discountprice REAL,'
          'image TEXT,'
          'producttype TEXT,'
          'infavorite INTEGER'
          ')');
      await db.execute('CREATE TABLE Cart('
          'id INTEGER PRIMARY KEY,'
          'cart TEXT'
          ')');
      await db.execute('CREATE TABLE Address('
          'id INTEGER PRIMARY KEY,'
          'address TEXT'
          ')');
      await db.execute('CREATE TABLE History('
          'id INTEGER PRIMARY KEY,'
          'history TEXT'
          ')');
      await db.execute('CREATE TABLE Cards('
          'id INTEGER PRIMARY KEY,'
          'cards TEXT'
          ')');
      await db.execute('CREATE TABLE Notification('
          'id INTEGER PRIMARY KEY,'
          'notification TEXT'
          ')');
    });
  }

  createDB(String name) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'arwa.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE $name('
          'id INTEGER PRIMARY KEY,'
          '${name.toLowerCase()} TEXT'
          ')');
    });
  }

  createUser(String user) async {
    final db = await database;
    final res = await db.insert('User', {
      "id": 0,
      "user": user,
    });

    return res;
  }

  updateUser(String user) async {
    final db = await database;
    final res = await db.update(
        'User',
        {
          "id": 0,
          "user": user,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getUser() async {
    final db = await database;
    final res = await db.rawQuery("SELECT user FROM User WHERE id=0");

    return res[0];
  }

  Future<void> deleteUser() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'User',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  // HOME DB  on database
  createHome(String home) async {
    final db = await database;
    final res = await db.insert('Home', {
      "id": 0,
      "home": home,
    });

    return res;
  }

  Future<void> deleteHome() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Home',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateHome(String home) async {
    final db = await database;
    final res = await db.update(
        'Home',
        {
          "id": 0,
          "home": home,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getHome() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Home WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Category DB  on database
  createBrand(String brand) async {
    final db = await database;
    final res = await db.insert('Brand', {
      "id": 0,
      "brands": brand,
    });

    return res;
  }

  Future<void> deleteBrand() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Brand',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateBrands(String brand) async {
    final db = await database;
    final res = await db.update(
        'Brand',
        {
          "id": 0,
          "brands": brand,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getBrands() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Brand WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Products DB  on database
  createProducts(String brand) async {
    final db = await database;
    final res = await db.insert('Product', {
      "id": 0,
      "products": brand,
    });

    return res;
  }

  Future<void> deleteProducts() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Product',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateProducts(String products) async {
    final db = await database;
    final res = await db.update(
        'Product',
        {
          "id": 0,
          "products": products,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getProducts() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Product WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // History DB  on database
  createHistory(String history) async {
    final db = await database;
    final res = await db.insert('History', {
      "id": 0,
      "history": history,
    });

    return res;
  }

  Future<void> deleteHistory() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'History',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateHistory(String history) async {
    final db = await database;
    final res = await db.update(
        'History',
        {
          "id": 0,
          "history": history,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getHistory() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM History WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Offers DB  on database
  createOffers(String Offers) async {
    final db = await database;
    final res = await db.insert('Offer', {
      "id": 0,
      "offers": Offers,
    });

    return res;
  }

  Future<void> deleteOffers() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Offer',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateOffers(String Offers) async {
    final db = await database;
    final res = await db.update(
        'Offer',
        {
          "id": 0,
          "offers": Offers,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getOffers() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Offer WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Favourite DB  on database
  createFavourite(
    ProductModel favourite,
  ) async {
    final db = await database;
    final res = await db.insert('Favourite', {
      "id": favourite.id,
      "productid": favourite.productid,
      "productdesc": favourite.productdesc,
      "country": favourite.country,
      "brandcode": favourite.brandcode,
      "flavourcode": favourite.flavourcode,
      "packsize": favourite.packsize,
      "baseprice": favourite.baseprice,
      "discountprice": favourite.discountprice,
      "image": favourite.image,
      "producttype": favourite.producttype,
      "infavorite": favourite.infavorite
    });

    return res;
  }

  Future<void> deleteFavourite(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Favourite',
      // Use a `where` clause to delete a specific dog.
      where: 'id = ${id.toString()}',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateFavourite(ProductModel favourite) async {
    final db = await database;
    final res = await db.update(
        'Favourite',
        {
          "id": favourite.id,
          "productid": favourite.productid,
          "productdesc": favourite.productdesc,
          "country": favourite.country,
          "brandcode": favourite.brandcode,
          "flavourcode": favourite.flavourcode,
          "packsize": favourite.packsize,
          "baseprice": favourite.baseprice,
          "discountprice": favourite.discountprice,
          "image": favourite.image,
          "producttype": favourite.producttype,
          "infavorite": favourite.infavorite
        },
        where: 'id = ?',
        whereArgs: [favourite.id]);

    return res;
  }

  getFavouriteList() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Favourite");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return ProductModel.jsonToProductsList(res);
  }

  setFavouriteList(List<ProductModel> favProducts) {

    for(int i=0;i<favProducts.length;i++){
       createFavourite(favProducts[i]);

    }


  }
 deleteAllFavourite()async{
   final db = await database;
   final res = await db.rawQuery("DELETE FROM Favourite");

 }
  // Cart DB  on database
  createCart(String cart) async {
    final db = await database;
    final res = await db.insert('Cart', {
      "id": 0,
      "cart": cart,
    });

    return res;
  }

  Future<void> deleteCart() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Cart',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateCart(String cart) async {
    final db = await database;
    final res = await db.update(
        'Cart',
        {
          "id": 0,
          "cart": cart,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getCart() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Cart WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Address DB  on database
  createAddress(String address) async {
    final db = await database;
    final res = await db.insert('Address', {
      "id": 0,
      "address": address,
    });

    return res;
  }

  Future<void> deleteAddress() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Address',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateAddress(String address) async {
    final db = await database;
    final res = await db.update(
        'Address',
        {
          "id": 0,
          "address": address,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getAddress() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Address WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Cards DB  on database
  createCards(String cards) async {
    final db = await database;
    final res = await db.insert('Cards', {
      "id": 0,
      "cards": cards,
    });

    return res;
  }

  Future<void> deleteCards() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Cards',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateCards(String address) async {
    final db = await database;
    final res = await db.update(
        'Cards',
        {
          "id": 0,
          "cards": address,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getCards() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Cards WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  // Notification DB  on database
  createNotification(String notification) async {
    final db = await database;
    final res = await db.insert('Notification', {
      "id": 0,
      "notification": notification,
    });

    return res;
  }

  Future<void> deleteNotification() async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'Notification',
      // Use a `where` clause to delete a specific dog.
      where: 'id = 0',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      // whereArgs: [id],
    );
  }

  updateNotification(String notification) async {
    final db = await database;
    final res = await db.update(
        'Notification',
        {
          "id": 0,
          "notification": notification,
        },
        where: 'id = ?',
        whereArgs: [0]);

    return res;
  }

  getNotification() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Notification WHERE id=0");
    // final res = await db.rawQuery("SELECT home FROM Home WHERE id=0");

    return res;
  }

  deleteAllTableData() async {
    deleteUser();
    deleteAddress();
    deleteBrand();
    deleteCart();
    // deleteFavourite();
    deleteHistory();
    deleteHome();
    deleteOffers();
    deleteProducts();
    deleteNotification();
  }

  checkDataExistenceByLength(String table) async {
    final db = await database;
    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $table'))!;
    // await createDB(table);
    // int count=Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table'));
    // final res = await db.rawQuery('SELECT COUNT(*) FROM $table');
    return count;
  }
}
