import 'dart:io';

import 'package:arwa/AppModules/CartModule/ViewController/MyCartViewController.dart';
import 'package:arwa/AppModules/CartModule/ViewModel/cart_view_model.dart';
import 'package:arwa/AppModules/CategoriesModule/ViewController/AllCategoriesViewController.dart';
import 'package:arwa/AppModules/CategoriesModule/ViewController/BrandsListView.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/HomeModule/Services/home_service.dart';
import 'package:arwa/AppModules/HomeModule/View/Components/FloatingActionHome.dart';
import 'package:arwa/AppModules/HomeModule/View/Components/ViewALlBTN.dart';
import 'package:arwa/AppModules/HomeModule/View/CourselSliding.dart';
import 'package:arwa/AppModules/HomeModule/View/DiscountOfferTile.dart';
import 'package:arwa/AppModules/HomeModule/View/shimmer_view.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/home_view_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewController/MyFavouriteViewController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewModel/favourite_view_model.dart';
import 'package:arwa/AppModules/NotificationModule/ViewController/NotificationViewController.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/ViewController/OfferAndDealsViewController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductListType.dart';
import 'package:arwa/AppModules/ProductModule/View/ProductHorizentolListView.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ArwaAllProductListViewController.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ProductListView.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class HomeViewController extends StatefulWidget {
  const HomeViewController({Key? key}) : super(key: key);

  @override
  _HomeViewControllerState createState() => _HomeViewControllerState();
}

class _HomeViewControllerState extends State<HomeViewController> {
  final categoryController = Get.put(HomeCategoryController());
  final favVM = Get.put(FavProductViewModel());
  final homeVM = Get.put(HomeViewModel());
  final cartVM = Get.put(CartViewModel());
  Future<HomeModel>? fetchHomeData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("Home Init");
    // homeVM.getHomeData(context);
    favVM.getFavProductsData(context);
    fetchHomeData = homeVM.getHomeData(context);
    cartVM.getCartProductsData(context);
  }

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 322);
    return Obx(() => Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        // backgroundColor: Color(0xFFFFFFFF),
        floatingActionButton: HomeFloating(),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: SafeArea(
            child: Container(
              color: Colors.white,
              child: FutureBuilder<HomeModel>(
                  future: fetchHomeData,
                  // future: homeVM.getHomeData(context),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: HomeShimmerView(),
                      );
                    }

                    return Obx(() => Container(
                          color: Colors.white,
                          // color: Color(0xFFFFFFFF),
                          width: AppConfig(context).width,
                          height: AppConfig(context).height,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 307,
                                  width: AppConfig(context).width,
                                  child: Stack(
                                    children: [
                                      CarouselSliderHome(
                                        slider:
                                            homeVM.homeData.value.sliderList,
                                      ),
                                      // Image(
                                      //   // image: NetworkImage(homeVM.homeData.value.offersList[0].image),
                                      //   image: AssetImage(
                                      //       "assets/home/home_back.png"),
                                      //   width: AppConfig(context).width,
                                      //   height: 322,
                                      //   fit: BoxFit.fill,
                                      // ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            bottom: 50,
                                            left: AppConfig(context).width / 2 -
                                                (homeVM.homeData.value
                                                        .sliderList.length *
                                                    8)),
                                        child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: SizedBox(
                                            height: 10,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Expanded(
                                                  child: Obx(
                                                    () => ListView.builder(
                                                      shrinkWrap: true,
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      itemCount: homeVM
                                                          .homeData
                                                          .value
                                                          .sliderList
                                                          .length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      4),
                                                          child: Obx(
                                                            () => CircleAvatar(
                                                              backgroundColor: homeVM
                                                                          .currentSlider
                                                                          .value ==
                                                                      index
                                                                  ? Color(int.parse(
                                                                      "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                                                                  : Colors.grey
                                                                      .shade100,
                                                              radius: 4,
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 20),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                // Navigator.pop(context);
                                                Get.to(
                                                    () =>
                                                        NotificationViewController(),
                                                    transition:
                                                        Transition.rightToLeft);
                                              },
                                              child: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                radius: 20,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: SizedBox(
                                                    width: 30,
                                                    height: 30,
                                                    child: Stack(
                                                      alignment:
                                                          Alignment.topRight,
                                                      children: [
                                                        Image(
                                                          image: AssetImage(
                                                              "assets/home/notification-bing.png"),
                                                          width: 30,
                                                          height: 30,
                                                          fit: BoxFit.fill,
                                                        ),
                                                        CircleAvatar(
                                                          radius: 3,
                                                          backgroundColor:
                                                              Colors.pink,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                Get.to(
                                                    () =>
                                                        MyCartViewController(),
                                                    transition:
                                                        Transition.rightToLeft);
                                              },
                                              child: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                radius: 20,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: SizedBox(
                                                    width: 30,
                                                    height: 30,
                                                    child: Stack(
                                                      alignment:
                                                          Alignment.topRight,
                                                      children: [
                                                        Image(
                                                          image: AssetImage(
                                                              "assets/home/cart.png"),
                                                          width: 30,
                                                          height: 30,
                                                          fit: BoxFit.fill,
                                                        ),
                                                        Obx(
                                                          () => CircleAvatar(
                                                            radius: 3,
                                                            backgroundColor: cartVM
                                                                    .getCartData
                                                                    .value
                                                                    .cartList
                                                                    .isNotEmpty
                                                                ? Colors.pink
                                                                : Colors
                                                                    .transparent,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),

                                      Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                            height: 30,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(30),
                                                  topLeft: Radius.circular(30)),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    // color: Colors.white,
                                    // color: Color(0xFFFFFFFF),
                                    // borderRadius: BorderRadius.only(
                                    //     topRight: Radius.circular(30),
                                    //     topLeft: Radius.circular(30)),
                                  ),
                                  width: AppConfig(context).width,
                                  // height: AppConfig(context).height,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          children: [
                                            AppText.customText(
                                                title: "Brands",
                                                size: 18,
                                                color: AppColor.DarkText,
                                                fontWeight:
                                                    FontWeights.semi_bold),
                                            Spacer(),
                                            ViewAllBTN(
                                              callBack: () {
                                                Get.to(
                                                  () =>
                                                      AllCategoryViewController(
                                                    // brandsList: homeVM.homeData
                                                    //     .value.brandsList,
                                                    isSecondary: true,
                                                  ),
                                                  transition:
                                                      Transition.rightToLeft,
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      BrandsListView(
                                        brandList:
                                            homeVM.homeData.value.brandsList,
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          children: [
                                            AppText.customText(
                                                title: "Deals and Discounts",
                                                size: 16,
                                                color: AppColor.DarkText,
                                                fontWeight:
                                                    FontWeights.semi_bold),
                                            Spacer(),
                                            ViewAllBTN(
                                              callBack: () {
                                                Get.to(
                                                  () =>
                                                      OfferAndDealsViewController(
                                                    inner: true,
                                                  ),
                                                  transition:
                                                      Transition.rightToLeft,
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      DiscountOfferView(
                                        offers:
                                            homeVM.homeData.value.offersList,
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Obx(
                                        () => ProductVerticleListView(
                                          productsList: homeVM
                                                  .homeData.value.brandsList
                                                  .where((element) =>
                                                      categoryController
                                                          .selectedBrand
                                                          .value
                                                          .id ==
                                                      element.id)
                                                  .isNotEmpty
                                              ? homeVM.homeData.value.brandsList
                                                  .where((element) =>
                                                      categoryController
                                                          .selectedBrand
                                                          .value
                                                          .id ==
                                                      element.id)
                                                  .first
                                                  .products
                                              : [],
                                          productListType:
                                              ProductListType.allProducts1,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          children: [
                                            AppText.customText(
                                                title: "New Arrivals",
                                                size: 16,
                                                color: AppColor.DarkText,
                                                fontWeight:
                                                    FontWeights.semi_bold),
                                            Spacer(),
                                            ViewAllBTN(
                                              callBack: () {
                                                Get.to(
                                                    () => ArwaAllProductListViewController(
                                                        brandCode:
                                                            categoryController
                                                                .selectedBrand
                                                                .value
                                                                .brandcode,
                                                        products:
                                                            categoryController
                                                                .selectedBrand
                                                                .value
                                                                .products),
                                                    transition:
                                                        Transition.rightToLeft);
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Obx(
                                        () => ProductVerticleListView(
                                          isFavourite: false,
                                          isNewArival: true,
                                          productsList: homeVM
                                                  .homeData.value.brandsList
                                                  .where((element) =>
                                                      categoryController
                                                          .selectedBrand
                                                          .value
                                                          .id ==
                                                      element.id)
                                                  .isNotEmpty
                                              ? homeVM.homeData.value.brandsList
                                                  .where((element) =>
                                                      categoryController
                                                          .selectedBrand
                                                          .value
                                                          .id ==
                                                      element.id)
                                                  .first
                                                  .newArrivalsProducts
                                              : [],
                                          productListType:
                                              ProductListType.allProducts1,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          children: [
                                            AppText.customText(
                                                title: "Favorite",
                                                size: 18,
                                                color: AppColor.DarkText,
                                                fontWeight:
                                                    FontWeights.semi_bold),
                                            Spacer(),
                                            ViewAllBTN(
                                              callBack: () {
                                                Get.to(
                                                    () =>
                                                        MyFavouriteViewController(),
                                                    transition:
                                                        Transition.rightToLeft);

                                                // Get.to(
                                                //     () =>
                                                //         ArwaAllProductListViewController(
                                                //           brandCode: "",
                                                //           products: homeVM
                                                //               .homeData
                                                //               .value
                                                //               .favouriteList,
                                                //         ),
                                                //     transition:
                                                //         Transition.rightToLeft);
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      // Obx(
                                      //   () =>
                                      ProductHorizentolListView(
                                        isFavourite: true,
                                        isNewArival: false,
                                        productsList:
                                            favVM.favProductsData.value,
                                      ),
                                      // ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ));
                  }),
            ),
          ),
        )));
  }
}
