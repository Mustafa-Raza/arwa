import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class HomeCategoryController extends GetxController {
  var selectedBrand = BrandsModel(
    image: "",
    brandcode: "",
    colorcode: "#FF0759AA",
newArrivalsProducts: [],
    products: [],
    display: "",
    id: 0,
    namedb: "",
    priority: 0,

  ).obs;

}
