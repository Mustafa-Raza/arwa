import 'dart:convert';

import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/HomeModule/Services/home_service.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class HomeViewModel extends GetxController {
  final categoryController = Get.put(HomeCategoryController());
  var homeData = HomeModel(
      brandsList: [], favouriteList: [], sliderList: [], offersList: []).obs;
  RxInt currentSlider = 0.obs;
  RxBool onUnauthorized = false.obs;

  Future<HomeModel> getHomeData(BuildContext context) async {
    HomeModel homeModel = HomeModel(
        brandsList: [], favouriteList: [], sliderList: [], offersList: []);
    if ((await DBProvider.db.checkDataExistenceByLength("Home")) == 0) {
      homeModel = await getHomeDataService(
          context: context,
          onUnauthorized: (value) {
            onUnauthorized.value = value;
            if(value)
            Get.offAll(()=>LoginViewController());
          });
    } else {
      print("Data is fetched in from local");
      //
      var localData = await DBProvider.db.getHome();
      // String value=jsonEncode(localData[0]["home"]);
      print(jsonDecode(localData[0]["home"])["data"]);

      homeModel = HomeModel.fromJson(jsonDecode(localData[0]["home"])["data"]);

      getHomeDataService(
          context: context,
          onUnauthorized: (value) {
            onUnauthorized.value = value;
            if(value)
              Get.offAll(()=>LoginViewController());
          }).then((value) => {
            if (onUnauthorized.value != true)
              {
                print("Data is fetched in background"),
                if (value.brandsList.isNotEmpty) homeData.value = value,
              }

            // homeModel = value,
          });
    }

    homeData.value = homeModel;
    if (homeModel.brandsList.isNotEmpty &&
        categoryController.selectedBrand.value.brandcode == "")
      categoryController.selectedBrand.value = homeModel.brandsList[0];
    return homeModel;
  }
  Future<HomeModel> getUpdatedHomeData(BuildContext context) async {
    HomeModel homeModel = HomeModel(
        brandsList: [], favouriteList: [], sliderList: [], offersList: []);
    // if ((await DBProvider.db.checkDataExistenceByLength("Home")) == 0) {
    //   homeModel = await getHomeDataService(
    //       context: context,
    //       onUnauthorized: (value) {
    //         onUnauthorized.value = value;
    //         if(value)
    //         Get.offAll(()=>LoginViewController());
    //       });
    // } else {
    //   print("Data is fetched in from local");
    //   //
    //   var localData = await DBProvider.db.getHome();
    //   // String value=jsonEncode(localData[0]["home"]);
    //   print(jsonDecode(localData[0]["home"])["data"]);
    //
    //   homeModel = HomeModel.fromJson(jsonDecode(localData[0]["home"])["data"]);
    //
    //   getHomeDataService(
    //       context: context,
    //       onUnauthorized: (value) {
    //         onUnauthorized.value = value;
    //         if(value)
    //           Get.offAll(()=>LoginViewController());
    //       }).then((value) => {
    //         if (onUnauthorized.value != true)
    //           {
    //             print("Data is fetched in background"),
    //             if (value.brandsList.isNotEmpty) homeData.value = value,
    //           }
    //
    //         // homeModel = value,
    //       });
    // }
    homeModel = await getHomeDataService(
        context: context,
        onUnauthorized: (value) {
          onUnauthorized.value = value;
          if(value)
            Get.offAll(()=>LoginViewController());
        });
    homeData.value = homeModel;
    if (homeModel.brandsList.isNotEmpty &&
        categoryController.selectedBrand.value.brandcode == "")
      categoryController.selectedBrand.value = homeModel.brandsList[0];
    return homeModel;
  }

  onAddToFav({required BuildContext context,required int index,required int productID}){

  }


}
