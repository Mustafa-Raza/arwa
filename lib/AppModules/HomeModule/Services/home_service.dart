import 'dart:convert';

import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<HomeModel> getHomeDataService({
  required BuildContext context,
   Function ?onUnauthorized,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: HOME_GET_API + "${user["customerid"]}", context: context);
  if(response==null){
    return HomeModel(brandsList: [], sliderList: [], offersList: [], favouriteList: []);
  }else
  if (jsonDecode(response.body)["status"] == 200) {
    print("Get Home Data");
    print(HomeModel.fromJson(jsonDecode(response.body)["data"]));

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Home")) == 0) {
      print("Home Inserted to SQL");
      await DBProvider.db.createHome(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getHome());
    } else {
      print("Home Updated to SQL");
      await DBProvider.db.updateHome(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getHome());
    }

    return HomeModel.fromJson(jsonDecode(response.body)["data"]);
  } else if (jsonDecode(response.body)["status"] == 401) {
    ShowMessage().showErrorMessage(context, "User is Unauthorized");
    onUnauthorized!(true);
    return HomeModel(
        brandsList: [], favouriteList: [], sliderList: [], offersList: []);
  } else {
    return HomeModel(
        brandsList: [], favouriteList: [], sliderList: [], offersList: []);
  }
}
