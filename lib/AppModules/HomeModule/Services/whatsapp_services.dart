
import 'dart:io';

import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

openWhatsapp({required BuildContext context}) async{
  var whatsapp ="+923099899248";
  var whatsappURl_android = "whatsapp://send?phone="+whatsapp+"&text=hello";
  var whatappURL_ios ="https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
  if(Platform.isIOS){
    // for iOS phone only
    if( await canLaunch(whatappURL_ios)){
      await launch(whatappURL_ios, forceSafariVC: false);
    }else{

      ShowMessage().showErrorMessage(context, "Whatsapp is not installed");
    }

  }else{
    // android , web
    if( await canLaunch(whatsappURl_android)){
      await launch(whatsappURl_android);
    }else{
      ShowMessage().showErrorMessage(context, "Whatsapp is not installed");


    }


  }

}
