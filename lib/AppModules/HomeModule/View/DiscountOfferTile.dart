import 'package:arwa/AppModules/HomeModule/View/Components/DiscountOfferTle.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class DiscountOfferView extends StatelessWidget {
final List<OfferModel> offers;  DiscountOfferView({Key? key,required this.offers}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 88,
      // color: Colors.red,

      child: ListView.builder(
        itemCount: offers.length,
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
              padding: const EdgeInsets.only(right: 3,left: 20),
              child: DiscountOfferTile(index: index,offerModel: offers[index],)
          );
        },
      ),
    );
  }
}
