import 'package:arwa/AppModules/HomeModule/Services/whatsapp_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeFloating extends StatelessWidget {
  const HomeFloating({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 50,

      child:
      FloatingActionButton(
     // focusColor: Colors.red,
          backgroundColor: Colors.white,
heroTag: Image(
  image: AssetImage("assets/home/whatsapp.png"),
  height: 54,
  width: 54,
  fit: BoxFit.cover,
),
          onPressed: () {
            openWhatsapp(context: context);
          },
          child: Center(
            child: Image(
              image: AssetImage("assets/home/whatsapp.png"),
              height: 54,
              width: 54,
              fit: BoxFit.cover,
            ),
          )),
    );
  }
}
