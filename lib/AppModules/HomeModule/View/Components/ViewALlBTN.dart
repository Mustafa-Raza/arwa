import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ViewAllBTN extends StatelessWidget {
  final VoidCallback callBack;

  ViewAllBTN({Key? key, required this.callBack}) : super(key: key);
  final categoryController=Get.put(HomeCategoryController());
  @override
  Widget build(BuildContext context) {
    return InkWell(

      onTap: () {
        callBack();
      },
      child: SizedBox(
        // width: 67,
        child: Row(
          children: [
            AppText.customText(
                title: "View All",
                size: 12,
                fontWeight: FontWeights.regular,
                color: AppColor.greyText),
            SizedBox(
              width: 5,
            ),
           Obx(()=> Container(
              width: 16,
              height: 16,
              decoration: BoxDecoration(
                color:Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                // color: AppColor.homeGreen,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Image(
                  image: AssetImage("assets/home/arrow-right.png"),
                  width: 8,
                  height: 8,
                ),
              ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}
