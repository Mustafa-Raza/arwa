import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DiscountOfferTile extends StatelessWidget {
  final int index;
  final OfferModel offerModel;

  DiscountOfferTile({Key? key, required this.index, required this.offerModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        // image: DecorationImage(
        //     image: NetworkImage( offerModel.image),
        //     fit: BoxFit.fill),
      ),
      width: 229,
      height: 88,
      child: ClipRRect(borderRadius: BorderRadius.circular(13),child: cacheImageView(image:  offerModel.image,circlularPadding: 50)),
      // Padding(
      //   padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 10),
      //   child: Column(
      //     crossAxisAlignment: CrossAxisAlignment.start,
      //     children: [
      //       // AppText.customText(
      //       //     title: "${offerModel.dealText}",
      //       //     size: 14,
      //       //     color: AppColor.white,
      //       //     fontWeight: FontWeights.regular),
      //       // SizedBox(height: 5,),
      //       // Row(
      //       //   children: [
      //       //     AppText.customText(
      //       //         title: "15% OFF",
      //       //         size: 14,
      //       //         color: AppColor.white,
      //       //         fontWeight: FontWeights.medium),
      //       //     SizedBox(width: 6,),
      //       //     // Image(image: AssetImage("assets/home/arrowCircle.png"),height: 15,width: 15,)
      //       //   ],
      //       // ),
      //       // SizedBox(height: 5,),
      //       // AppText.customText(
      //       //     title: "* No Coupon Required",
      //       //     size: 12,
      //       //     color: AppColor.white,
      //       //     fontWeight: FontWeights.regular),
      //     ],
      //   ),
      // ),
    );
  }
}
