import 'dart:ui';

import 'package:arwa/AppModules/HomeModule/Model/slider_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/home_view_model.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/image/gf_image_overlay.dart';

class CarouselSliderHome extends StatefulWidget {
  final List<SliderModel> slider;

  CarouselSliderHome({Key? key, required this.slider}) : super(key: key);

  @override
  _CarouselSliderHomeState createState() => _CarouselSliderHomeState();
}

class _CarouselSliderHomeState extends State<CarouselSliderHome> {
  final homeVM = Get.put(HomeViewModel());

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 322,
        // aspectRatio: 16 / 9,
        // viewportFraction: 0.8,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        // enlargeCenterPage: true,
        // onPageChanged: callbackFunction,
        onPageChanged: (index, reason) {
          print(index);
          setState(() {
            homeVM.currentSlider.value = index;
          });
          print(homeVM.currentSlider.value);
        },
        aspectRatio: 2.0,
        disableCenter: true,
        viewportFraction: 1,
        enlargeCenterPage: false,
        scrollDirection: Axis.horizontal,
      ),
      items: widget.slider.map((sale) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: MediaQuery.of(context).size.width,
              // decoration: BoxDecoration(
              //     // color: AppColor.primaryColor.withOpacity(0.5),
              //     image: DecorationImage(
              //   image: NetworkImage(sale.image),
              //   fit: BoxFit.fill,
              // )),
              child: cacheImageView(
                  image: sale.image,
                  circlularPadding: AppConfig(context).width / 2.5),
            );
          },
        );
      }).toList(),
    );
  }
}
