class SliderModel{
  int id;
  String image;
  SliderModel({
    required this.image,required this.id
});
  factory SliderModel.fromJson(Map<String, dynamic> json) =>
      SliderModel(
        id: json["id"],
        image: json["image"],

      );
  static List<SliderModel> jsonToSliderList(List<dynamic> emote) =>
      emote.map<SliderModel>((item) => SliderModel.fromJson(item)).toList();
}