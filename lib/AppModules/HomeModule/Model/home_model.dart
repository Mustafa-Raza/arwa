import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/HomeModule/Model/slider_model.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';

class HomeModel {
  List<BrandsModel> brandsList;
  List<SliderModel> sliderList;
  List<OfferModel> offersList;
  List<ProductModel> favouriteList;


  HomeModel(
      {required this.brandsList,
      required this.sliderList,
      required this.offersList,
      required this.favouriteList});

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
        brandsList: BrandsModel.jsonToBrandList(json["brands"]),
        sliderList: SliderModel.jsonToSliderList(json["sliders"]),
        offersList: OfferModel.jsonToOfferList(json["offers"]),
    favouriteList: ProductModel.jsonToProductsList(json["favorite"]),

      );
}
