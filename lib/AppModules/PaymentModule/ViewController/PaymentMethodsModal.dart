
import 'package:arwa/AppModules/PaymentModule/ViewController/AddCardViewController.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future paymentMethodModalView({
  required BuildContext context,
}) {
  return showModalBottomSheet(
    backgroundColor: Colors.white.withOpacity(0),
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return Container(
            // height: 500,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15), topLeft: Radius.circular(15)),
            ),
            child: PaymentMethodModalTile(),
          );
        },
      );
    },
  );
}

class PaymentMethodModalTile extends StatefulWidget {
  const PaymentMethodModalTile({Key? key}) : super(key: key);

  @override
  _PaymentMethodModalTileState createState() => _PaymentMethodModalTileState();
}

class _PaymentMethodModalTileState extends State<PaymentMethodModalTile> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 350,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image(
                    image: AssetImage("assets/paymentAndAddress/cancel.png"),
                    height: 15,
                    width: 15,
                  ),
                ),
                Spacer(),
                InkWell(
                    child: AppText.customText(
                  title: "Select Payment method",
                  color: AppColor.DarkText,
                  fontWeight: FontWeights.semi_bold,
                  size: 16,
                )),
                Spacer(),
              ],
            ),
          ),

          Divider(
            endIndent: 20,
            indent: 20,
            height: 1,
            color: Colors.grey.shade300,
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 4,
                      ),
                      Image(
                        image:
                            AssetImage("assets/paymentAndAddress/Oval.png"),
                        height: 16,
                        width: 16,
                        color: AppColor. greyText ,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText.customText(
                        title: "Master Card ",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.regular,
                        size: 16,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image(
                            image: AssetImage(
                                "assets/paymentAndAddress/visa.png"),
                            height: 18,
                            // width: 18,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          AppText.customText(
                            title: "**** 5743",
                            color: AppColor.textGREY,
                            fontWeight: FontWeights.regular,
                            size: 14,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Spacer(),
                  Image(
                    image: AssetImage("assets/paymentAndAddress/Edit.png"),
                    height: 16,
                    width: 16,
                    color: AppColor.primaryColor,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Divider(
            height: 1,
            color: Colors.grey.shade300,
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: InkWell(
              onTap: (){
                Get.to(()=>AddCardViewController());
              },
              child: Container(
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: 4,
                        ),
                        Image(
                          image:
                              AssetImage("assets/paymentAndAddress/Oval.png"),
                          height: 16,
                          width: 16,
                          color: AppColor.greyText,
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.customText(
                          title: "Credit or debit card",
                          color: AppColor.DarkText,
                          fontWeight: FontWeights.regular,
                          size: 16,
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image(
                              image: AssetImage(
                                  "assets/paymentAndAddress/visa1.png"),
                              height: 18,
                              // width: 18,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Image(
                              image: AssetImage(
                                  "assets/paymentAndAddress/visa.png"),
                              height: 18,
                              // width: 18,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Spacer(),
                    Image(
                      image:
                          AssetImage("assets/paymentAndAddress/Deliverto.png"),
                      height: 16,
                      width: 16,
                      color: AppColor.primaryColor,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Divider(
            height: 1,
            color: Colors.grey.shade300,
          ),
          SizedBox(
            height:  25,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              // height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: Image(
                      image: AssetImage("assets/paymentAndAddress/Oval.png"),
                      height: 16,
                      width: 16,
                      color: AppColor.primaryColor,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  AppText.customText(
                    title: "Cash on delivery",
                    color: AppColor.DarkText,
                    fontWeight: FontWeights.regular,
                    size: 16,
                  ),
                  Spacer(),
                  Image(
                    image:
                        AssetImage("assets/paymentAndAddress/Deliverto.png"),
                    height: 16,
                    width: 16,
                    color: AppColor.primaryColor,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                ],
              ),
            ),
          ),
          // SizedBox(
          //   height: 30,
          // ),
          // Divider(
          //   height: 1,
          //   color: Colors.grey.shade300,
          // ),
          // SizedBox(
          //   height: 30,
          // ),
        ],
      ),
    );
  }
}
