import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/AddCardField.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/AddCardTile.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/MyCardTile.dart';
import 'package:arwa/AppModules/PaymentModule/View/credit_card_validator.dart';
import 'package:arwa/AppModules/PaymentModule/ViewModel/add_card_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/ViewModel/payment_view_model.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:credit_card_validator/credit_card_validator.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AddCardViewController extends StatefulWidget {
  final bool isMyCard;

  AddCardViewController({
    Key? key,
    this.isMyCard = false,
  }) : super(key: key);

  @override
  _AddCardViewControllerState createState() => _AddCardViewControllerState();
}

class _AddCardViewControllerState extends State<AddCardViewController> {
  final addCardVM = Get.put(AddCardViewModel());
  final myCardVM = Get.put(PaymentViewModel());
  final categoryController = Get.put(HomeCategoryController());
  FocusNode dateFocusNode = FocusNode();
  FocusNode nameFocusNode = FocusNode();
  FocusNode numberFocusNode = FocusNode();
  FocusNode cvvFocusNode = FocusNode();
  TextEditingController dateController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController cvvController = TextEditingController();

  getCreditCardType(String value) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            height: AppConfig(context).height,
            width: AppConfig(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.isMyCard ? 20 : 20),
                    child: InnerAppBar(
                      title: widget.isMyCard
                          ? "My Card"
                          : "Add Credit or Debit Card",
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 39,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child:
                                // widget.isMyCard
                                // ? MyCardTile(cardModel: widget.myCards!)
                                // :
                                AddCardTile(),
                          ),
                          SizedBox(
                            height: 39,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child:
                                // Obx(
                                //   () =>
                                AddCardInputField(
                              controller: nameController,
                              width: double.infinity,
                              hint: "Enter Card Holder",
                              label: "Name on Card",
                              focusNode: nameFocusNode,
                              onChange: (value) {
                                addCardVM.onUpdateCardHolder(value);
                              },
                            ),
                            // ),
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child:
                                Obx(
                                  () =>
                                AddCardInputField(
                              width: double.infinity,
                              controller: numberController,
                              hint: "Enter Card Number",
                              label: "Card Number",
                              inputType: TextInputType.number,
                              focusNode: numberFocusNode,
                              isImgIcon: true,
                              cardValue:addCardVM.cardNumber.value ,
                              onChange: (value) {
                                // CreditCardValidationBloc().validateCard(value);
                                addCardVM.onUpdateCardNumber(value);
                              },
                              isCreditCard: true,
                              imgIcon:
                                  "assets/paymentAndAddress/Icon_MasterCard.png",
                            ),
                            ),
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // Obx(
                                //   () =>
                                AddCardInputField(
                                  focusNode: dateFocusNode,
                                  controller: dateController,
                                  width: 144,
                                  hint: 'MM/YYYY',
                                  inputType: TextInputType.number,
                                  onChange: (value) {
                                    setState(() {
                                      value =
                                          value.replaceAll(RegExp(r"\D"), "");
                                      switch (value.length) {
                                        case 0:
                                          dateController.text = "MM/YYYY";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 0);
                                          break;
                                        case 1:
                                          dateController.text =
                                              "${value}M/YYYY";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 1);
                                          break;
                                        case 2:
                                          dateController.text = "$value/YYYY";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 2);
                                          break;
                                        case 3:
                                          dateController.text =
                                              "${value.substring(0, 2)}/${value.substring(2)}YYY";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 4);
                                          break;
                                        case 4:
                                          dateController.text =
                                              "${value.substring(0, 2)}/${value.substring(2, 4)}YY";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 5);
                                          break;
                                        case 5:
                                          dateController.text =
                                              "${value.substring(0, 2)}/${value.substring(2, 5)}Y";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 6);
                                          break;
                                        case 6:
                                          dateController.text =
                                              "${value.substring(0, 2)}/${value.substring(2, 6)}";
                                          addCardVM.cardExpiry.value =
                                              "${value.substring(0, 2)}/${value.substring(2, 6)}";
                                          dateController.selection =
                                              TextSelection.collapsed(
                                                  offset: 7);
                                          break;
                                      }
                                      if (value.length > 6) {
                                        dateController.text =
                                            "${value.substring(0, 2)}/${value.substring(2, 6)}";
                                        addCardVM.cardExpiry.value =
                                            "${value.substring(0, 2)}/${value.substring(2, 6)}";

                                        dateController.selection =
                                            TextSelection.collapsed(offset: 7);
                                      }

                                      print(addCardVM.cardExpiry.value);
                                    });
                                    // addCardVM.onUpdateCardExpiry(value);
                                  },
                                  label: "Expiry Date",
                                  isImgIcon: false,
                                  imgIcon: "assets/paymentAndAddress/visa.png",
                                ),
                                // ),
                                // Obx(
                                //   () =>
                                AddCardInputField(
                                  focusNode: cvvFocusNode,
                                  width: 144,
                                  inputType: TextInputType.number,
                                  hint: "***",
                                  label: "CVV",
                                  controller: cvvController,
                                  onChange: (value) {
                                    addCardVM.onUpdateCardCVV(value);
                                  },
                                  isImgIcon: false,
                                  imgIcon: "assets/paymentAndAddress/visa.png",
                                ),
                                // ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 54,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child: Obx(
                              () => addCardVM.loading.value
                                  ? spinKitButton(
                                      context,
                                      46,
                                      widget.isMyCard
                                          ? AppConfig(context).width - 40
                                          : AppConfig(context).width - 60,
                                  Color(int.parse(
                                      "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                                    )
                                  : primaryButton(
                                      buttonTitle: widget.isMyCard
                                          ? "Save Card"
                                          : "Continue",
                                      callback: () async {
                                        bool result = await addCardVM.onAddCard(
                                            context: context);
                                        if (result) {
                                          if (widget.isMyCard)
                                            myCardVM.getCardsData(context);
                                          Get.back();
                                        }
                                      },
                                      width: AppConfig(context).width,
                                      btnColor: Color(int.parse(
                                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                                      txtColor: AppColor.white,
                                      height: 46,
                                      fontweight: FontWeights.medium,
                                    ),
                            ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: widget.isMyCard ? 20 : 30),
                            child: primaryButton(
                              buttonTitle: "Cancel",
                              callback: () {
                                Get.back();
                              },
                              width: AppConfig(context).width,
                              btnColor: AppColor.white,
                              txtColor: AppColor.DarkText,
                              height: 46,
                              fontweight: FontWeights.medium,
                            ),
                          ),
                          SizedBox(
                            height: 35,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
