
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/MyCardTile.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/AddCardViewController.dart';
import 'package:arwa/AppModules/PaymentModule/ViewModel/payment_view_model.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyCardsViewController extends StatefulWidget {
  const MyCardsViewController({Key? key}) : super(key: key);

  @override
  _MyCardsViewControllerState createState() => _MyCardsViewControllerState();
}

class _MyCardsViewControllerState extends State<MyCardsViewController> {

  final cardVM=Get.put(PaymentViewModel());
  Future<List<CardModel>>? getCards;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    cardVM.getCardsData(context);
    getCards=cardVM.getCardsData(context);
  }
  final categoryController = Get.put(HomeCategoryController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: primaryButton(
              buttonTitle: "Add Card",
              callback: () {
                Get.to(()=> AddCardViewController(isMyCard: true,),transition: Transition.rightToLeft);
                // Navigator.pushReplacement(
                //     context,
                //     MaterialPageRoute(
                //       builder: (context) => AddCardViewController(isMyCard: true,),
                //     ));
              },
              width: AppConfig(context).width - 60,
              btnColor: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              txtColor: AppColor.white,
              height: 46,
              fontweight: FontWeights.medium,
            ),
          ),
        ),
        backgroundColor:Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20,),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 20,),
                  child: InnerAppBar(title: "My Cards"),
                ),
                Expanded(
                  child
                      : Padding(
                    padding:   const EdgeInsets.symmetric(horizontal: 20),
                    child: FutureBuilder<List<CardModel>>(
                      // future: productVM.getProductsData(context,widget.brandCode),
                        future: getCards,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Center(
                              child: FavShimmerView(),
                            );
                          }
                        return Obx(()=> cardVM.cardsData.isNotEmpty? ListView.builder(
                          itemCount: cardVM.cardsData.value.length,
                          // physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return Padding(
                                padding: const EdgeInsets.only(bottom: 29),
                                child: MyCardTile(cardModel:  cardVM.cardsData.value[index]));
                          },
                        ):Center(child:  AppText.customText(
                        title: "No Cards Found",
                            color: AppColor.primaryColor,
                            size: 18),),
                        );
                      }
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
