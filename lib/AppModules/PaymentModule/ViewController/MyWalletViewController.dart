import 'package:arwa/AppModules/HomeModule/View/Components/ViewALlBTN.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/PaymentModule/View/PaymentCardListView.dart';
import 'package:arwa/AppModules/PaymentModule/View/RecentPaymentListView.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppBar/SecondaryAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';

class MyWalletViewController extends StatefulWidget {
final  bool isPrimary; MyWalletViewController({Key? key,this.isPrimary=true}) : super(key: key);

  @override
  _MyWalletViewControllerState createState() => _MyWalletViewControllerState();
}

class _MyWalletViewControllerState extends State<MyWalletViewController> {
  final categoryController = Get.put(HomeCategoryController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
      height: AppConfig(context).height,
      width: AppConfig(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
      child: Column(
        children: [
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: widget.isPrimary?SecondaryAppBar(
              title: "My Wallet",
            ):InnerAppBar(
              title: "My Wallet",
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [


                  SizedBox(
                    height: 29,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: DottedBorder(
                        color: AppColor.textGREY,
                        borderType: BorderType.RRect,
                        dashPattern: [10, 6],
                        radius: Radius.circular(10),
                        strokeCap: StrokeCap.square,
                        strokeWidth: 1,
                        child: Container(
                          height: 110,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color(0xFFF3F7F8),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 15,left: 20),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 19,
                                    ),
                                    AppText.customText(
                                        title: "Available Balance",
                                        color: Color(0xFF999B9F),
                                        size: 14,
                                        fontWeight: FontWeights.regular),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    AppText.customText(
                                        title: "AED 78.26",
                                        color: AppColor.primaryColor,
                                        size: 30,
                                        fontWeight: FontWeights.semi_bold),
                                  ],
                                ),
                                Expanded(
                                  child
                                      : Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [

                                          Image(
                                            image: AssetImage("assets/wallet/card-add.png"),
                                            height: 18,
                                            width: 18,color: AppColor.primaryColor,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          AppText.customText(
                                              title: "Top up",
                                              color: AppColor.primaryColor,
                                              size: 14,
                                              fontWeight: FontWeights.semi_bold),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: AppText.customText(
                        title: "Payment Cards",
                        color: AppColor.DarkText,
                        size: 16,
                        fontWeight: FontWeights.semi_bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: PaymentCardListView(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        AppText.customText(
                            title: "Recent Activities",
                            size: 16,
                            color: AppColor.DarkText,
                            fontWeight: FontWeights.semi_bold),
                        Spacer(),
                        ViewAllBTN(
                          callBack: () {},
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(
                  //   height: 20,
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: RecentPaymentListView(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
        ));
  }
}
