class PaymentListModel{
  String img;
  String title;
  String description;
  PaymentListModel({required this.title,required this.img,required this.description});
}


class PaymentCardModel{
  String img;
  String title;
  String cardNumber;
  bool isSelected;
  PaymentCardModel({required this.title,required this.img,required this.cardNumber,required this.isSelected});
}