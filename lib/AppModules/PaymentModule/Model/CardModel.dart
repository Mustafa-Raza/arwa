
class CardModel {
  CardModel({
  required  this.id,
    required   this.customerid,
    required  this.cardholder,
    required  this.cardnumber,
    required   this.expiry,
    required   this.cvv,
    required   this.cardtype,
    required  this.cardvendor,
    required  this.cardcolor,
    required  this.creationtime,
    required  this.cardlogo,
  });

  int id;
  String customerid;
  String cardholder;
  int cardnumber;
  String expiry;
  int cvv;
  String cardtype;
  String cardvendor;
  String cardcolor;
  String creationtime;
  String cardlogo;

  factory CardModel.fromJson(Map<String, dynamic> json) => CardModel(
    id: json["id"],
    customerid: json["customerid"],
    cardholder: json["cardholder"],
    cardnumber: json["cardnumber"],
    expiry: json["expiry"],
    cvv: json["cvv"],
    cardtype: json["cardtype"]??"",
    cardvendor: json["cardvendor"]??"",
    cardcolor: json["cardcolor"]??"",
    creationtime: json["creationtime"],
    cardlogo: json["cardlogo"],
  );
  static List<CardModel> jsonToCardsList(List<dynamic> emote) =>
      emote.map<CardModel>((item) => CardModel.fromJson(item)).toList();

  Map<String, dynamic> toJson() => {
    "id": id,
    "customerid": customerid,
    "cardholder": cardholder,
    "cardnumber": cardnumber,
    "expiry": expiry,
    // "expiry": "${expiry.year.toString().padLeft(4, '0')}-${expiry.month.toString().padLeft(2, '0')}-${expiry.day.toString().padLeft(2, '0')}",
    "cvv": cvv,
    "cardtype": cardtype,
    "cardvendor": cardvendor,
    "cardcolor": cardcolor,
    "creationtime": creationtime,
    "cardlogo": cardlogo,
  };

}
String splitStringByLength(String str, int lengthSpacing) {
  String value = "";
  int length = 0;
  for (int i = 0; i < str.length; i++) {
    if (length == lengthSpacing-1) {
      value = value + str[i] + "  ";
      length = 0;
    } else {
      value = value + str[i];
      length++;
    }
  }
  return value;
}
