import 'package:arwa/AppModules/PaymentModule/Services/payment_services.dart';
import 'package:arwa/AppModules/PaymentModule/View/credit_card_validator.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AddCardViewModel extends GetxController {
  RxString cardHolder = "".obs;
  RxString cardNumber = "".obs;
  RxString cardCVV = "".obs;
  RxString cardExpiry = "".obs;
  RxBool loading = false.obs;

  Future<bool> onAddCard({required BuildContext context}) async {
    if (cardHolder.value == "") {
      ShowMessage().showErrorMessage(context, "Card Holder Name is Required");
      return false;
    } else if (cardNumber.value == "") {
      ShowMessage().showErrorMessage(context,"Card Number  is Required");
      return false;
    } else if (cardCVV.value == "") {
      ShowMessage().showErrorMessage(context, "CVV Number  is Required");
      return false;
    } else if (cardExpiry.value == "") {
      ShowMessage().showErrorMessage(context,"Card Expiry is Required");
      return false;
    } else {
      loading.value = !loading.value;
      bool result = await addCardService(
          context: context,
          cardholder: cardHolder.value,
          cardnumber: cardNumber.value,
          cvv: cardCVV.value,
          expiry: cardExpiry.value,
          cardvendor:CreditCardValidationBloc().getCreditCardType( cardNumber.value,));
      loading.value = !loading.value;
      return result;
    }
  }

  onUpdateCardHolder(String value) {
    cardHolder.value = value;
  }

  onUpdateCardNumber(String value) {
    cardNumber.value = value;
  }

  onUpdateCardCVV(String value) {
    cardCVV.value = value;
  }

  onUpdateCardExpiry(String value) {
    cardExpiry.value = value;
  }
}
