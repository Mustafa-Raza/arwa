import 'dart:convert';

import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/PaymentModule/Services/payment_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class PaymentViewModel extends GetxController {
  var cardsData = [].obs;

  Future<List<CardModel>> getCardsData(BuildContext context) async {
    List<CardModel> productsModel = [];
    // productsModel =
    // await getProductsDataService(context: context, brandCode: brandCode);
    if ((await DBProvider.db.checkDataExistenceByLength("Cards")) == 0) {
      productsModel = await getCardsDataService(context: context);
    } else {
      print("Cards is fetched in from local");

      var localData = await DBProvider.db.getCards();
      print(jsonDecode(localData[0]["cards"])["data"]);

      productsModel =
          CardModel.jsonToCardsList(jsonDecode(localData[0]["cards"])["data"]);

      getCardsDataService(context: context).then((value) => {
            print("Cards is fetched in background"),
            cardsData.value = value,
          });
    }

    cardsData.value = productsModel;

    return productsModel;
  }
}
