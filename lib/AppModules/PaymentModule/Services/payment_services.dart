import 'dart:convert';
import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/PaymentModule/View/credit_card_validator.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<List<CardModel>> getCardsDataService({
  required BuildContext context,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_CARDS + "${user["customerid"]}", context: context);
if(response==null){
  return [];
}else
  if (jsonDecode(response.body)["status"] == 200) {
    print("Get cards Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Cards")) == 0) {
      print("Cards Inserted to SQL");
      await DBProvider.db.createCards(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getCards());
    } else {
      print("Cards Updated to SQL");
      await DBProvider.db.updateCards(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getCards());
    }

    return CardModel.jsonToCardsList(jsonDecode(response.body)["data"]);
  } else {
    return [];
  }
}

Future<bool> addCardService({
  required BuildContext context,
  required String cardholder,
  required String cardnumber,
  required String cvv,
  required String expiry,
  required String cardvendor,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    // "cardcolor": "string",
    "cardholder": cardholder,
    // "cardlogo": "string",
    "cardnumber": int.parse(cardnumber),
    // "cardtype": "string",
    "cardvendor": cardholder,
    // "creationtime": "string",
    "customerid": "${user["customerid"]}",
    "cvv": int.parse(cvv),
    "expiry": expiry,
    // "id": 0
  };
  var response = await ApiCall().postRequestHeader(
      apiUrl: ADD_CARDS, bodyParameter: data, context: context);
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Card added Successfully");
    return true;
  } else if (jsonDecode(response.body)["status"] == 403) {
    ShowMessage().showErrorMessage(context, "Already Added");
    return false;
  } else {
    return false;
  }
}

Future<bool> removeCardService({
  required BuildContext context,
  required String cardID,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    // "customerid": "${user["customerid"]}",
    // "productid": "$productId",
  };

  var response = await ApiCall().deleteRequestHeader(
    apiUrl: DELETE_CARDS + "$cardID",
    bodyParameter: data,
    context: context,
  );
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Deleted From My Cards");
    return true;
  } else {
    return false;
  }
}
