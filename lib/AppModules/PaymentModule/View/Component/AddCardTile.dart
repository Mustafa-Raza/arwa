import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/PaymentModule/View/credit_card_validator.dart';
import 'package:arwa/AppModules/PaymentModule/ViewModel/add_card_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/ViewModel/payment_view_model.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class AddCardTile extends StatefulWidget {
  const AddCardTile({Key? key}) : super(key: key);

  @override
  _AddCardTileState createState() => _AddCardTileState();
}

class _AddCardTileState extends State<AddCardTile> {
  final cardVM = Get.put(PaymentViewModel());
  final addCardVM = Get.put(AddCardViewModel());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 162,
      width: MediaQuery.of(context).size.width - 40,
      // margin: EdgeInsets.symmetric(horizontal: 5.0),
      decoration: BoxDecoration(
          color: Color(0xFF7EBFF2),
          // color: Color(0xFF449FE6),
          // color: Color(0xFF7DBFF2),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Color(0xFF7DBFF2).withOpacity(0.7),
                blurRadius: 15,
                offset: Offset(0, 8)),
          ]),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              height: 140,
              width: 150,
              decoration: BoxDecoration(
                color: Color(0xFF78BBEF),
                // color:Color(0xFF78BBF0),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  topRight: Radius.circular(200),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              height: 90,
              width: 100,
              decoration: BoxDecoration(
                color: Color(0xFF72B7ED),
                // color:Color(0xFF6AB4EF),

                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(100),
                  topLeft: Radius.circular(0),
                ),
              ),
            ),
          ),
          Obx(
            () => Padding(
              padding: const EdgeInsets.fromLTRB(17, 15, 20, 15),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 3,
                      ),
                      AppText.customText(
                          title: "Arwa",
                          color: Colors.white,
                          size: 18,
                          fontWeight: FontWeights.semi_bold),
                      Spacer(),
                      Image(
                        image: AssetImage("assets/paymentAndAddress/Chip.png"),
                        height: 23,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 26,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child
                            : AppText.customText(
                            title: addCardVM.cardNumber.value.isNotEmpty
                                ? splitStringByLength(
                                    addCardVM.cardNumber.value, 4)
                                : "**** **** **** ***",
                            letterSpacce: 3,
                            color: Colors.white,
                            size:  addCardVM.cardNumber.value.isNotEmpty?18:20,
                            overFlowText: TextOverflow.ellipsis,
                            fontWeight: FontWeights.medium),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 3,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.customText(
                              title: "Card Holder Name",
                              color: Colors.white,
                              size: 7,
                              fontWeight: FontWeights.semi_bold),
                          SizedBox(
                            height: 6,
                          ),
                          AppText.customText(
                              title: addCardVM.cardHolder.value,
                              color: Colors.white,
                              size: 11,
                              fontWeight: FontWeights.semi_bold),
                        ],
                      ),
                      Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.customText(
                              title: "Expiry Date",
                              color: Colors.white,
                              size: 7,
                              fontWeight: FontWeights.semi_bold),
                          SizedBox(
                            height: 6,
                          ),
                          AppText.customText(
                              title: addCardVM.cardExpiry.value.isNotEmpty?addCardVM.cardExpiry.value:"MM/YYYY",
                              color: Colors.white,
                              size: 11,
                              fontWeight: FontWeights.semi_bold),
                        ],
                      ),
                      Spacer(),
                      CreditCardValidationBloc().getCreditCardICON(addCardVM.cardNumber.value, 30),
                      // Image(
                      //   image:
                      //       AssetImage("assets/paymentAndAddress/visalogo.png"),
                      //   height: 22,
                      // ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
