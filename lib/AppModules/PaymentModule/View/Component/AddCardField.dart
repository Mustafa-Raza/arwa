import 'package:arwa/AppModules/PaymentModule/View/credit_card_validator.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddCardInputField extends StatefulWidget {
  final bool isCreditCard;
  final String hint;
  final String cardValue;
  final String label;
  final String imgIcon;
  final double width;
  final bool isImgIcon;
  final TextInputType inputType;
final FocusNode focusNode;
final TextEditingController controller;
  final bool isIcon;
  final bool readOnly;

  final VoidCallback? onTap;
  final Function? onChange;

  AddCardInputField({
    Key? key,
    this.onChange,
    this.cardValue="",
 required   this.controller,
    this.onTap,
    this.isCreditCard = false,
    this.readOnly = false,
    this.inputType = TextInputType.text,
    required this.hint,
    required  this.focusNode,
    required this.label,
    this.isIcon = false,
    this.width = 300,
    this.isImgIcon = false,
    this.imgIcon = "",
  }) : super(key: key);

  @override
  _AddCardInputFieldState createState() => _AddCardInputFieldState();
}

class _AddCardInputFieldState extends State<AddCardInputField> {
  // bool _secureText=false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // if(widget.isPassword){
    //   setState(() {
    //     _secureText=!_secureText;;
    //   });
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: widget.width,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(8),
        color: AppColor.white,
        // border: Border.all(
        //   color: Color(0xFFE6E6E6),
        // )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText.customText(
              title: widget.label,
              color: Color(0xFF999B9F),
              size: 12,
              fontWeight: FontWeights.regular),
          Expanded(
            child: TextFormField(
              textCapitalization: TextCapitalization.sentences,
              focusNode:widget.focusNode,
              controller: widget.controller,
              onChanged: (value) {
                widget.onChange!(value);
              },
              readOnly: widget.readOnly,
              // obscureText: _secureText,
              onTap: () {
                if (widget.readOnly) {
                  widget.onTap!();
                }
                print("selected");
                // genderSelector(context: context, onSelectGender:(selected){
                //
                // });
              },
              cursorColor: AppColor.primaryColor,
              keyboardType: widget.inputType,
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade300),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade300),
                ),
                errorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade300),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade300),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade300),
                ),
                // contentPadding:
                // EdgeInsets.only(left: 0, bottom: 0, top: 0, right: 0),
                hintText: widget.hint,

                hintStyle: TextStyle(
                    color: AppColor.DarkText,
                    letterSpacing: widget.isCreditCard ? 3 : 0,
                    fontSize: 14,
                    fontWeight: FontWeights.medium),
                suffixIcon: widget.isIcon || widget.isImgIcon
                    ? widget.isIcon
                        ? Padding(
                            padding: const EdgeInsets.only(
                              right: 10,
                              left: 10,
                            ),
                            child: Image(
                              // color: Color(0xFF999B9F),
                              image: AssetImage(
                                  "assets/auth/lock.png"),
                                  // "assets/Payment/question_mark.png"),
                              height: 14,
                              width: 15,
                            ),
                          )
                        : widget.isCreditCard ?Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 1, 15),
                            child: CreditCardValidationBloc().getCreditCardICON(widget.cardValue, 20),
                          ):Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 1, 15),
                            child: Image(
                              image: AssetImage(widget.imgIcon),
                              height: 14,
                              width: 15,
                              // color: Color(0xFFCCCCCC),
                            ),
                          )
                    : SizedBox(
                        height: 16,
                        width: 16,
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
