import 'package:arwa/AppModules/PaymentModule/Model/PaymentListModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentCardListTile extends StatelessWidget {
  final PaymentCardModel paymentCardModel;
// final VoidCallback callback;
  PaymentCardListTile({Key? key, required this.paymentCardModel,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: paymentCardModel.isSelected
                    ? AppColor.primaryColor
                    :  Color(0xFFF3F7F8),
                borderRadius: BorderRadius.circular(8),
                // boxShadow: [
                //   BoxShadow(
                //     color: Colors.grey.shade300, blurRadius: 15,
                //     offset: Offset(3, 8),
                //     // color: Color(0xFF0000001A),blurRadius: 15,offset: Offset(0,8),
                //   ),
                // ]
            ),
            width: AppConfig(context).width,
            height: 71,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 20,
                ),
                Image(
                  image: AssetImage(paymentCardModel.img),
                  width: 41,
                  height: 41,
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppText.customText(
                        title: paymentCardModel.title,
                        color: paymentCardModel.isSelected
                            ? Colors.white
                            : AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 14),
                    AppText.customText(
                        title: paymentCardModel.cardNumber,
                        color: paymentCardModel.isSelected
                            ? Colors.white
                            : AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 16)
                  ],
                ),
                Spacer(),
                InkWell(
                  onTap: () {},
                  child: Image(
                    image: AssetImage(paymentCardModel.isSelected
                        ? "assets/wallet/Select.png"
                        : "assets/wallet/unSelect.png"),
                    width: 22,
                    height: 22,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
