import 'package:arwa/AppModules/PaymentModule/Model/CardModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyCardTile extends StatelessWidget {
  final CardModel cardModel;

  MyCardTile({Key? key, required this.cardModel}) : super(key: key);




  @override
  Widget build(BuildContext context) {
    return Container(
      height: 162,
      width: MediaQuery.of(context).size.width,
      // margin: EdgeInsets.symmetric(horizontal: 5.0),
      decoration: BoxDecoration(
          // color: cardModel.color.withOpacity(0.7),
          // color: Color(0xFF78BBF0),
          // color: Color(0xFF11DB75),
          gradient: LinearGradient(
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
              colors: [
                Color(int.parse("0xFF${cardModel.cardcolor.split("#")[1]}"))
                    .withOpacity(0.8),
                Color(int.parse("0xFF${cardModel.cardcolor.split("#")[1]}"))
                    .withOpacity(0.8),
              ]),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color:
                  Color(int.parse("0xFF${cardModel.cardcolor.split("#")[1]}"))
                      .withOpacity(0.5),
              blurRadius: 15,
              offset: Offset(0, 4),
            )
          ]),
      child: Stack(
        children: [

          Container(
            height: 162,
            width: MediaQuery.of(context).size.width / 1.35,
            // margin: EdgeInsets.symmetric(horizontal: 5.0),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(int.parse("0xFF${cardModel.cardcolor.split("#")[1]}"))
                        .withOpacity(0.7),
                    Colors.white.withOpacity(0.1),
                    // cardModel.color.withOpacity(0.0),
                  ]),
              // color: Color(0xFF78BBF0),
              // color: Color(0xFF11DB75),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(150),
                topLeft: Radius.circular(15),
                topRight: Radius.circular(0),
              ),
              // boxShadow: [
              //   BoxShadow(
              //     color: cardModel.color.withOpacity(0.5),
              //     blurRadius: 15,
              //     offset: Offset(0, 4),
              //   )
              // ]
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(18, 15, 20, 15),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 7),
                  child: Row(
                    children: [
                      Spacer(),
                      SizedBox(
                        height: 30,
                        width: 50,
                        child: cacheImageView(
                            image: cardModel.cardlogo,
                            imageColor: Colors.white),
                      ),
                      // Image(
                      //   image:
                      //       AssetImage( "assets/paymentAndAddress/visalogo.png"),
                      //   height: 30,
                      //   width: 50,
                      // )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    AppText.customText(
                        letterSpacce: 2,
                        title: splitStringByLength(
                                cardModel.cardnumber.toString(), 4)
                            .toString(),
                        // title: cardModel.cardnumber.toString().spl,
                        color: Colors.white,
                        size: 18,
                        fontWeight: FontWeights.semi_bold),
                  ],
                ),
                SizedBox(
                  height: 13,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 2,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.customText(
                            title: "Card Holder",
                            color: Colors.white,
                            size: 10,
                            fontWeight: FontWeights.regular),
                        SizedBox(
                          height: 6,
                        ),
                        AppText.customText(
                            title: cardModel.cardholder,
                            color: Colors.white,
                            size: 14,
                            fontWeight: FontWeights.medium),
                      ],
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.customText(
                            title: "Expiry",
                            color: Colors.white,
                            size: 10,
                            fontWeight: FontWeights.regular),
                        SizedBox(
                          height: 6,
                        ),
                        AppText.customText(
                            title: cardModel.expiry,
                            // cardModel.expiry.month.toString() +
                            //     "/" +
                            //     cardModel.expiry.month.toString(),
                            color: Colors.white,
                            size: 14,
                            fontWeight: FontWeights.semi_bold),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
