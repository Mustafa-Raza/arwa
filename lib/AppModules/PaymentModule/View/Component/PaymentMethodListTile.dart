import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///TODO:PaymentMethodCard
class PaymentMethodCard extends StatelessWidget {
  const PaymentMethodCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        height: 80,width:  MediaQuery.of(context).size.width-60,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
            boxShadow: [AppColor().boxShadow]),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppText.customText(
                    title: "Master Card ",
                    color: AppColor.DarkText,
                    fontWeight: FontWeights.semi_bold,
                    size: 14,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage("assets/paymentAndAddress/visa.png"),
                        height: 18,
                        // width: 2,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      AppText.customText(
                        title: "**** 5743",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 14,
                      ),
                    ],
                  ),
                ],
              ),
              Spacer(),
              Image(
                image: AssetImage("assets/paymentAndAddress/select.png"),
                height: 18,
                width: 18,
              ),
              SizedBox(
                width: 5,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
///TODO:PaymentMethodTypeTile
class PaymentMethodTypeTile extends StatelessWidget {
  String icon;
  String title;
  VoidCallback callback;
  PaymentMethodTypeTile({Key? key,required this.callback,required this.icon,required this.title}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: InkWell(
        onTap: (){
          callback();
        },
        child: Container(
          height: 80,width:  MediaQuery.of(context).size.width-60,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [AppColor().boxShadow]),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 10,
                ),
                Image(
                  image: AssetImage(icon),
                  height: 24,
                  width: 24
                ),  SizedBox(
                  width: 19,
                ),
                AppText.customText(
                  title: title,
                  color: AppColor.DarkText,
                  fontWeight: FontWeights.semi_bold,
                  size: 16,
                ),
                Spacer(),
                Image(
                  image: AssetImage("assets/paymentAndAddress/Deliverto.png"),
                  height: 18,
                  width: 18,
                ),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
