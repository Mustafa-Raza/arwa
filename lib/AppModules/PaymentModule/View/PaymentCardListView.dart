import 'package:arwa/AppModules/PaymentModule/Model/PaymentListModel.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/PaymentCardListTile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentCardListView extends StatefulWidget {
  const PaymentCardListView({Key? key}) : super(key: key);

  @override
  _PaymentCardListViewState createState() => _PaymentCardListViewState();
}

class _PaymentCardListViewState extends State<PaymentCardListView> {
  List<PaymentCardModel> paymentCards = [
    PaymentCardModel(
        title: "Credit Card",
        img: "assets/wallet/master.png",
        cardNumber: "74251 **** **** 4421",
        isSelected: true),
    PaymentCardModel(
        title: "Debit Card",
        img: "assets/wallet/visa.png",
        cardNumber: "8421 **** **** 4851",
        isSelected: false),
  ];
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: paymentCards.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            print("clicked");
            // setState(() {
            //
            //   // paymentCards[selectedIndex].isSelected=paymentCards[selectedIndex].isSelected;
            //   paymentCards[index].isSelected = paymentCards[index].isSelected;
            //   // selectedIndex=index;
            // });
          },
          child: PaymentCardListTile(
            paymentCardModel: paymentCards[index],
          ),
        );
      },
    );
  }
}
