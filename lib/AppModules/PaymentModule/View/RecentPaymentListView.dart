import 'package:arwa/AppModules/PaymentModule/View/RecentPaymentListTile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class RecentPaymentListView extends StatefulWidget {
  const RecentPaymentListView({Key? key}) : super(key: key);

  @override
  _RecentPaymentListViewState createState() => _RecentPaymentListViewState();
}

class _RecentPaymentListViewState extends State<RecentPaymentListView> {

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: 3,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return RecentPaymentListTile(

        );
      },
    );
  }
}
