import 'package:credit_card_type_detector/credit_card_type_detector.dart';
import 'package:credit_card_validator/credit_card_validator.dart';
import 'package:credit_card_validator/validation_results.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CreditCardValidationBloc {
  CreditCardValidator _ccValidator = CreditCardValidator();

  validateCard(String cardNumber) {
    CCNumValidationResults result = _ccValidator.validateCCNum(cardNumber);
    print(result.ccType == CreditCardType.visa);
    print(result.message);
    print(result.isValid);
    print(result.isPotentiallyValid);
    // CCNumValidationResults(ccType: ccType, isValid: isValid, isPotentiallyValid: isPotentiallyValid)
  }

  Widget getCreditCardICON(String cardNumber, double iconSize,) {
    Widget icon;
    CCNumValidationResults result = _ccValidator.validateCCNum(cardNumber);
    switch (result.ccType) {
      case CreditCardType.visa:
        {
          icon = Icon(
            FontAwesomeIcons.ccVisa,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      case CreditCardType.amex:
        {
          icon = Icon(
            FontAwesomeIcons.ccAmex,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      case CreditCardType.mastercard:
        {
          icon = Icon(
            FontAwesomeIcons.ccMastercard,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      case CreditCardType.dinersclub:
        {
          icon = Icon(
            FontAwesomeIcons.ccDinersClub,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;
      case CreditCardType.discover:
        {
          icon = Icon(
            FontAwesomeIcons.ccDiscover,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      case CreditCardType.jcb:
        {
          icon = Icon(
            FontAwesomeIcons.ccJcb,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      case CreditCardType.unknown:
        {
          icon = Icon(
            Icons.error,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;

      default:
        {
          icon = Icon(
           Icons.credit_card,
            size: iconSize,
            // color: Color(0xffffffff),
          );
        }
        break;
    }
    print(icon);
    return icon;
  }

  String getCreditCardType(String cardNumber,) {
    String type;
    CCNumValidationResults result = _ccValidator.validateCCNum(cardNumber);
    switch (result.ccType) {
      case CreditCardType.visa:
        {
          type="Visa";
        }
        break;

      case CreditCardType.amex:
        {
          type="Amex";
        }
        break;

      case CreditCardType.mastercard:
        {
          type="MasterCard";
        }
        break;

      case CreditCardType.dinersclub:
        {
          type="DinersClub";
        }
        break;
      case CreditCardType.discover:
        {
          type="Discover";
        }
        break;

      case CreditCardType.jcb:
        {
          type="JCB";
        }
        break;

      case CreditCardType.unknown:
        {
          type="Unknown";
        }
        break;

      default:
        {
          type="Visa";
        }
        break;
    }
    print(type);
    return type;
  }

// bool validateCreditCardInfo(string ccNum, string expDate, string cvv, ) {
// var ccNumResults = _ccValidator.validateCCNum(ccNum);
// var expDateResults = _ccValidator.validateExpDate(expDate);
// var cvvResults = _ccValidator.validateCVV(cvv, ccNumResults.ccType);
//
//
// if(ccNumResults.isPotentiallyValid) {
// # Call UI code that shows to the user their credit card number is invalid
// displayInvalidCardNumber();
// }
// }
}
