import 'package:arwa/AppModules/PaymentModule/View/Component/PaymentMethodListTile.dart';
import 'package:arwa/AppModules/AddressModule/View/SelectAddressTile.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/AddCardViewController.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/MyWalletViewController.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/PaymentMethodsModal.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentMethodsView extends StatelessWidget {
  const PaymentMethodsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: [
                InkWell(
                    child: AppText.customText(
                  title: "Payment Method",
                  color: AppColor.DarkText,
                  fontWeight: FontWeights.semi_bold,
                  size: 16,
                )),
                // Spacer(),
                // InkWell(
                //   onTap: () {
                //     paymentMethodModalView(context: context);
                //   },
                //   child: Image(
                //     image: AssetImage("assets/paymentAndAddress/Edit.png"),
                //     height: 16,
                //     width: 16,
                //   ),
                // ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          // PaymentMethodCard(),
          PaymentMethodTypeTile(
            title: "Wallet",
            callback: () {
              Get.to(() => MyWalletViewController(isPrimary: false,),transition: Transition.rightToLeft);
            },
            icon: "assets/paymentAndAddress/wallet.png",
          ),
          PaymentMethodTypeTile(
            title: "Credit or Debit Card",
            callback: () {
              Get.to(() => AddCardViewController());
            },
            icon: "assets/paymentAndAddress/groupCard.png",
          ),
          PaymentMethodTypeTile(
            title: "Cash on Delivery",
            callback: () {
              // Get.to(() => AddCardViewController());
            },
            icon: "assets/paymentAndAddress/groupCard.png",
          ),
          // ListView.builder(
          //   itemCount: 4,
          //   shrinkWrap: true,
          //   physics: NeverScrollableScrollPhysics(),
          //   itemBuilder: (context, index) {
          //     return PaymentMethodTile();
          //   },
          // )
        ],
      ),
    );
  }
}
