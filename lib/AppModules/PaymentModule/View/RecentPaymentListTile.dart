import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RecentPaymentListTile extends StatelessWidget {

  RecentPaymentListTile({Key? key,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color:  Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.shade200, blurRadius: 10,
                    // offset: Offset(3, 8),
                    // color: Color(0xFF0000001A),blurRadius: 15,offset: Offset(0,8),
                  ),
                ]),
            width: AppConfig(context).width,
            height: 71,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 20,
                ),
                Image(
                  image: AssetImage("assets/wallet/recentPayment.png"),
                  width: 36,
                  height: 36,
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppText.customText(
                        title: "22 Oct, 2021",
                        color:Color(0xFF999B9F),
                        fontWeight: FontWeights.medium,
                        size: 12),
                    AppText.customText(
                        title: "Arwa 200ml 1 x 24 PET",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 14)
                  ],
                ),
                Spacer(),
                AppText.customText(
                    title: "-",
                    color:Colors.red,
                    fontWeight: FontWeights.semi_bold,
                    size: 14),
                AppText.customText(
                    title: "AED 450",
                    color:Color(0xFF1D2733),
                    fontWeight: FontWeights.semi_bold,
                    size: 14),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
