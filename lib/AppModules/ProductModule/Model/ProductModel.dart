
class ProductModel {
  ProductModel({
    required this.id,
    required this.cartid,
    required this.cartQuantity,
    required this.productid,
    required this.productdesc,
    required this.country,
    required this.brandcode,
    required this.flavourcode,
    required this.packsize,
    required this.baseprice,
    required this.discountprice,
    required this.image,
    required this.infavorite,
    required this.producttype,
  });

  int id;
  int cartid;
  int cartQuantity;
  int infavorite;
  String productid;
  String productdesc;
  String country;
  String brandcode;
  String flavourcode;
  String packsize;
  double baseprice;
  double discountprice;
  String image;
  String producttype;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"]??0,
    cartid: json["cartid"]??0,
    cartQuantity: json["qty"]??0,
    infavorite: json["infavorite"]??0,
        productid: json["productid"]??"",
        productdesc: json["productdesc"]??"",
        country: json["country"]??"",
        brandcode: json["brandcode"]??"",
        flavourcode: json["flavourcode"]??"",
        packsize: json["packsize"]??"",
        baseprice: json["baseprice"]??0.0,
        discountprice: json["discountprice"]??0.0,
        image: json["image"]??"",
        producttype: json["producttype"]??"",
      );

  static List<ProductModel> jsonToProductsList(List<dynamic> emote) =>
      emote.map<ProductModel>((item) => ProductModel.fromJson(item)).toList();
}

// class PackSizeModel{
//   String packSize;
//   PackSizeModel({required this.packSize});
//   factory PackSizeModel.fromJson(Map<String, dynamic> json) => ProductModel(
//     packSize: json["id"],
//
//   );
//
// }
