import 'dart:convert';

import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/CategoriesModule/Services/category_services.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Services/fourite_services.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/Services/product_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class ProductViewModel extends GetxController {
  RxString selectedSize = "All".obs;
  var productsData = [].obs;
  var packSizes = [].obs;
  var productsDataSaved = [].obs;

  Future<List<ProductModel>> getProductsData(
      BuildContext context, String brandCode) async {
    List<ProductModel> productsModel = [];
    // productsModel =
    // await getProductsDataService(context: context, brandCode: brandCode);
    if ((await DBProvider.db.checkDataExistenceByLength("Product")) == 0) {
      productsModel =
          await getProductsDataService(context: context, brandCode: brandCode);
    } else {
      print("productsModel is fetched in from local");
      //
      var localData = await DBProvider.db.getProducts();
      // String value=jsonEncode(localData[0]["home"]);
      print(jsonDecode(localData[0]["products"])["data"]);

      productsModel = ProductModel.jsonToProductsList(
          jsonDecode(localData[0]["products"])["data"]["products"]);

      getProductsDataService(context: context, brandCode: brandCode)
          .then((value) => {
                print("productsModel is fetched in background"),
                productsData.value = value,
                productsDataSaved.value = value,
                // homeModel = value,
              });
    }

    productsData.value = productsModel;
    productsDataSaved.value = productsModel;

    return productsModel;
  }

  filterProducts(String value) {
    if (value == "All") {
      productsData.value = productsDataSaved.value;
    } else {
      productsData.value = productsDataSaved
          .where((element) => element.packsize.toString().contains(value))
          .toList();
    }
  }

  getPackSize(BuildContext context, String brandCode) async {
    var data =
        await getPackSizeDataService(context: context, brandCode: brandCode);
    packSizes.value = data;
  }

  addToFav(BuildContext context, String productId) async {
    bool likeResult =
        await addToFavouriteService(context: context, productId: productId);
  }
}
