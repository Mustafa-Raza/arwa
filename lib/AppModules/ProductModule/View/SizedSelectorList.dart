import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/ViewModel/product_view_model.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SizeSelectorList extends StatefulWidget {
  final List<dynamic> products;

  // final List<ProductModel> products;
  // Function ?callBack;

  SizeSelectorList({
    Key? key,
    required this.products,
  }) : super(key: key);

  @override
  _SizeSelectorListState createState() => _SizeSelectorListState();
}

class _SizeSelectorListState extends State<SizeSelectorList> {
  int selectedIndex = 0;

  final categoryController = Get.put(HomeCategoryController());
  final productVM = Get.put(ProductViewModel());

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      child:    Obx(() => ListView.builder(
          // physics: NeverScrollableScrollPhysics(),
          itemCount: productVM.packSizes.value.length,
          // itemCount: widget.products.length,
          shrinkWrap: true,
          // reverse: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(left: 20),
              child: index == 0
                  ? InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                        });
                        productVM.filterProducts("All");
                        // productVM.selectedSize.value =
                        //     widget.products[index].packsize;
                      },
                      child: Container(
                        height: 35,
                        width: 100,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: selectedIndex == index
                                    ? Color(int.parse(
                                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                                    : AppColor.transparent,
                                blurRadius: 2)
                          ],
                          borderRadius: BorderRadius.circular(8),
                          color: selectedIndex == index
                              ? Color(int.parse(
                                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                              : AppColor.transparent,
                        ),
                        child: Center(
                            child: AppText.customText(
                                title: "All",
                                color: selectedIndex == index
                                    ? AppColor.white
                                    : AppColor.textGREY,
                                fontWeight: FontWeights.medium,
                                size: 14)),
                      ),
                    )
                  : InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                        });
                        productVM.filterProducts(productVM.packSizes[index]);
                        // widget.callBack!("${widget.products[index].packsize}");
                      },
                      child: Container(
                        height: 35,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: selectedIndex == index
                              ? Color(int.parse(
                                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                              : AppColor.transparent,
                        ),
                        child: Center(
                            child: AppText.customText(
                                title: "${productVM.packSizes[index]}",
                                color: selectedIndex == index
                                    ? AppColor.white
                                    : AppColor.textGREY,
                                fontWeight: FontWeights.medium,
                                size: 14)),
                      ),
                    ),
            );
          },
        ),
      ),
    );
  }
}
