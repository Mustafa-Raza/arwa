import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/home_view_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Services/fourite_services.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewModel/favourite_view_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ProductDetailViewController.dart';
import 'package:arwa/AppModules/ProductModule/ViewModel/product_view_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductListTile extends StatefulWidget {
  final ProductModel productModel;
  final int index;
  final bool isNewArival;
  final bool isAllProducts;
  final bool isFavourite;
  final List<dynamic> relatedProductsList;

  // final List<ProductModel> relatedProductsList;
  ProductListTile(
      {Key? key,
      required this.productModel,
      required this.index,
      this.isNewArival = false,
      this.isAllProducts = false,
      this.isFavourite = false,
      required this.relatedProductsList})
      : super(key: key);

  @override
  _ProductListTileState createState() => _ProductListTileState();
}

class _ProductListTileState extends State<ProductListTile> {
  // bool isLiked = true;
  final homeVM = Get.put(HomeViewModel());
  final favVM = Get.put(FavProductViewModel());
  final productVM = Get.put(ProductViewModel());
  final categoryController = Get.put(HomeCategoryController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // setState(() {
    //   isLiked = widget.productModel.infavorite == 1;
    // });
  }

  onAddToFav() async {
    setState(() {
      if (widget.isNewArival) {
        homeVM.homeData.value.brandsList
            .where((element) =>
                categoryController.selectedBrand.value.id == element.id)
            .first
            .newArrivalsProducts[widget.index]
            .infavorite = 1;
      } else if (widget.isAllProducts) {
        productVM.productsData.value[widget.index].infavorite = 1;
      } else {
        homeVM.homeData.value.brandsList
            .where((element) =>
                categoryController.selectedBrand.value.id == element.id)
            .first
            .products[widget.index]
            .infavorite = 1;
      }
    });
    print("On Add");
    if (!widget.isAllProducts) favVM.addToFav(context, widget.productModel);

    bool likeResult = await addToFavouriteService(
        context: context, productId: widget.productModel.productid.toString());
    if (likeResult) {
      if (widget.isAllProducts) {
        await favVM.getUpdateFavProductsData(context);
        await homeVM.getUpdatedHomeData(context);
      } else {
        await getFavProductsDataService(context: context);
      }
    }
  }

  onRemoveFromFav() async {
    print("On Remove");
    setState(() {
      if (widget.isNewArival) {
        print("new arival");
        homeVM.homeData.value.brandsList
            .where((element) =>
                categoryController.selectedBrand.value.id == element.id)
            .first
            .newArrivalsProducts[widget.index]
            .infavorite = 0;
      }
      if (widget.isAllProducts) {
        print("new arival");
        productVM.productsData.value[widget.index].infavorite = 0;
      } else {
        print("new arival");
        homeVM.homeData.value.brandsList
            .where((element) =>
                categoryController.selectedBrand.value.id == element.id)
            .first
            .products[widget.index]
            .infavorite = 0;
      }
    });
    if (!widget.isAllProducts)
      favVM.removeFromFav(context, widget.productModel);

    bool disLikeResult = await removeFromFavouriteService(
        context: context, productId: widget.productModel.productid.toString());
    if (disLikeResult) {
      if (widget.isAllProducts) {
        await favVM.getUpdateFavProductsData(context);
        await homeVM.getUpdatedHomeData(context);
      }
      if (widget.isFavourite) {
        await homeVM.getUpdatedHomeData(context);
        await getFavProductsDataService(context: context);

      } else {
        await getFavProductsDataService(context: context);
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print("Cick on Product ");
        Navigator.push(
          context,
          PageTransition(
            duration: Duration(milliseconds: 400),
            type: PageTransitionType.rightToLeft,
            child: ProductDetailViewController(
              isAllProducts: widget.isAllProducts,
              index: widget.index,
              isFavourite: widget.isFavourite,
              isNewArival: widget.isNewArival,
              productModel: widget.productModel,
              relatedProductsList: widget.relatedProductsList,
            ),
          ),
        );

        print("Cicked on Product ");
      },
      child: Container(
        height: 228,
        width: 160,
        color: Colors.transparent,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 176,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.shade300, blurRadius: 20,
                        // color: Color(0xF0000001A),blurRadius: 15,offset: Offset(0,8),
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          InkWell(
                            onTap: () async {
                              if (widget.productModel.infavorite == 1) {
                                // setState(() {
                                //   if (widget.isNewArival) {
                                //     homeVM.homeData.value.brandsList
                                //         .where((element) =>
                                //             categoryController
                                //                 .selectedBrand.value.id ==
                                //             element.id)
                                //         .first
                                //         .newArrivalsProducts[widget.index]
                                //         .infavorite = 0;
                                //   } else {
                                //     homeVM.homeData.value.brandsList
                                //         .where((element) =>
                                //             categoryController
                                //                 .selectedBrand.value.id ==
                                //             element.id)
                                //         .first
                                //         .products[widget.index]
                                //         .infavorite = 0;
                                //   }
                                //
                                //   // isLiked = !isLiked;
                                // });
                                // bool disLikeResult =
                                //     await removeFromFavouriteService(
                                //         context: context,
                                //         productId: widget.productModel.productid
                                //             .toString());
                                // if (disLikeResult) {
                                //   await favVM.getUpdateFavProductsData(context);
                                //   await homeVM.getUpdatedHomeData(context);
                                //   //
                                // } else {}
                                onRemoveFromFav();
                              } else {
                                // setState(() {
                                //   if (widget.isNewArival) {
                                //     homeVM.homeData.value.brandsList
                                //         .where((element) =>
                                //             categoryController
                                //                 .selectedBrand.value.id ==
                                //             element.id)
                                //         .first
                                //         .newArrivalsProducts[widget.index]
                                //         .infavorite = 1;
                                //   } else {
                                //     homeVM.homeData.value.brandsList
                                //         .where((element) =>
                                //             categoryController
                                //                 .selectedBrand.value.id ==
                                //             element.id)
                                //         .first
                                //         .products[widget.index]
                                //         .infavorite = 1;
                                //   }
                                //   // isLiked = !isLiked;
                                // });
                                // bool likeResult = await addToFavouriteService(
                                //     context: context,
                                //     productId: widget.productModel.productid
                                //         .toString());
                                // if (likeResult) {
                                //   await favVM.getUpdateFavProductsData(context);
                                //   await homeVM.getUpdatedHomeData(context);
                                // }
                                onAddToFav();
                              }
                            },
                            child: widget.productModel.infavorite == 1
                                ? Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Image(
                                      image: AssetImage(
                                          "assets/home/filledHeart.png"),
                                      height: 32,
                                      width: 32,
                                    ),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Image(
                                      image: AssetImage(
                                          "assets/home/unfilledheart.png"),
                                      height: 32,
                                      width: 32,
                                    ),
                                  ),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                      Spacer(),
                      AppText.customText(
                        title: widget.productModel.packsize,
                        size: 10,
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      AppText.customText(
                        title: widget.productModel.productdesc,
                        size: 14,
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      AppText.customText(
                        title: "AED ${widget.productModel.baseprice}",
                        size: 12,
                        color: Color(int.parse(
                            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                        fontWeight: FontWeights.semi_bold,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Align(
                alignment: Alignment.topCenter,
                child: SizedBox(
                    height: 140,
                    width: 90,
                    child: cacheImageView(
                        image: widget.productModel.image,
                        circlularPadding: 50)),
                // Image(
                //   image: NetworkImage(widget.productModel.image),
                //   // image: AssetImage(widget.productModel.image),
                //   height: 140,
                //   // width: 53,
                //   fit: BoxFit.cover,
                // )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
