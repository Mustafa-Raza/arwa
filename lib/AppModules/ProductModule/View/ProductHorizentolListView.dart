import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductListType.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/ProductVerticleListTile.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ProductDetailViewController.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductHorizentolListView extends StatefulWidget {
  final List<dynamic> productsList;
  final bool isNewArival;
  final bool isFavourite;
  ProductHorizentolListView({Key? key, this.isNewArival = false,
    this.isFavourite = false,required this.productsList})
      : super(key: key);

  @override
  _ProductHorizentolListViewState createState() =>
      _ProductHorizentolListViewState();
}

class _ProductHorizentolListViewState extends State<ProductHorizentolListView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: widget.productsList.isNotEmpty
          ? Container(
              height: 238,
              color: Colors.white,
              child: ListView.builder(
                itemCount: widget.productsList.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(
                        left: 20,
                        bottom: 10,
                        right:
                            index + 1 == widget.productsList.length ? 10 : 0),
                    child: ProductListTile(
                      isNewArival: widget.isNewArival,
                      isFavourite: widget.isFavourite,
                      index: index,
                      relatedProductsList: widget.productsList,
                      productModel: widget.productsList[index],
                    ),
                  );
                },
              ),
            )
          : Center(child: AppText.customText(title: "Products Not Found")),
    );
    // );
  }
}
