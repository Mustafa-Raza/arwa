import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductCountTile extends StatefulWidget {
  final String minusImage;
  final String plusImage;
  final bool isCart;
  final int value;
  final Function onChange;
  final VoidCallback ?onDelete;

  ProductCountTile(
      {Key? key,
      this.minusImage = "assets/icons/Minus.png",
      this.isCart = false,
      this.value=0,
   required   this.onChange,
      this.onDelete,
      this.plusImage = "assets/icons/Plus.png"})
      : super(key: key);

  @override
  _ProductCountTileState createState() => _ProductCountTileState();
}

class _ProductCountTileState extends State<ProductCountTile> {
  final categoryController = Get.put(HomeCategoryController());
   int valueCount=1;
   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.value!=0)
    valueCount=widget.value;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          if (!widget.isCart)
          InkWell(
            onTap: (){
              if(valueCount>1 )
              setState(() {
                valueCount--;
              });
              widget.onChange(valueCount);
            },
            child
                : Image(
              image: AssetImage(widget.minusImage),
              height: 30,
              width: 30,
            ),
          ),
          if (widget.isCart)
            valueCount==1 ?InkWell(
            onTap: (){

                widget.onDelete!();
            },
            child:   Image(
                image: AssetImage("assets/cart/del.png"),
                height: 30,
                width: 30,
              )
          ):InkWell(
            onTap: (){

                setState(() {
                  valueCount--;
                });
              widget.onChange(valueCount);
            },
            child:  Image(
              image: AssetImage("assets/cart/minusCart.png"),
              height: 30,
              width: 30,
            )
            // CircleAvatar(
            //   radius: 14,
            //   backgroundColor: Colors.blue.shade50,
            //   child: Icon(
            //     Icons.remove,
            //     color: Colors.white,
            //   ),
            // ),
          ),

          SizedBox(
            width: 10,
          ),
          AppText.customText(
              title:valueCount.toString(),
              size: 12,
              fontWeight: FontWeights.medium,
              color: AppColor.DarkText),
          SizedBox(
            width: 10,
          ),
          if (widget.isCart)
            InkWell(
              onTap: (){
                setState(() {
                  valueCount++;
                });
                widget.onChange(valueCount);
              },
              child: CircleAvatar(
                radius: 14,
                backgroundColor: Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
          if (!widget.isCart)
            InkWell(
              onTap: (){
                setState(() {
                  valueCount++;
                });
                widget.onChange(valueCount);
              },
              child
                  : Image(
                image: AssetImage(widget.plusImage),
                height: 30,
                width: 30,
              ),
            ),
        ],
      ),
    );
  }
}
