import 'dart:io';

import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductListType.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/SizedSelectorList.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ProductListView.dart';
import 'package:arwa/AppModules/ProductModule/ViewModel/product_view_model.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class ArwaAllProductListViewController extends StatefulWidget {
  final List<ProductModel> products;
  final String brandCode;

  ArwaAllProductListViewController(
      {Key? key, required this.products, required this.brandCode})
      : super(key: key);

  @override
  _ArwaAllProductListViewControllerState createState() =>
      _ArwaAllProductListViewControllerState();
}

class _ArwaAllProductListViewControllerState
    extends State<ArwaAllProductListViewController> {
  final categoryController = Get.put(HomeCategoryController());
  final productVM = Get.put(ProductViewModel());

  Future<List<ProductModel>>? getProducts;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productVM.getPackSize(context, widget.brandCode);
    getProducts = productVM.getProductsData(context, widget.brandCode);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    productVM.packSizes.clear();
    productVM.productsData.clear();
    productVM.productsDataSaved.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Container(
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            color: Color(int.parse(
                "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
            child: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                ),
                width: AppConfig(context).width,
                // height: AppConfig(context).height - 50,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: InnerAppBar(
                        title: categoryController.selectedBrand.value.display,
                      ),
                    ),
                    Expanded(
                      child: FutureBuilder<List<ProductModel>>(
                          // future: productVM.getProductsData(context,widget.brandCode),
                          future: getProducts,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: ProductShimmerView(),
                              );
                            }

                            return Obx(() => Column(
                                  children: [
                                    SizedBox(
                                      height: 20,
                                    ),
                                    SizeSelectorList(
                                      products: productVM.productsData.value,
                                      // callBack: (size) {},
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Expanded(
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Obx(
                                              () => ProductVerticleListView(
                                                isFavourite: false,
                                                isNewArival: false,
                                                isAllProducts: true,

                                                productsList: productVM
                                                    .productsData.value,
                                                productListType:
                                                    ProductListType.allProducts,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ));
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
