import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductListType.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/ProductVerticleListTile.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductVerticleListView extends StatefulWidget {
  final ProductListType productListType;
  final bool isNewArival;
  final bool isAllProducts;
  final bool isFavourite;
  final List<dynamic> productsList;

  ProductVerticleListView(
      {Key? key,
      this.isNewArival = false,
      this.isFavourite = false,
      this.isAllProducts = false,
      required this.productListType,
      required this.productsList})
      : super(key: key);

  @override
  _ProductVerticleListViewState createState() =>
      _ProductVerticleListViewState();
}

class _ProductVerticleListViewState extends State<ProductVerticleListView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: widget.productsList.isNotEmpty
          ? Container(
              child: GridView.builder(
                shrinkWrap: true,
                itemCount: widget.productsList.length,
                padding: const EdgeInsets.all(4.0),
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 0.65,
                    mainAxisSpacing: 30.0,
                    crossAxisSpacing: 15.0,
                    crossAxisCount: 2),
                itemBuilder: (context, index) {
                  return ProductListTile(
                    isNewArival: widget.isNewArival,
                    isAllProducts: widget.isAllProducts,
                    isFavourite: widget.isFavourite,
                    index: index,
                    relatedProductsList: widget.productsList,
                    productModel: widget.productsList[index],
                  );
                },
              ),
            )
          : Center(child: AppText.customText(title: "Products Not Found")),
    );
  }
}
