
import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/CartModule/Services/cart_services.dart';
import 'package:arwa/AppModules/CartModule/ViewController/MyCartViewController.dart';
import 'package:arwa/AppModules/CartModule/ViewModel/cart_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/home_view_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Services/fourite_services.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewModel/favourite_view_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/Components/ProductCountTile.dart';
import 'package:arwa/AppModules/ProductModule/View/ProductHorizentolListView.dart';
import 'package:arwa/AppModules/ProductModule/ViewModel/product_view_model.dart';
import 'package:arwa/AppModules/Utills/AppButtons/CartButton.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class ProductDetailViewController extends StatefulWidget {
  final ProductModel productModel;
  final int index;
  final bool isNewArival;
  final bool isFavourite;
  final bool isAllProducts;
  final List<dynamic> relatedProductsList;

  // final List<ProductModel> relatedProductsList;
  ProductDetailViewController(
      {Key? key,
      required this.index,
      required this.productModel,
      this.isFavourite = false,
      this.isAllProducts = false,
      this.isNewArival = false,
      required this.relatedProductsList})
      : super(key: key);

  @override
  _ProductDetailViewControllerState createState() =>
      _ProductDetailViewControllerState();
}

class _ProductDetailViewControllerState
    extends State<ProductDetailViewController> {
  final favVM = Get.put(FavProductViewModel());

  final productVM = Get.put(ProductViewModel());
  final homeVM = Get.put(HomeViewModel());
  final categoryController = Get.put(HomeCategoryController());

  bool isLiked = true;
  bool isLoading = false;
  bool addToCartLoading = false;
  int quantity = 1;
  final cartVM = Get.put(CartViewModel());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isLiked = widget.productModel.infavorite == 1;
    });
  }

  onAddToFav() async {
    if(widget.isFavourite){
      setState(() {
        isLiked=!isLiked;
      });
    }

      setState(() {
        if (widget.isNewArival) {
          homeVM.homeData.value.brandsList
              .where((element) =>
                  categoryController.selectedBrand.value.id == element.id)
              .first
              .newArrivalsProducts[widget.index]
              .infavorite = 1;
        }else if (widget.isAllProducts) {
          print("new arival");
          productVM.productsData.value[widget.index]
              .infavorite = 0;
        } else {
          homeVM.homeData.value.brandsList
              .where((element) =>
                  categoryController.selectedBrand.value.id == element.id)
              .first
              .products[widget.index]
              .infavorite = 1;
        }
      });
    print("On Add");

    favVM.addToFav(context, widget.productModel);

    bool likeResult = await addToFavouriteService(
        context: context, productId: widget.productModel.productid.toString());
    if (likeResult) {
      await favVM.getUpdateFavProductsData(context);
      await homeVM.getUpdatedHomeData(context);
    }
  }

  onRemoveFromFav() async {
    print("On Remove");
    if(widget.isFavourite){
      setState(() {
        isLiked=!isLiked;
      });
    }

      setState(() {
        if (widget.isNewArival) {
          homeVM.homeData.value.brandsList
              .where((element) =>
                  categoryController.selectedBrand.value.id == element.id)
              .first
              .newArrivalsProducts[widget.index]
              .infavorite = 0;
        } else {
          homeVM.homeData.value.brandsList
              .where((element) =>
                  categoryController.selectedBrand.value.id == element.id)
              .first
              .products[widget.index]
              .infavorite = 0;
        }
      });
    favVM.removeFromFav(context, widget.productModel);
    bool disLikeResult = await removeFromFavouriteService(
        context: context, productId: widget.productModel.productid.toString());
    if (disLikeResult) {
      await favVM.getUpdateFavProductsData(context);
      await homeVM.getUpdatedHomeData(context);
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
          child: Obx(
            () => cartButton(
              isLoading: addToCartLoading,
              cartList: cartVM.getCartData.value.cartList.length,
              buttonTitle: "Add to Cart",
              callback: () async {
                setState(() {
                  addToCartLoading = !addToCartLoading;
                });
                print("Add to Cart");
                var user = await DatabaseHandler().getCurrentUser();
                bool result = await addToCartService(
                  context: context,
                  cartsProduct: [
                    CartModel(
                        customerid: "${user["customerid"]}",
                        cartId: widget.productModel.cartid,
                        productid: widget.productModel.productid,
                        qty: quantity)
                  ],
                );
                setState(() {
                  addToCartLoading = !addToCartLoading;
                });
                if (result) {
                  await cartVM.getCartProductsData(context);
                }
              },
              width: AppConfig(context).width - 60,
              btnColor: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              txtColor: AppColor.white,
              height: 46,
              fontweight: FontWeights.medium,
            ),
          ),
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: SafeArea(
            child: Container(
              color: Colors.white,
              width: AppConfig(context).width,
              height: AppConfig(context).height,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      color: Color(0xFFF2F9FF),
                      // categoryController.selectedBrand.value.color
                      //     .withOpacity(0.08),
                      height: 430,
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: SizedBox(
                              // height: 250,
                              width: 175,
                              child: cacheImageView(
                                  image: widget.productModel.image),
                              // Image(
                              //   image: NetworkImage(widget.productModel.image),
                              //   // image: AssetImage(widget.productModel.image),
                              //   width: AppConfig(context).width,
                              //   // height: 250,
                              //   // fit: BoxFit.fill,
                              // ),
                            ),
                          ),
                          SizedBox(
                            width: AppConfig(context).width,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 18,
                                      child: Center(
                                          child: Icon(
                                        Icons.arrow_back_ios_rounded,
                                        size: 20,
                                        color: Colors.black,
                                      )),
                                    ),
                                    onTap: () {
                                      Get.back();
                                    },
                                  ),
                                  Expanded(
                                    child: AppText.customText(
                                        title: widget.productModel.productdesc,
                                        color: AppColor.DarkText,
                                        alignment: TextAlign.center,
                                        overFlowText: TextOverflow.ellipsis,
                                        fontWeight: FontWeights.semi_bold,
                                        size: 16),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Get.to(() => MyCartViewController(),
                                          transition: Transition.rightToLeft);
                                    },
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 18,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child:
                                            // Image(
                                            //   image:
                                            //       AssetImage("assets/home/cart.png"),
                                            //   width: 30,
                                            //   height: 30,
                                            //   fit: BoxFit.fill,
                                            // ),

                                            SizedBox(
                                          width: 30,
                                          height: 30,
                                          child: Stack(
                                            alignment: Alignment.topRight,
                                            children: [
                                              Image(
                                                image: AssetImage(
                                                    "assets/home/cart.png"),
                                                width: 30,
                                                height: 30,
                                                fit: BoxFit.fill,
                                              ),
                                              Obx(
                                                () => CircleAvatar(
                                                  radius: 3,
                                                  backgroundColor: cartVM
                                                          .getCartData
                                                          .value
                                                          .cartList
                                                          .isNotEmpty
                                                      ? Colors.pink
                                                      : Colors.transparent,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(bottom: 50),
                          //   child: Align(
                          //     alignment: Alignment.bottomCenter,
                          //     child: Row(
                          //       mainAxisAlignment: MainAxisAlignment.center,
                          //       children: [
                          //         Image(
                          //           image: AssetImage("assets/home/dot-2.png"),
                          //           width: 8,
                          //           height: 8,
                          //           color: Color(int.parse(
                          //               "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                          //           fit: BoxFit.fill,
                          //         ),
                          //         SizedBox(
                          //           width: 8,
                          //         ),
                          //         Image(
                          //           image: AssetImage("assets/home/dot-1.png"),
                          //           width: 8,
                          //           height: 8,
                          //           fit: BoxFit.fill,
                          //           color: Colors.grey.shade200,
                          //         ),
                          //         SizedBox(
                          //           width: 8,
                          //         ),
                          //         Image(
                          //           image: AssetImage("assets/home/dot.png"),
                          //           width: 8,
                          //           height: 8,
                          //           fit: BoxFit.fill,
                          //           color: Colors.grey.shade200,
                          //         ),
                          //       ],
                          //     ),
                          //   ),
                          // ),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      topLeft: Radius.circular(30)),
                                ),
                              )),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 20, bottom: 5),
                              child: isLoading
                                  ? SizedBox(
                                      width: 50,
                                      height: 50,
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          child:
                                              SpinKitView(themeIsDark: false)),
                                    )
                                  : InkWell(
                                      onTap: () async {
                                        if (widget.productModel.infavorite ==
                                            1) {
                                          onRemoveFromFav();
                                        } else {
                                          onAddToFav();
                                        }
                                      },
                                      child:widget.isFavourite? SizedBox(
                                          height: 50,
                                          width: 50,
                                          child:
                                              isLiked
                                                  ? Image(
                                                      image: AssetImage(
                                                          "assets/home/filledHeart.png"),
                                                      height: 32,
                                                      width: 32,
                                                    )
                                                  : Image(
                                                      image: AssetImage(
                                                          "assets/home/unfilledheart.png"),
                                                      height: 32,
                                                      width: 32,
                                                    )):SizedBox(
                                          height: 50,
                                          width: 50,
                                          child:
                                              widget.productModel.infavorite ==
                                                      1
                                                  ? Image(
                                                      image: AssetImage(
                                                          "assets/home/filledHeart.png"),
                                                      height: 32,
                                                      width: 32,
                                                    )
                                                  : Image(
                                                      image: AssetImage(
                                                          "assets/home/unfilledheart.png"),
                                                      height: 32,
                                                      width: 32,
                                                    )),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        // borderRadius: BorderRadius.only(
                        //     topRight: Radius.circular(30),
                        //     topLeft: Radius.circular(30)),
                      ),
                      width: AppConfig(context).width,
                      // height: AppConfig(context).height,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: AppText.customText(
                                  title: widget.productModel.productdesc +
                                      " " +
                                      widget.productModel.packsize,
                                  size: 20,
                                  fontWeight: FontWeights.semi_bold,
                                  color: AppColor.DarkText),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  AppText.customText(
                                      title:
                                          "AED ${widget.productModel.baseprice}",
                                      size: 20,
                                      fontWeight: FontWeights.semi_bold,
                                      color: Color(int.parse(
                                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))),
                                  Spacer(),
                                  ProductCountTile(
                                    isCart: false,
                                    onChange: (value) {
                                      print(value);
                                      setState(() {
                                        quantity = value;
                                      });
                                      print(quantity);
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey.shade300,
                            ),

                            ///TODO:Description for product
                            // SizedBox(
                            //   height: 15,
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.symmetric(horizontal: 20),
                            //   child: AppText.customText(
                            //       title: "Description",
                            //       size: 16,
                            //       fontWeight: FontWeights.semi_bold,
                            //       color: AppColor.DarkText),
                            // ),
                            // SizedBox(
                            //   height: 15,
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.symmetric(horizontal: 20),
                            //   child: AppText.customText(
                            //       title:
                            //           "Tellus, nunc at enim turpis auctor purus amet urna facibus. Enim laoreet fringilla tempor, odio arcu. Viverra nec etiam habitant rutrum at donec honcus sociis nunc at enim turpis auctor purus.",
                            //       size: 12,
                            //       fontWeight: FontWeights.regular,
                            //       color: AppColor.textGREY),
                            // ),
                            SizedBox(
                              height: 15,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: AppText.customText(
                                  title: "Related Products",
                                  size: 16,
                                  fontWeight: FontWeights.semi_bold,
                                  color: AppColor.DarkText),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            ProductHorizentolListView(
                              isNewArival: widget.isNewArival,
                              isFavourite: widget.isFavourite,
                              productsList: widget.relatedProductsList,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
