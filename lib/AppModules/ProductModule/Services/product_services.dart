import 'dart:convert';

import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:flutter/cupertino.dart';

Future<List<ProductModel>> getProductsDataService({
  required BuildContext context,
  required String brandCode,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_PRODUCT_BY_BRAND + "${user["customerid"]}/$brandCode",
      context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get brand Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Product")) == 0) {
      print("brand Inserted to SQL");
      await DBProvider.db.createProducts(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getProducts());
    } else {
      print("brand Updated to SQL");
      await DBProvider.db.updateProducts(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getProducts());
    }

    return ProductModel.jsonToProductsList(
        jsonDecode(response.body)["data"]["products"]);
  } else {
    return [];
  }
}

getPackSizeDataService({
  required BuildContext context,
  required String brandCode,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_PRODUCT_BY_BRAND + "${user["customerid"]}/$brandCode",
      context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get packe  size Data");

    return jsonDecode(response.body)["data"]["packsizes"];
  } else {
    return [];
  }
}
