import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Model/fav_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/FavouriteProductTile.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewModel/favourite_view_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ProductDetailViewController.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MyFavouriteViewController extends StatefulWidget {
  const MyFavouriteViewController({Key? key}) : super(key: key);

  @override
  _MyFavouriteViewControllerState createState() =>
      _MyFavouriteViewControllerState();
}

class _MyFavouriteViewControllerState extends State<MyFavouriteViewController> {
  final favVM = Get.put(FavProductViewModel());

  final categoryController = Get.put(HomeCategoryController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // favVM.getFavProductsData(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              height: AppConfig(context).height,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InnerAppBar(title: "My Favourites"),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: FutureBuilder<List<ProductModel>>(
                          future: favVM.getFavProductsData(context),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: FavShimmerView(),
                                // child: SpinKitView(themeIsDark: false),
                              );
                            }
                            return Obx(
                              () => favVM.favProductsData.isEmpty
                                  ? Center(
                                      child: AppText.customText(
                                          title: "No Favourite Product"),
                                    )
                                  : ListView.builder(
                                      itemCount:
                                          favVM.favProductsData.value.length,
                                      physics: BouncingScrollPhysics(),
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) {
                                        return InkWell(
                                          onTap: () {
                                            Get.to(
                                                () =>
                                                    ProductDetailViewController(
                                                      isNewArival: false,
                                                      isFavourite: true,
                                                      index: index,
                                                      productModel: favVM
                                                          .favProductsData
                                                          .value[index],
                                                      relatedProductsList: favVM
                                                          .favProductsData
                                                          .value,
                                                    ),
                                                transition:
                                                    Transition.rightToLeft);
                                          },
                                          child: FavouriteProductTile(
                                            productModel: favVM
                                                .favProductsData.value[index],
                                          ),
                                        );
                                      },
                                    ),
                            );
                          }),
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
