import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/home_view_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Model/fav_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Services/fourite_services.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewModel/favourite_view_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FavouriteProductTile extends StatefulWidget {
  final ProductModel productModel;

  FavouriteProductTile({Key? key, required this.productModel})
      : super(key: key);

  @override
  State<FavouriteProductTile> createState() => _FavouriteProductTileState();
}

class _FavouriteProductTileState extends State<FavouriteProductTile> {
  final favVM = Get.put(FavProductViewModel());

  // bool isLoading = false;
  final homeVM = Get.put(HomeViewModel());
  final categoryController = Get.put(HomeCategoryController());
  onRemoveFromFav() async {
    print("On Remove");

    favVM.removeFromFav(context, widget.productModel);

    bool disLikeResult = await removeFromFavouriteService(
        context: context, productId: widget.productModel.productid.toString());

    if (disLikeResult) {
      await  getFavProductsDataService(context: context);
      // await  getFavProductsDataService(context: context);
    //   await favVM.getUpdateFavProductsData(context);
    //   await homeVM.getUpdatedHomeData(context);
    } else {}
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        height: 105,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [AppColor().boxShadow]),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            children: [
              Container(
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                  color: Colors.blue.shade50.withOpacity(0.5),
                  // color: Color(0xFFF9F9FC),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: cacheImageView(image: widget.productModel.image),
                ),
              ),
              SizedBox(
                width: 14,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        AppText.customText(
                          title: widget.productModel.packsize,
                          size: 12,
                          color: AppColor.textGREY,
                          fontWeight: FontWeights.regular,
                        ),
                        Spacer(),
                        // isLoading
                        //     ? SizedBox(
                        //         height: 30,
                        //         width: 30,
                        //         child: ClipRRect(
                        //             borderRadius: BorderRadius.circular(50),
                        //             child: SpinKitView(
                        //               themeIsDark: false,
                        //               size: 30,
                        //             )),
                        //       )
                        //     :
                        InkWell(
                          onTap: () async {
                            onRemoveFromFav();
                            // // setState(() {
                            // //   isLoading = !isLoading;
                            // // });
                            // bool likeResult = await removeFromFavouriteService(
                            //     context: context,
                            //     productId:
                            //         widget.productModel.productid.toString());
                            // // setState(() {
                            // //   isLoading = !isLoading;
                            // // });
                            // if (likeResult) {
                            //   await favVM.getFavProductsData(context);
                            // }
                          },
                          child: Image(
                            image: AssetImage("assets/home/filledHeart.png"),
                            height: 30,
                            width: 30,
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.end,
                    ),
                    AppText.customText(
                      title: widget.productModel.productdesc,
                      overFlowText: TextOverflow.ellipsis,
                      size: 16,
                      color: AppColor.DarkText,
                      fontWeight: FontWeights.semi_bold,
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    AppText.customText(
                      title: "AED ${widget.productModel.baseprice}",
                      size: 14,
                      color: Color(int.parse(
                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                      fontWeight: FontWeights.semi_bold,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
