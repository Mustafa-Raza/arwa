
class FavouriteModel {
  FavouriteModel({
    // required this.id,
    required this.productid,
    required this.productdesc,
    // required this.country,
    // required this.brandcode,
    // required this.flavourcode,
    required this.packsize,
    required this.baseprice,
    required this.discountprice,
    required this.image,
    required this.favoriteid,
    // required this.producttype,
  });

  // int id;
  int favoriteid;
  String productid;
  String productdesc;
  // String country;
  // String brandcode;
  // String flavourcode;
  String packsize;
  double baseprice;
  double discountprice;
  String image;
  // String producttype;

  factory FavouriteModel.fromJson(Map<String, dynamic> json) => FavouriteModel(
    // id: json["id"]??0,
    favoriteid: json["favoriteid"]??0,
    productid: json["productid"]??"",
    productdesc: json["productdesc"]??"",
    // country: json["country"]??"",
    // brandcode: json["brandcode"]??"",
    // flavourcode: json["flavourcode"]??"",
    packsize: json["packsize"]??"",
    baseprice: json["baseprice"]??0.0,
    discountprice: json["discountprice"]??0.0,
    image: json["image"]??"",
    // producttype: json["producttype"]??"",
  );

  static List<FavouriteModel> jsonToFAVProductsList(List<dynamic> emote) =>
      emote.map<FavouriteModel>((item) => FavouriteModel.fromJson(item)).toList();
}

// class PackSizeModel{
//   String packSize;
//   PackSizeModel({required this.packSize});
//   factory PackSizeModel.fromJson(Map<String, dynamic> json) => ProductModel(
//     packSize: json["id"],
//
//   );
//
// }
