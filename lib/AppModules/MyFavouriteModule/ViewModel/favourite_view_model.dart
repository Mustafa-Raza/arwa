import 'dart:convert';

import 'package:arwa/AppModules/CategoriesModule/Services/category_services.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Model/fav_model.dart';
import 'package:arwa/AppModules/MyFavouriteModule/Services/fourite_services.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/Services/product_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class FavProductViewModel extends GetxController {
  var favProductsData = [].obs;
  RxBool onUnauthorized = false.obs;

  Future<List<ProductModel>> getFavProductsData(BuildContext context) async {
    List<ProductModel> favProductsModel = [];
    // productsModel =
    // await getProductsDataService(context: context, brandCode: brandCode);
    if ((await DBProvider.db.checkDataExistenceByLength("Favourite")) == 0) {
      favProductsModel = await getFavProductsDataService(
          context: context,
          onUnauthorized: (value) {
            onUnauthorized.value = value;
          });
    } else {
      print("favProductsModel is fetched in from local");
      // //
      // var localData = await DBProvider.db.getFavouriteList();
      // print(jsonDecode(localData[0]["favourite"])["data"]);
      //
      // favProductsModel = ProductModel.jsonToProductsList(
      //     jsonDecode(localData[0]["favourite"])["data"]);
      favProductsModel = await DBProvider.db.getFavouriteList();
      getFavProductsDataService(
          context: context,
          onUnauthorized: (value) {
            onUnauthorized.value = value;
          }).then((value) => {
            if (onUnauthorized.value != true)
              {
                print("favourite is fetched in background"),
                favProductsData.value = value,
              }
          });
    }

    favProductsData.value = favProductsModel;

    return favProductsModel;
  }

  Future<List<ProductModel>> getUpdateFavProductsData(
      BuildContext context) async {
    List<ProductModel> favProductsModel = [];
    // productsModel =
    // await getProductsDataService(context: context, brandCode: brandCode);
    favProductsModel = await getFavProductsDataService(
        context: context,
        onUnauthorized: (value) {
          onUnauthorized.value = value;
        });
    // if ((await DBProvider.db.checkDataExistenceByLength("Favourite")) == 0) {
    //   favProductsModel = await getFavProductsDataService(
    //       context: context,
    //       onUnauthorized: (value) {
    //         onUnauthorized.value = value;
    //       });
    // } else {
    //   print("favProductsModel is fetched in from local");
    //   //
    //   var localData = await DBProvider.db.getFavourite();
    //   // String value=jsonEncode(localData[0]["home"]);
    //   print(jsonDecode(localData[0]["favourite"])["data"]);
    //
    //   favProductsModel = ProductModel.jsonToProductsList(
    //       jsonDecode(localData[0]["favourite"])["data"]);
    //
    //   getFavProductsDataService(
    //       context: context,
    //       onUnauthorized: (value) {
    //         onUnauthorized.value = value;
    //       }).then((value) => {
    //
    //         if (onUnauthorized.value != true){
    //           print("favourite is fetched in background"),
    //           favProductsData.value = value,
    //         }
    //
    //       });
    // }

    favProductsData.value = favProductsModel;

    return favProductsModel;
  }

  removeFromFav(BuildContext context, ProductModel productModel) async {
    await DBProvider.db.deleteFavourite(productModel.id);
    favProductsData.value = await DBProvider.db.getFavouriteList();

  }

  addToFav(BuildContext context, ProductModel productModel) async {
    // favProductsData.value.add(productModel);
    await DBProvider.db.createFavourite(productModel);
    favProductsData.value = await DBProvider.db.getFavouriteList();

  }
}
