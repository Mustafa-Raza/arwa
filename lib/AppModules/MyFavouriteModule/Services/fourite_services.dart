import 'dart:convert';

import 'package:arwa/AppModules/MyFavouriteModule/Model/fav_model.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<List<ProductModel>> getFavProductsDataService({
  required BuildContext context,
  int limit = 0,
  Function? onUnauthorized,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_FAV_PRODUCTS + "${user["customerid"]}/${limit.toString()}",
      context: context);
  if (response == null) {
    return [];
  } else if (jsonDecode(response.body)["status"] == 200) {
    print("Get fav products Data");

    ///TODO:Save home data to Local DB

    if ((await DBProvider.db.checkDataExistenceByLength("Favourite")) == 0) {
      print("Favourite Inserted to SQL");
      await DBProvider.db.setFavouriteList(
          ProductModel.jsonToProductsList(jsonDecode(response.body)["data"]));
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getFavouriteList());
    } else {
      print("Favourite Updated to SQL");
      await DBProvider.db.deleteAllFavourite();
      await DBProvider.db.setFavouriteList(
          ProductModel.jsonToProductsList(jsonDecode(response.body)["data"]));
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getFavouriteList());
    }

    // if ((await DBProvider.db.checkDataExistenceByLength("Favourite")) == 0) {
    //   print("Favourite Inserted to SQL");
    //   await DBProvider.db.createFavourite(response.body.toString());
    //   // .createHome(jsonDecode(response.body)["data"].toString());
    //   print("SQLITE");
    //   print(await DBProvider.db.getFavouriteList());
    // } else {
    //   print("Favourite Updated to SQL");
    //   await DBProvider.db.updateFavourite(response.body.toString());
    //   // .updateHome(jsonDecode(response.body)["data"].toString());
    //   print("SQLITE");
    //   print(await DBProvider.db.getFavouriteList());
    // }

    return ProductModel.jsonToProductsList(jsonDecode(response.body)["data"]);
  } else if (jsonDecode(response.body)["status"] == 401) {
    ShowMessage().showErrorMessage(context, "User is Unauthorized");
    onUnauthorized!(true);
    return [];
  } else {
    return [];
  }
}

Future<bool> addToFavouriteService({
  required BuildContext context,
  required String productId,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    "customerid": "${user["customerid"]}",
    "productid": "$productId",
  };

  var response = await ApiCall().postRequestHeader(
      apiUrl: ADD_TO_FAV, bodyParameter: data, context: context);
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    // ShowMessage().showMessage(context, "Liked");
    return true;
  } else if (jsonDecode(response.body)["status"] == 403) {
    // ShowMessage().showErrorMessage(context, "Already Liked");
    return false;
  } else {
    return false;
  }
}

Future<bool> removeFromFavouriteService({
  required BuildContext context,
  required String productId,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    // "customerid": "${user["customerid"]}",
    // "productid": "$productId",
  };

  var response = await ApiCall().deleteRequestHeader(
    apiUrl: REMOVE_FROM_FAV + "${user["customerid"]}" + "/$productId",
    bodyParameter: data,
    context: context,
  );
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    // ShowMessage().showMessage(context, "Deleted From Favourite");
    return true;
  } else {
    return false;
  }
}
