import 'dart:convert';

import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<List<BrandsModel>> getCategoryDataService({
  required BuildContext context,
  Function ?onUnauthorized,
}) async {
  // var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_ALL_BRANDS , context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get brand Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Brand")) == 0) {
      print("brand Inserted to SQL");
      await DBProvider.db
          .createBrand(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getBrands());
    } else {
      print("brand Updated to SQL");
      await DBProvider.db
          .updateBrands(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getBrands());
    }


    return BrandsModel.jsonToBrandList(jsonDecode(response.body)["data"]);
  }else if (jsonDecode(response.body)["status"] == 401) {
    ShowMessage().showErrorMessage(context, "User is Unauthorized");
    onUnauthorized!(true);
    return [];
  } else {
    return [];
  }
}
