import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AllCategoryTile extends StatelessWidget {
  final bool isSelected;
  final BrandsModel brandsModel;

  AllCategoryTile(
      {Key? key, this.isSelected = false, required this.brandsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 15,
      ),
      child: Container(
        height: 120,
        // width: 90,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                color: isSelected
                    ? Color(
                        int.parse("0xFF${brandsModel.colorcode.split("#")[1]}"))
                    : Colors.transparent),
            boxShadow: [
              AppColor().boxShadow,
            ]),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              height: 93,
              width: 93,
              decoration: BoxDecoration(
                color: Color(0xFFF9F9FC), shape: BoxShape.circle,

                // boxShadow: [
                //   AppColor().boxShadow,
                // ]
              ),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: cacheImageView(image: brandsModel.image.trim())
              ),
            ),
            SizedBox(
              height: 10,
            ),
            AppText.customText(
                title: brandsModel.display,
                color: AppColor.DarkText,
                fontWeight: FontWeights.semi_bold,
                overFlowText: TextOverflow.ellipsis,
                size: 16),
          ],
        ),
      ),
    );
  }
}
