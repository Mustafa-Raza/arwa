import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrandsTile extends StatelessWidget {
  final bool isSelected;
  final BrandsModel brandsModel;

  BrandsTile({Key? key, this.isSelected = false, required this.brandsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 15,
      ),
      child: Container(
        height: 120,
        width: 90,
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      color: isSelected
                          ? Color(int.parse("0xFF${brandsModel.colorcode.split("#")[1]}"))
                          : Colors.transparent),
                  boxShadow: [
                    AppColor().boxShadow,
                  ]),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child:  cacheImageView(image:  brandsModel.image),
              ),
            ),
            Spacer(),
            AppText.customText(
                title: brandsModel.display,
                color: Color(0xFF1D2733),
                fontWeight: FontWeights.medium,
                size: 12),
          ],
        ),
      ),
    );
  }
}
