import 'dart:io';

import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/CategoriesModule/View/AllCategoryTile.dart';
import 'package:arwa/AppModules/CategoriesModule/View/category_shimmer_view.dart';
import 'package:arwa/AppModules/CategoriesModule/ViewModel/categories_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/ProductModule/ViewController/ArwaAllProductListViewController.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppBar/SecondaryAppBar.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AllCategoryViewController extends StatefulWidget {
  final bool isSecondary;

  // final List<BrandsModel> brandsList;

  AllCategoryViewController({Key? key, this.isSecondary = false})
      : super(key: key);

  @override
  _AllCategoryViewControllerState createState() =>
      _AllCategoryViewControllerState();
}

class _AllCategoryViewControllerState extends State<AllCategoryViewController> {
  int selectedIndex = 0;
  final categoryController = Get.put(HomeCategoryController());
  final brandVM = Get.put(CategoryViewModel());
  Future<List<BrandsModel>>? getBrands;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBrands = brandVM.getBrandsData(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Container(
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            // color: AppColor.primaryColor,
            child: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                ),
                width: AppConfig(context).width,
                height: AppConfig(context).height - 50,
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    widget.isSecondary
                        ? Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: InnerAppBar(
                              title: "Categories",
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: SecondaryAppBar(
                              title: 'Categories',
                            ),
                          ),
                    SizedBox(
                      height: 30,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: FutureBuilder<List<BrandsModel>>(
                            future: getBrands,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return Center(
                                  child: CategoryShimmerView(),
                                );
                              }

                              return Obx(
                                () => (GridView.builder(
                                  shrinkWrap: true,
                                  itemCount: brandVM.brandsData.length,
                                  // padding: const EdgeInsets.all(4.0),
                                  // physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          childAspectRatio: 1,
                                          mainAxisSpacing: 30.0,
                                          crossAxisSpacing: 0.0,
                                          crossAxisCount: 2),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        setState(() {
                                          selectedIndex = index;
                                          categoryController
                                                  .selectedBrand.value =
                                              brandVM.brandsData[index];
                                          // Get.find<HomeCategoryController>().selectedBrand.value=brandsModel[index];
                                        });

                                        Get.to(
                                            () =>
                                                ArwaAllProductListViewController(
                                                  products: categoryController
                                                      .selectedBrand
                                                      .value
                                                      .products,
                                                  brandCode: brandVM
                                                      .brandsData[index]
                                                      .brandcode,
                                                ),
                                            transition: Transition.rightToLeft);
                                      },
                                      child: AllCategoryTile(
                                        isSelected: selectedIndex == index,
                                        brandsModel: brandVM.brandsData[index],
                                      ),
                                    );
                                  },
                                )),
                              );
                            }),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
