import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/CategoriesModule/View/BrandsTile.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BrandsListView extends StatefulWidget {
  final List<BrandsModel> brandList; BrandsListView({Key? key,required this.brandList}) : super(key: key);

  @override
  _BrandsListViewState createState() => _BrandsListViewState();
}

class _BrandsListViewState extends State<BrandsListView> {
  int selectedIndex = 0;
  final categoryController=Get.put(HomeCategoryController());
  // List<BrandsModel> brandsModel = [
  //   // BrandsModel(
  //   //   title: "Arwa",
  //   //   imgSRC: "assets/home/brands/arwa.png",
  //   //   color: AppColor.primaryColor,themeColor: Color(0xFF75C650),
  //   //
  //   // ),
  //   // BrandsModel(
  //   //     title: "Sparkling",
  //   //     imgSRC: "assets/home/brands/sparkling.png",themeColor: Color(0xFF75C650),
  //   //     color: Color(0xFF0759AA)),
  //   // BrandsModel(
  //   //     title: "Coffee",
  //   //     imgSRC: "assets/home/brands/Coffee.png",themeColor: Color(0xFF89182E),
  //   //     color: Color(0xFF89182E)),
  //   // BrandsModel(
  //   //     title: "Chocolate",
  //   //     imgSRC: "assets/home/brands/chocolate.png",themeColor: Color(0xFFFF2D5F),
  //   //     color: Color(0xFFFF2D5F)),
  //   // BrandsModel(
  //   //     title: "Snakes",
  //   //     imgSRC: "assets/home/brands/snaks.png",themeColor: Color(0xFFEA9D2D),
  //   //     color: Color(0xFFEA9D2D)),
  // ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      width: AppConfig(context).width,
      child:ListView.builder(
        shrinkWrap: true,
        itemCount: widget.brandList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              setState(() {
                selectedIndex = index;
                categoryController.selectedBrand.value= widget.brandList[index];
                print(categoryController.selectedBrand.value.colorcode.split("#")[1]);
                // Get.find<HomeCategoryController>().selectedBrand.value=brandsModel[index];
              });
            },
            child: Padding(
              padding: EdgeInsets.only(
                  right: index + 1 ==  widget.brandList.length ? 20 : 0),
              child: Obx(() =>BrandsTile(
                isSelected:  categoryController.selectedBrand.value.display ==  widget.brandList[index].display,
                // isSelected: selectedIndex == index,
                brandsModel:  widget.brandList[index],
              ),
            ),
            ),
          );
        },
      ),
    );
  }
}
