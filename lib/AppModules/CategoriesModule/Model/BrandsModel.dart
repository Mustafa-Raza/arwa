import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:flutter/cupertino.dart';

class BrandsModel {
  BrandsModel({
    required this.id,
    required this.brandcode,
    required this.image,
    required this.display,
    required this.namedb,
    required this.priority,
    required this.colorcode,
    required this.newArrivalsProducts,
    required this.products,
  });

  int id;
  String brandcode;
  String image;
  String display;
  String namedb;
  int priority;
  String colorcode;
  List<ProductModel> products;
  List<ProductModel> newArrivalsProducts;


  factory BrandsModel.fromJson(Map<String, dynamic> json) => BrandsModel(
        products: ProductModel.jsonToProductsList(json["products"]),
        newArrivalsProducts:
            ProductModel.jsonToProductsList(json["newarrivals"]),
        id: json["id"],
        brandcode: json["brandcode"],
        image: json["image"],
        display: json["display"],
        namedb: json["namedb"],
        priority: json["priority"],
        colorcode: json["colorcode"],
      );

  static List<BrandsModel> jsonToBrandList(List<dynamic> emote) =>
      emote.map<BrandsModel>((item) => BrandsModel.fromJson(item)).toList();
}
