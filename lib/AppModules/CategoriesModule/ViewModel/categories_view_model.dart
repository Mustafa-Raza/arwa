import 'dart:convert';

import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/CategoriesModule/Services/category_services.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/HomeModule/Services/home_service.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class CategoryViewModel extends GetxController {
  var brandsData = [].obs;
  RxInt currentSlider = 0.obs;
  RxBool onUnauthorized = false.obs;

  Future<List<BrandsModel>> getBrandsData(BuildContext context) async {
    List<BrandsModel> brandsModel = [];
    if ((await DBProvider.db.checkDataExistenceByLength("Brand")) == 0) {
      brandsModel = await getCategoryDataService(context: context,onUnauthorized: (value){

      });
    } else {
      print("Category is fetched in from local");
      //
      var localData = await DBProvider.db.getBrands();
      // String value=jsonEncode(localData[0]["home"]);
      print(jsonDecode(localData[0]["brands"])["data"]);

      brandsModel = BrandsModel.jsonToBrandList(
          jsonDecode(localData[0]["brands"])["data"]);

      getCategoryDataService(context: context,onUnauthorized: (value){
        onUnauthorized.value=value;
      }).then((value) => {
            print("Category is fetched in background"),
            if(onUnauthorized.value!=true)
            brandsData.value = value,
            // homeModel = value,
          });
    }

    brandsData.value = brandsModel;

    return brandsModel;
  }
}
