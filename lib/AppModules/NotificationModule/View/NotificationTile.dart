import 'package:arwa/AppModules/NotificationModule/Model/notification_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';

class NotificationTile extends StatelessWidget {
  // final PaymentListModel model;
final NotificationModel notificationModel;
  NotificationTile({
    Key? key,
    required this.notificationModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20,left: 20,top: 10,bottom: 10),
      child: Material(
        child: InkWell(
          onTap: () {

          },
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.shade200, blurRadius: 15,
                    offset: Offset(3, 8),
                    // color: Color(0xFF0000001A),blurRadius: 15,offset: Offset(0,8),
                  ),
                ]),
            width: AppConfig(context).width,
            // height: 71,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16,
                  ),
                  AppText.customText(
                      title: notificationModel.title,
                      color: AppColor.DarkText,
                      fontWeight: FontWeights.semi_bold,
                      size: 15),
                  SizedBox(
                    height: 7,
                  ),
                  AppText.customText(
                      title: notificationModel.body,
                      color: Color(0xFF999B9F),
                      fontWeight: FontWeights.regular,
                      size: 12),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
