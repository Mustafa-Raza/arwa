import 'dart:convert';
import 'package:arwa/AppModules/NotificationModule/Model/notification_model.dart';
import 'package:arwa/AppModules/NotificationModule/Services/notification_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class NotificationViewModel extends GetxController {
  // final List<Map<String, List<HistoryModel>>>
  var notificationData = [].obs;

  Future<List<Map<String, List<NotificationModel>>>> getNotificationData(
      {required BuildContext context}) async {
    List<Map<String, List<NotificationModel>>> notificationModel = [];
    List<dynamic> resultData = [];
    // await getHistoryDataService(context: context);

    if ((await DBProvider.db.checkDataExistenceByLength("Notification")) == 0) {
      resultData = await getNotificationDataService(context: context);
    } else {
      print("Notification is fetched in from local");
      //
      var localData = await DBProvider.db.getNotification();
      print(localData);
      print("localData");
      print(jsonDecode(localData[0]["notification"])["data"]);
      resultData = jsonDecode(localData[0]["notification"])["data"];
      // ProductModel.jsonToProductsList(
      // jsonDecode(localData[0]["history"])["data"]);

      getNotificationDataService(context: context).then((value) => {
            print("Notification is fetched in background"),
            if (value.isNotEmpty)
              {
                notificationData.clear(),
                for (int i = 0; i < value.length; i++)
                  {
                    notificationData.add({
                      "${value[i].keys.toList()[0]}":
                          NotificationModel.jsonToNotificationList(
                              value[i].values.toList()[0]),
                    }),
                  }
              }
          });
    }

    for (int i = 0; i < resultData.length; i++) {
      notificationModel.add({
        "${resultData[i].keys.toList()[0]}":
            NotificationModel.jsonToNotificationList(
                resultData[i].values.toList()[0]),
      });
    }
    notificationData.value = notificationModel;
    debugPrint("resultData");
    debugPrint(resultData.toString());
    return notificationModel;
  }
}
