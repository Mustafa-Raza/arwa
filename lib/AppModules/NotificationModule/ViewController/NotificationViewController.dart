import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/NotificationModule/Model/notification_model.dart';
import 'package:arwa/AppModules/NotificationModule/View/NotificationTile.dart';
import 'package:arwa/AppModules/NotificationModule/ViewModel/notification_view_model.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:group_list_view/group_list_view.dart';

class NotificationViewController extends StatefulWidget {
  const NotificationViewController({Key? key}) : super(key: key);

  @override
  _NotificationViewControllerState createState() =>
      _NotificationViewControllerState();
}

class _NotificationViewControllerState
    extends State<NotificationViewController> {
  final categoryController = Get.put(HomeCategoryController());
  final notificationVM = Get.put(NotificationViewModel());
  Future<List<Map<String, List<NotificationModel>>>>? getNotifications;

  // Map<String, List> _elements = {
  //   'Today': ['Klay Lewis', 'Ehsan Woodard', 'River Bains'],
  //   'Yesterday': ['Toyah Downs', 'Tyla Kane'],
  //   '12/11/2021': ['Toyah Downs', 'Tyla Kane'],
  //   '13/11/2021': ['Toyah Downs', 'Tyla Kane'],
  //   '14/11/2021': ['Toyah Downs', 'Tyla Kane'],
  //   '15/11/2021': ['Toyah Downs', 'Tyla Kane'],
  // };
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNotifications = notificationVM.getNotificationData(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: InnerAppBar(
                    title: "Notifications",
                  ),
                ),
                // Expanded(
                //     child:
                //     ListView.builder(
                //   itemCount: 3,
                //   itemBuilder: (context, index) {
                //     return NotificationTile();
                //   },
                // )),
                Expanded(
                  child:
                      FutureBuilder<List<Map<String, List<NotificationModel>>>>(
                          future: getNotifications,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Center(
                                  child: FavShimmerView(),
                                ),
                              );
                            }

                            return Obx(
                              () => notificationVM.notificationData.value.isNotEmpty?GroupListView(
                                physics: BouncingScrollPhysics(),
                                sectionsCount: notificationVM
                                    .notificationData.value.length,
                                countOfItemInSection: (int section) {
                                  return notificationVM
                                      .notificationData.value[section].values
                                      .toList()[0]
                                      .length;
                                },
                                itemBuilder:
                                    (BuildContext context, IndexPath index) {
                                  return NotificationTile(
                                    notificationModel: notificationVM
                                        .notificationData
                                        .value[index.section]
                                        .values
                                        .toList()[0][index.index],
                                  );
                                },
                                groupHeaderBuilder:
                                    (BuildContext context, int section) {
                                  return Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20,
                                          right: 15,
                                          top: 5,
                                          bottom: 5),
                                      child: AppText.customText(
                                          title: notificationVM.notificationData
                                              .value[section].keys
                                              .toList()[0],
                                          color: AppColor.primaryColor,
                                          size: 18));
                                },
                                // separatorBuilder: (context, index) => NotificationTile(),
                                // sectionSeparatorBuilder: (context, section) => SizedBox(height: 10),
                              ):Center(child:  AppText.customText(
                                  title: "No Notification Found",
                                  color: AppColor.primaryColor,
                                  size: 18),)
                            );
                          }),
                ),
              ],
            ),
          ),
        ));
  }
}
