

class NotificationModel {
  NotificationModel({
   required this.id,
    required this.customerid,
    required    this.title,
    required this.body,
    required  this.datetime,
  });

  int id;
  String customerid;
  String title;
  String body;
  DateTime datetime;

  factory NotificationModel.fromJson(Map<String, dynamic> json) => NotificationModel(
    id: json["id"],
    customerid: json["customerid"],
    title: json["title"],
    body: json["body"],
    datetime: DateTime.parse(json["datetime"]),
  );

  static List<NotificationModel> jsonToNotificationList(List<dynamic> emote) =>
      emote.map<NotificationModel>((item) => NotificationModel.fromJson(item)).toList();
  Map<String, dynamic> toJson() => {
    "id": id,
    "customerid": customerid,
    "title": title,
    "body": body,
    "datetime": "${datetime.year.toString().padLeft(4, '0')}-${datetime.month.toString().padLeft(2, '0')}-${datetime.day.toString().padLeft(2, '0')}",
  };
}
