import 'dart:convert';

import 'package:arwa/AppModules/NotificationModule/Model/notification_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:flutter/cupertino.dart';

Future< List<dynamic>> getNotificationDataService({
  required BuildContext context,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_NOTIFICATIONS + "${user["customerid"]}", context: context);
  if (response == null) {
    return [];
  } else if (jsonDecode(response.body)["status"] == 200) {
    print("Get Notification Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Notification")) == 0) {
      print("History Inserted to SQL");
      await DBProvider.db.createNotification(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getNotification());
    } else {
      print("Notification Updated to SQL");
      await DBProvider.db.updateNotification(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getNotification());
    }

    return jsonDecode(response.body)["data"];
  } else {
    return [];
  }
}