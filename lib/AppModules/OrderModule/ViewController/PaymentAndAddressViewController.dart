import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/OrderModule/ViewModel/order_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/View/PaymentMethodsView.dart';
import 'package:arwa/AppModules/AddressModule/ViewController/SelectAddressView.dart';
import 'package:arwa/AppModules/OrderModule/ViewController/ThankUViewController.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arwa/AppModules/Utills/AppBar/SecondaryAppBar.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:get/get.dart';

class PaymentAndAddressViewController extends StatefulWidget {
  final List<CartModel> cartList;

  final String totalBill;

  PaymentAndAddressViewController(
      {Key? key, required this.cartList, required this.totalBill})
      : super(key: key);

  @override
  _PaymentAndAddressViewControllerState createState() =>
      _PaymentAndAddressViewControllerState();
}

class _PaymentAndAddressViewControllerState
    extends State<PaymentAndAddressViewController> {
  final orderVM = Get.put(OrderViewModel());
  final categoryController = Get.put(HomeCategoryController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
          height: 149,
          width: AppConfig(context).width,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(color: Colors.grey.shade300, blurRadius: 15)
          ]),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    AppText.customText(
                        title: "Total",
                        color: AppColor.DarkText,
                        size: 16,
                        fontWeight: FontWeights.semi_bold),
                    AppText.customText(
                        title: "(incl.VAT)",
                        color: AppColor.textGREY,
                        size: 12,
                        fontWeight: FontWeights.regular),
                    Spacer(),
                    AppText.customText(
                        title: "AED ${widget.totalBill}",
                        color: AppColor.DarkText,
                        size: 16,
                        fontWeight: FontWeights.semi_bold),
                  ],
                ),
                Obx(
                  () => orderVM.isLoading.value
                      ? spinKitButton(
                          context,
                          46,
                          AppConfig(context).width,
                          Color(int.parse(
                              "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")))
                      : primaryButton(
                          buttonTitle: "Place Order",
                          callback: () {
                            orderVM.onPlaceOrder(
                                context: context,
                                cartList: widget.cartList,
                                onPlaceOrder: (orderNumber, DeliveryDate) {
                                  Get.offAll(() => ThanksViewController(
                                        deliveryDate: DeliveryDate,
                                        orderNumber: orderNumber,
                                      ));
                                });
                          },
                          width: AppConfig(context).width,
                          btnColor: Color(int.parse(
                              "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                          txtColor: AppColor.white,
                          height: 46,
                          fontweight: FontWeights.medium,
                        ),
                ),
              ],
            ),
          ),
        ),
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            height: AppConfig(context).height,
            width: AppConfig(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: InnerAppBar(
                      title: "Payment and Address",
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 39,
                          ),
                          SelectAddressView(),
                          PaymentMethodsView(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
