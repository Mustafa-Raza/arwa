import 'dart:io';

import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/SignUpViewController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/RootPageModule/AppRootPageController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class ThanksViewController extends StatefulWidget {
  final String deliveryDate;
  final String orderNumber;
  ThanksViewController({Key? key,required this.deliveryDate,required this.orderNumber}) : super(key: key);

  @override
  _ThanksViewControllerState createState() => _ThanksViewControllerState();
}

class _ThanksViewControllerState extends State<ThanksViewController> {
  @override
  Widget build(BuildContext context) {

    final categoryController = Get.put(HomeCategoryController());

    print(AppConfig(context).height - 320);
    return Scaffold(
        body: Container(
      width: AppConfig(context).width,
      height: AppConfig(context).height,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage("assets/auth/login_back.png"),
              width: AppConfig(context).width,
              height: 322,
              fit: BoxFit.fill,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              height: AppConfig(context).height - 300,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AppText.customText(
                        title: "THANK YOU",
                        color: AppColor.primaryColor,
                        fontWeight: FontWeights.semi_bold,
                        size: 30,
                      ),
                      AppText.customText(
                        title: "For Your Order",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      AppText.customText(
                        title: "Order number:",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: AppColor.primaryColor,
                          borderRadius: BorderRadius.circular(22),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 28, vertical: 9),
                          child: AppText.customText(
                            title: "#${widget.orderNumber}",
                            color: AppColor.white,
                            fontWeight: FontWeights.medium,
                            size: 18,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 46,
                      ),
                      AppText.customText(
                        title: "DELIVERY TIME",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AppText.customText(
                        title: "${widget.deliveryDate}",
                        // title: "Sunday October 29, 2021",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.medium,
                        size: 18,
                      ),
                      SizedBox(
                        height: 60,
                      ),
                      primaryButton(
                        buttonTitle: "Return to home",
                        callback: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AppRootPageController(currentIndex: 2,),
                              ));
                        },
                        width: AppConfig(context).width - 60,
                        btnColor: Color(int.parse(
                            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                        txtColor: AppColor.white,
                        height: 46,
                        fontweight: FontWeights.medium,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
