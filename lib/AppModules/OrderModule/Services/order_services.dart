import 'dart:convert';

import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<Map<String ,dynamic>> orderService({
  required BuildContext context,
  required List<CartModel> cartList,
  required int id,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var data = {
    "addressid": id,
    "customerid": "${user["customerid"]}",
    "notes": "",
    "productdetails":CartModel.cartProductsListToJSON(cartList),
  };
  print("Order data");
  print(data);
  var response = await ApiCall().postRequestHeader(
      apiUrl: GENERATE_ORDER, bodyParameter: data, context: context);
  print(jsonDecode(response.body));
  if (response == null) {
    return {

    };
  } else if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Order Placed Successfully");
    return jsonDecode(response.body)["data"];
  } else {
    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);

    return {

    };
  }
}
