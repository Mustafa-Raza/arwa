import 'package:arwa/AppModules/CartModule/Model/CartModel.dart';
import 'package:arwa/AppModules/OrderModule/Services/order_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class OrderViewModel extends GetxController {
  RxInt selectedAddressID = 0.obs;
  RxBool isLoading = false.obs;

  onPlaceOrder(
      {required BuildContext context,
      required List<CartModel> cartList,required Function onPlaceOrder}) async {
    if (selectedAddressID.value == 0) {
      ShowMessage().showErrorMessage(context, "Please Add address");
    } else {
      isLoading.value = true;
      var result = await orderService(
          context: context, cartList: cartList, id: selectedAddressID.value);
      isLoading.value = false;
      if(result!={}){

        onPlaceOrder("${result["ordernumber"].toString()}","${result["deliverydate"]}",);
      }

    }
  }
}
