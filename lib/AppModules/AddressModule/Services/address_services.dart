
import 'dart:convert';

import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<List<AllAddressModel>> getAddressDataService({
  required BuildContext context,

}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_MY_ADDRESS
          + "${user["customerid"]}",
      context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get brand Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Address")) == 0) {
      print("Address Inserted to SQL");
      await DBProvider.db.createAddress(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getAddress());
    } else {
      print("Address Updated to SQL");
      await DBProvider.db.updateAddress(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getAddress());
    }

    return AllAddressModel. jsonToAddressList(
        jsonDecode(response.body)["data"]);
  } else {
    return [];
  }
}
Future<bool> removeFromAddressService({
  required BuildContext context,
  required String addressID,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    // "customerid": "${user["customerid"]}",
    // "productid": "$productId",
  };

  var response = await ApiCall().deleteRequestHeader(
    apiUrl: REMOVE_FROM_ADDRESS + "$addressID",
    bodyParameter: data,
    context: context,

  );
  if(response==null){
    return false;
  }else
  if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Deleted From Address");
    return true;
  }else{
    return false;
  }

}


Future<bool> addToAddressService({
  required BuildContext context,
  required String addresstype,
   required int id,
   required String area,
   required String building,
    String country="AE",
   required String floorunit,
   String latitude="",
   String longitude="",
   String note="",
   String address="",
   required String street,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data =
  {
  "addresstype": "$addresstype",
  "area": "$area",
  "building": "$building",
  "country": "$country",
  "customerid": "${user["customerid"]}",
  "floorunit": "$floorunit",
  "id": id,
  "latitude": "$latitude",
  "longitude": "$longitude",
  "note": "$note",
  "street": "$street",
  "address": "$address",
  };
  var response = await ApiCall().postRequestHeader(
      apiUrl: ADD_TO_ADDRESS, bodyParameter: data, context: context);
  if(response==null){
    return false;
  }else
  if (jsonDecode(response.body)["status"] == 200) {
    ShowMessage().showMessage(context, "Added to Address");
    return true;
  } else if (jsonDecode(response.body)["status"] == 403) {
    ShowMessage().showErrorMessage(context, "Already in Address");
    return false;
  }else{
    return false;
  }
}
