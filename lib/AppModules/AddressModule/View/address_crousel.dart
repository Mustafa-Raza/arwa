// import 'package:arwa/AppModules/AddressModule/View/SelectAddressTile.dart';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter/cupertino.dart';
//
// class AddressCarousel extends StatefulWidget {
//   const AddressCarousel({Key? key}) : super(key: key);
//
//   @override
//   _AddressCarouselState createState() => _AddressCarouselState();
// }
//
// class _AddressCarouselState extends State<AddressCarousel> {
//   @override
//   Widget build(BuildContext context) {
//     return CarouselSlider(
//       options: CarouselOptions(
//         height: 322,
//         // aspectRatio: 16 / 9,
//         // viewportFraction: 0.8,
//         initialPage: 0,
//         enableInfiniteScroll: true,
//         reverse: false,
//         autoPlay: true,
//         autoPlayInterval: Duration(seconds: 3),
//         autoPlayAnimationDuration: Duration(milliseconds: 800),
//         autoPlayCurve: Curves.fastOutSlowIn,
//         // enlargeCenterPage: true,
//         // onPageChanged: callbackFunction,
//         onPageChanged: (index, reason) {
//           print(index);
//
//         },
//         aspectRatio: 2.0,
//         disableCenter: true,
//         viewportFraction: 1,
//         enlargeCenterPage: false,
//         scrollDirection: Axis.horizontal,
//       ),
//       items: widget.slider.map((sale) {
//         return Builder(
//           builder: (BuildContext context) {
//             return SelectAddressTile(
//               addressModel:  addressVM.addressData.value[index],
//               isSelected: index == selectedIndex,
//             );
//           },
//         );
//       }).toList(),
//     );
//   }
// }
