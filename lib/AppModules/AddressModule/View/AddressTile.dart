import 'package:arwa/AppModules/PaymentModule/Model/AddressModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressTile extends StatelessWidget {
 final AddressModel addressModel;
 final bool isSelected;
 AddressTile({Key? key,required this.addressModel,required this.isSelected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        height: 90,
        decoration: BoxDecoration(
            // borderRadius: BorderRadius.circular(8),
            color: Colors.white,
            border: Border(bottom: BorderSide(
              color: Colors.grey.shade200 ,
            ))
            // boxShadow: [AppColor().boxShadow]
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 10, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  SizedBox(height: 3,),
                  Image(
                    image: AssetImage(isSelected? "assets/paymentAndAddress/Oval.png":"assets/paymentAndAddress/Oval-1.png"),
                    height: 15,
                    // width: 13,
                  ),
                ],
              ),
              SizedBox(
                width: 25,
              ),
              Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText.customText(
                        title: addressModel.title,
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 14,
                      ),
                      AppText.customText(
                        title:
                        addressModel.location,
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 12,
                      ),
                    ],
                  )),
              SizedBox(
                width: 20,
              ),

            ],
          ),
        ),
      ),
    );
  }
}
