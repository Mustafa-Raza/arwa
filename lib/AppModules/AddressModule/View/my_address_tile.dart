import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/Services/address_services.dart';
import 'package:arwa/AppModules/AddressModule/ViewModel/address_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/Model/AddressModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

class MyAddressTileView extends StatefulWidget {
  final AllAddressModel addressModel;

  MyAddressTileView({Key? key, required this.addressModel}) : super(key: key);

  @override
  State<MyAddressTileView> createState() => _MyAddressTileViewState();
}

class _MyAddressTileViewState extends State<MyAddressTileView> {
  bool isDeleting = false;
  final addressVM = Get.put(AddressViewModel());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: Slidable(
          endActionPane: ActionPane(
            motion: ScrollMotion(),
            extentRatio: 0.3,
            closeThreshold: 0.5,
            openThreshold: 0.1,
            children: [
              InkWell(
                child: Container(
                  color: Color(0xFFFF2D5F),
                  width: 80,
                  child: !isDeleting
                      ? Center(
                          child: Image(
                            image: AssetImage("assets/cart/Trash-alt.png"),
                            width: 23,
                            height: 23,
                          ),
                        )
                      : SpinKitView(
                          themeIsDark: false,
                        ),
                ),
                onTap: () async {
                  setState(() {
                    isDeleting = !isDeleting;
                  });

                  var result = await removeFromAddressService(
                      context: context,
                      addressID: widget.addressModel.id.toString());
                  setState(() {
                    isDeleting = !isDeleting;
                  });
                  if (result) {
                    await addressVM.getAddressData(context, (data) {});
                  }
                },
              ),
            ],
          ),
          //
          child: Container(
            width: AppConfig(context).width - 20,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppText.customText(
                  title: widget.addressModel.addresstype
                      .toString()
                      .capitalize
                      .toString(),
                  color: AppColor.DarkText,
                  fontWeight: FontWeights.semi_bold,
                  size: 18,
                ),
                SizedBox(
                  height: 15,
                ),
                AppText.customText(
                  title: widget.addressModel.address,
                  // "Floor" +
                  //     widget.addressModel.floorunit +
                  //     "," +
                  //     widget.addressModel.street +
                  //     " st " +
                  //     "," +
                  //     widget.addressModel.area +
                  //     "," +
                  //     widget.addressModel.country,
                  color: AppColor.textGREY,
                  fontWeight: FontWeights.regular,
                  size: 12,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
