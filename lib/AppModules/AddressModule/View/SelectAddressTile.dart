import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/PaymentModule/Model/AddressModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectAddressTile extends StatelessWidget {
  final AllAddressModel addressModel;
  final bool isSelected;

  SelectAddressTile(
      {Key? key, required this.isSelected, required this.addressModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        height: 90,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade300,
                blurRadius: 15,
                // offset: Offset(3, 8),
              )
            ]),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 3),
                child: addressModel.addresstype == "Home"
                    ? Image(
                        image: AssetImage("assets/paymentAndAddress/HOME.png"),
                        height: 13,
                        width: 13,
                        color: AppColor.primaryColor,
                      )
                    : addressModel.addresstype == "Office"
                        ? Image(
                            image: AssetImage(
                                "assets/paymentAndAddress/briefcase.png"),
                            height: 13,
                            color: AppColor.primaryColor,
                            width: 13,
                          )
                        : Image(
                            image: AssetImage(
                                "assets/paymentAndAddress/marker.png"),
                            height: 13,
                            color: AppColor.primaryColor,
                            width: 13,
                          ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppText.customText(
                    title: addressModel.addresstype,
                    color: AppColor.DarkText,
                    fontWeight: FontWeights.semi_bold,
                    size: 14,
                  ),
                  AppText.customText(
                    title: "Floor" +
                        addressModel.floorunit +
                        "," +
                        addressModel.street +
                        " st " +
                        "," +
                        addressModel.area +
                        "," +
                        addressModel.country,
                    color: AppColor.textGREY,
                    fontWeight: FontWeights.regular,
                    size: 12,
                  ),
                ],
              )),
              SizedBox(
                width: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage(isSelected
                        ? "assets/paymentAndAddress/select.png"
                        : "assets/paymentAndAddress/Unselect.png"),
                    height: 18,
                    width: 18,
                  ),
                ],
              ),
              SizedBox(
                width: 5,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
