class AllAddressModel {
  AllAddressModel({
    required this.id,
    required this.customerid,
    required this.building,
    required this.street,
    required this.note,
    required this.address,
    required this.area,
    required this.floorunit,
    required this.latitude,
    required this.country,
    required this.addresstype,
    required this.longitude,
  });

  int id;
  String customerid;
  String building;
  String street;
  String area;
  String address;
  String floorunit;
  String country;
  String note;
  String latitude;
  String addresstype;
  String longitude;

  factory AllAddressModel.fromJson(Map<String, dynamic> json) =>
      AllAddressModel(
        id: json["id"],
        customerid: json["customerid"],
        building: json["building"],
        street: json["street"],
        area: json["area"],
        note: json["note"],
        latitude: json["latitude"],
        address: json["address"],
        floorunit: json["floorunit"],
        country: json["country"],
        addresstype: json["addresstype"],
        longitude: json["longitude"],
      );

  static List<AllAddressModel> jsonToAddressList(List<dynamic> emote) => emote
      .map<AllAddressModel>((item) => AllAddressModel.fromJson(item))
      .toList();

  Map<String, dynamic> toJson() => {
        "id": id,
        "customerid": customerid,
        "building": building,
        "address": address,
        "note": note,
        "street": street,
        "area": area,
        "latitude": latitude,
        "longitude": longitude,
        "floorunit": floorunit,
        "country": country,
        "addresstype": addresstype,
      };
}
