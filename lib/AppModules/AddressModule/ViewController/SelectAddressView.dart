import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/ViewController/AddNewAddressViewController.dart';
import 'package:arwa/AppModules/AddressModule/ViewModel/address_view_model.dart';
import 'package:arwa/AppModules/OrderModule/ViewModel/order_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/Model/AddressModel.dart';
import 'package:arwa/AppModules/AddressModule/View/SelectAddressTile.dart';
import 'package:arwa/AppModules/AddressModule/ViewController/AddressModalView.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelectAddressView extends StatefulWidget {
  const SelectAddressView({Key? key}) : super(key: key);

  @override
  _SelectAddressViewState createState() => _SelectAddressViewState();
}

class _SelectAddressViewState extends State<SelectAddressView> {
  final addressVM = Get.put(AddressViewModel());
  final orderVM = Get.put(OrderViewModel());
  Future<List<AllAddressModel>>? getAddress;
  int selectedIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getAddress = addressVM.getAddressData(context, (data) {
      if (data.isNotEmpty) orderVM.selectedAddressID.value = data[0].id;
    });
    // if( addressVM.addressData.value.isNotEmpty)
    //   orderVM.selectedAddressID.value =
    //       addressVM.addressData.value[selectedIndex].id;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: [
                InkWell(
                    child: AppText.customText(
                  title: "Select Address",
                  color: AppColor.DarkText,
                  fontWeight: FontWeights.semi_bold,
                  size: 16,
                )),
                Spacer(),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFF2F9FF),
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          color: AppColor.primaryColor,
                          size: 16,
                        ),
                        InkWell(
                          onTap: () {
                            Get.to(() => AddNewAddressViewController(
                                  addressModel: AllAddressModel(
                                      building: "",
                                      latitude: "",
                                      longitude: "",
                                      note: "",
                                      id: 0,
                                      customerid: "",
                                      street: "",
                                      address: "",
                                      area: "",
                                      floorunit: "",
                                      country: "",
                                      addresstype: ""),
                                ));
                          },
                          child: InkWell(
                              child: AppText.customText(
                            title: "Add New",
                            color: AppColor.primaryColor,
                            fontWeight: FontWeights.semi_bold,
                            size: 11,
                          )),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: FutureBuilder<List<AllAddressModel>>(
                  // future: productVM.getProductsData(context,widget.brandCode),
                  future: getAddress,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return SizedBox(
                        height: 200,
                        child: Center(
                          child: ProductShimmerView(),
                        ),
                      );
                    }
                    return Obx(() => addressVM.addressData.value.isNotEmpty
                        ? CarouselSlider(
                            options: CarouselOptions(
                              height: 100,
                              // aspectRatio: 16 / 9,
                              // viewportFraction: 0.8,
                              initialPage: 0,
                              enableInfiniteScroll: false,
                              reverse: false,
                              autoPlay: false,
                              autoPlayInterval: Duration(seconds: 3),
                              autoPlayAnimationDuration:
                                  Duration(milliseconds: 800),
                              autoPlayCurve: Curves.fastOutSlowIn,
                              // enlargeCenterPage: true,
                              // onPageChanged: callbackFunction,
                              onPageChanged: (index, reason) {
                                print(index);

                                setState(() {
                                  orderVM.selectedAddressID.value = addressVM
                                      .addressData.value[selectedIndex].id;
                                  selectedIndex = index;
                                });
                              },
                              aspectRatio: 2.0,
                              disableCenter: true,
                              viewportFraction: 1,
                              enlargeCenterPage: false,
                              scrollDirection: Axis.horizontal,
                            ),
                            items: addressVM.addressData.value.map((address) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return SelectAddressTile(
                                    addressModel: address,
                                    isSelected: addressVM.addressData
                                            .value[selectedIndex].id ==
                                        address.id,
                                  );
                                },
                              );
                            }).toList(),
                          )
                        : Center(
                            child: AppText.customText(
                                title: "No Address Found",
                                color: AppColor.textGREY),
                          ));
                  }),
            ),
          )
        ],
      ),
    );
  }
}
