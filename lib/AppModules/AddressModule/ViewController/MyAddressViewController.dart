import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/View/my_address_tile.dart';
import 'package:arwa/AppModules/AddressModule/ViewController/AddNewAddressViewController.dart';
import 'package:arwa/AppModules/AddressModule/ViewModel/address_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MyAddressListView extends StatefulWidget {
  const MyAddressListView({Key? key}) : super(key: key);

  @override
  _MyAddressListViewState createState() => _MyAddressListViewState();
}

class _MyAddressListViewState extends State<MyAddressListView> {
  final addressVM = Get.put(AddressViewModel());
  Future<List<AllAddressModel>>? getAddress;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAddress = addressVM.getAddressData(context, (data) {});
  }

  final categoryController = Get.put(HomeCategoryController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
          color: Colors.white,
          height: 60,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: primaryButton(
              buttonTitle: "Add New Address",
              callback: () {
                Get.to(
                  () => AddNewAddressViewController(
                    addressModel:  AllAddressModel(
                        building: "",
                        latitude: "",
                        longitude: "",
                        note: "",
                        id: 0,
                        customerid: "",
                        street: "",
                        address: "",
                        area: "",
                        floorunit: "",
                        country: "",
                        addresstype: ""),
                  ),
                );
              },
              width: AppConfig(context).width - 40,
              btnColor: Color(int.parse(
                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
              txtColor: AppColor.white,
              height: 46,
              fontweight: FontWeights.medium,
            ),
          ),
        ),
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              height: AppConfig(context).height,
              width: AppConfig(context).width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InnerAppBar(
                          title: "My Addresses",
                        ),
                        SizedBox(
                          height: 39,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: FutureBuilder<List<AllAddressModel>>(
                              // future: productVM.getProductsData(context,widget.brandCode),
                              future: getAddress,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return SizedBox(
                                    height: AppConfig(context).height / 1.5,
                                    child: Center(
                                      child: FavShimmerView(),
                                    ),
                                  );
                                }
                                return Obx(
                                  () => addressVM.addressData.value.isNotEmpty
                                      ? ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          itemCount: addressVM
                                              .addressData.value.length,
                                          itemBuilder: (context, index) {
                                            return InkWell(
                                              onTap: () {
                                                Get.to(
                                                  () =>
                                                      AddNewAddressViewController(
                                                    addressModel: addressVM
                                                        .addressData
                                                        .value[index]
                                                        ,
                                                  ),
                                                );
                                              },
                                              child: MyAddressTileView(
                                                  addressModel: addressVM
                                                      .addressData
                                                      .value[index]),
                                            );
                                          },
                                        )
                                      : Center(
                                          child: AppText.customText(
                                            title: "No Address Found",
                                            color: AppColor.greyText,
                                            fontWeight: FontWeights.semi_bold,
                                            size: 16,
                                          ),
                                        ),
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
