import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/ViewController/AddNewAddressViewController.dart';
import 'package:arwa/AppModules/PaymentModule/Model/AddressModel.dart';
import 'package:arwa/AppModules/AddressModule/View/AddressTile.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future addressModalView({
  required BuildContext context,
}) {
  return showModalBottomSheet(
    backgroundColor: Colors.white.withOpacity(0),
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return Container(
            height: 500,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15), topLeft: Radius.circular(15)),
            ),
            child: AddressList(),
          );
        },
      );
    },
  );
}

class AddressList extends StatefulWidget {
  const AddressList({Key? key}) : super(key: key);

  @override
  _AddressListState createState() => _AddressListState();
}

class _AddressListState extends State<AddressList> {
  List<AddressModel> address = [
    AddressModel(
        title: "Home",
        isSelected: true,
        icon: "assets/paymentAndAddress/HOME.png",
        location:
            "381 Shirley St, Munster, Dubai United Arab Emirates - 10005"),
    AddressModel(
        title: "Office",
        isSelected: true,
        icon: "assets/paymentAndAddress/HOME.png",
        location:
            "381 Shirley St, Munster, Dubai United Arab Emirates - 10005"),
    AddressModel(
        title: "Other",
        isSelected: true,
        icon: "assets/paymentAndAddress/HOME.png",
        location:
            "381 Shirley St, Munster, Dubai United Arab Emirates - 10005"),
  ];
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image(
                      image: AssetImage("assets/paymentAndAddress/cancel.png"),
                      height: 15,
                      width: 15,
                    ),
                  ),
                  Spacer(),
                  InkWell(
                      child: AppText.customText(
                    title: "Deliver to",
                    color: AppColor.DarkText,
                    fontWeight: FontWeights.semi_bold,
                    size: 16,
                  )),
                  Spacer(),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                height: 1,
                color: Colors.grey.shade300,
              ),
              SizedBox(
                height: 10,
              ),
              ListView.builder(
                itemCount: address.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                    child: AddressTile(
                      isSelected: selectedIndex == index,
                      addressModel: address[index],
                    ),
                  );
                },
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.add,
                    color: AppColor.primaryColor,
                    size: 16,
                  ),
                  InkWell(
                    onTap: () {
                      Get.to(()=>AddNewAddressViewController(addressModel:  AllAddressModel(
                        building: "",
                          latitude: "",
                          longitude: "",
                          note: "",
                          id: 0,
                          customerid: "",
                          street: "",
                          address: "",
                          area: "",
                          floorunit: "",
                          country: "",
                          addresstype: ""),));
                    },
                    child: InkWell(
                        child: AppText.customText(
                      title: "Add New Address",
                      color: AppColor.primaryColor,
                      fontWeight: FontWeights.semi_bold,
                      size: 16,

                    )),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
