import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/ViewModel/address_view_model.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/GoogleMapModule/PlacesServices/geolocatorServices.dart';
import 'package:arwa/AppModules/GoogleMapModule/PlacesServices/googlePlacesApiService.dart';
import 'package:arwa/AppModules/GoogleMapModule/google_map_Controller.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AddNewAddressViewController extends StatefulWidget {
  final AllAddressModel addressModel;

  AddNewAddressViewController({
    Key? key,
    required this.addressModel,
  }) : super(key: key);

  @override
  _AddNewAddressViewControllerState createState() =>
      _AddNewAddressViewControllerState();
}

class _AddNewAddressViewControllerState
    extends State<AddNewAddressViewController> {
  TextEditingController buildingController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController areaController = TextEditingController();
  TextEditingController floorController = TextEditingController();
  TextEditingController notes = TextEditingController();
  TextEditingController address = TextEditingController();
  final addressVM = Get.put(AddressViewModel());
  var placeSearchController = Get.put(GoogleApiPlace());
  LatLng locationData = LatLng(0.0, 0.0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    onEditInitialize();
  }

  onEditInitialize() {
    if (widget.addressModel.id != 0) {
      setState(() {
        buildingController.text = widget.addressModel.building;
        addressVM.onUpdateBuilding(widget.addressModel.building);
        address.text = widget.addressModel.address;
        addressVM.onUpdateAddress(widget.addressModel.address);
        notes.text = widget.addressModel.note;
        addressVM.onUpdateNotes(widget.addressModel.note);
        floorController.text = widget.addressModel.floorunit;
        addressVM.onUpdateFloor(widget.addressModel.floorunit);
        areaController.text = widget.addressModel.area;
        addressVM.onUpdateArea(widget.addressModel.area);
        streetController.text = widget.addressModel.street;
        addressVM.onUpdateStreet(widget.addressModel.street);
        addressVM.addressType.value = widget.addressModel.addresstype;
        addressVM.onUpdateAddressType(widget.addressModel.address);

        if (widget.addressModel.latitude != "")
          locationData = LatLng(double.parse(widget.addressModel.latitude),
              double.parse(widget.addressModel.longitude));
        addressVM.onUpdateLocation(widget.addressModel.latitude,widget.addressModel.longitude);

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 322);
    return Scaffold(
        // backgroundColor: AppColor.primaryColor,
        backgroundColor: Color(0xFFFFFFFF),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: SafeArea(
            child: Container(
              color: Color(0xFFFFFFFF),
              width: AppConfig(context).width,
              height: AppConfig(context).height,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 322,
                      child: Stack(
                        children: [
                          Container(
                            height: 322,
                            child: GoogleMapViewController(
                                isFloatingBTN: false,
                                latLng: locationData,
                                isSecondary: false,
                                onGetCurrentLocation: (location) {}),
                          ),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      topLeft: Radius.circular(30)),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        // borderRadius: BorderRadius.only(
                        //     topRight: Radius.circular(30),
                        //     topLeft: Radius.circular(30)),
                      ),
                      width: AppConfig(context).width,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  child: AppText.customText(
                                    title: "Cancel",
                                    color: AppColor.primaryColor,
                                    fontWeight: FontWeights.medium,
                                    size: 12,
                                  ),
                                  onTap: () {
                                    Get.back();
                                  },
                                ),
                                Spacer(),
                                InkWell(
                                    child: AppText.customText(
                                  title: "New Address",
                                  color: AppColor.DarkText,
                                  fontWeight: FontWeights.semi_bold,
                                  size: 16,
                                )),
                                Spacer(),
                                InkWell(
                                    onTap: () {
                                      addressVM.onSaveAddress(
                                          context: context,
                                          id: widget.addressModel.id,
                                          onSave: (val) {
                                            if (val) {
                                              Get.back();
                                            }
                                          });
                                    },
                                    child: AppText.customText(
                                      title: "Apply",
                                      color: AppColor.primaryColor,
                                      fontWeight: FontWeights.medium,
                                      size: 12,
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey.shade300,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              height: 74,
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Column(
                                      children: [
                                        Obx(
                                          () => Image(
                                            image: AssetImage(
                                                "assets/paymentAndAddress/HOME.png"),
                                            height: 25,
                                            width: 25,
                                            color:
                                                addressVM.selectedType.value ==
                                                        0
                                                    ? AppColor.primaryColor
                                                    : Colors.grey,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        AppText.customText(
                                          title: "Home",
                                          color: AppColor.DarkText,
                                          fontWeight: FontWeights.medium,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                    onTap: () {
                                      addressVM.selectedType.value = 0;
                                    },
                                  ),
                                  Spacer(),
                                  VerticalDivider(
                                    indent: 10,
                                    endIndent: 10,
                                    width: 1,
                                    color: Colors.grey.shade300,
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: () {
                                      addressVM.selectedType.value = 1;
                                    },
                                    child: Column(
                                      children: [
                                        Obx(
                                          () => Image(
                                            image: AssetImage(
                                                "assets/paymentAndAddress/briefcase.png"),
                                            height: 25,
                                            width: 25,
                                            color:
                                                addressVM.selectedType.value ==
                                                        1
                                                    ? AppColor.primaryColor
                                                    : Colors.grey,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        AppText.customText(
                                          title: "Office",
                                          color: AppColor.DarkText,
                                          fontWeight: FontWeights.medium,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  VerticalDivider(
                                    indent: 10,
                                    endIndent: 10,
                                    width: 1,
                                    color: Colors.grey.shade300,
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: () {
                                      addressVM.selectedType.value = 2;
                                    },
                                    child: Column(
                                      children: [
                                        Obx(
                                          () => Image(
                                            image: AssetImage(
                                                "assets/paymentAndAddress/marker.png"),
                                            height: 25,
                                            width: 25,
                                            color:
                                                addressVM.selectedType.value ==
                                                        2
                                                    ? AppColor.primaryColor
                                                    : Colors.grey,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        AppText.customText(
                                          title: "Other",
                                          color: AppColor.DarkText,
                                          fontWeight: FontWeights.medium,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            InputField(
                              controller: buildingController,
                              width: double.infinity,
                              hint: "Building",
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              isSufixIcon: true,
                              onChange: (value) {
                                addressVM.onUpdateBuilding(value);
                              },
                              sufixIcon:
                                  "assets/paymentAndAddress/myLocation.png",
                              textFieldType: TextFieldType.SufixOnly,
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InputField(
                              controller: streetController,
                              hint: "Street",
                              width: double.infinity,
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              onChange: (value) {
                                addressVM.onUpdateStreet(value);
                              },
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InputField(
                              controller: areaController,
                              hint: "Area",
                              width: double.infinity,
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              onChange: (value) {
                                addressVM.onUpdateArea(value);
                              },
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InputField(
                              controller: floorController,
                              width: double.infinity,

                              hint: "Floor / Unit (Optional)",
                              // hint: ,
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              onChange: (value) {
                                addressVM.onUpdateFloor(value);
                              },
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InputField(
                              controller: notes,
                              width: double.infinity,
                              hint: "Note to Rider (Optional)",
                              // hint: "Note to Rider (Optional)",
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              onChange: (value) {
                                addressVM.onUpdateNotes(value);
                              },
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InputField(
                              controller: address,
                              width: double.infinity,
                              hint: "Address",
                              // hint: "Note to Rider (Optional)",
                              prefixIcon: "assets/auth/email.png",
                              isPrefixIcon: false,
                              onChange: (value) {
                                // addressVM.onUpdateNotes(value);
                                placeSearchController.searchPlaces(value);
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Obx(() => placeSearchController
                                    .searchResults.isNotEmpty
                                ? Container(
                                    height: 300.0,
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(.6),
                                        backgroundBlendMode: BlendMode.darken),
                                    child: ListView.builder(
                                        itemCount: placeSearchController
                                            .searchResults.length,
                                        itemBuilder: (context, index) {
                                          return ListTile(
                                            title: Text(
                                              placeSearchController
                                                  .searchResults[index]
                                                  .description,
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            onTap: () async {
                                              addressVM.onUpdateAddress(
                                                  placeSearchController
                                                      .searchResults[index]
                                                      .description);
                                              setState(() {
                                                address.text =
                                                    placeSearchController
                                                        .searchResults[index]
                                                        .description;
                                                // seachLocationController
                                                //     .searchResults.clear();
                                              });

                                              var sLocation =
                                                  await PlacesService()
                                                      .getPlace(
                                                          placeSearchController
                                                              .searchResults[
                                                                  index]
                                                              .placeId);
                                              setState(() {
                                                locationData = LatLng(
                                                    sLocation
                                                        .geometry.location.lat,
                                                    sLocation
                                                        .geometry.location.lng);
                                              });
                                              addressVM.onUpdateLocation(
                                                  sLocation
                                                      .geometry.location.lat
                                                      .toString(),
                                                  sLocation
                                                      .geometry.location.lng
                                                      .toString());

                                              // print("sLocation");
                                              // print(sLocation
                                              //     .geometry.location.lat);
                                              // print(sLocation
                                              //     .geometry.location.lng);
                                            },
                                          );
                                        }),
                                  )
                                : SizedBox()),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
