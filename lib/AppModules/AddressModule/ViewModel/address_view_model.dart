import 'dart:convert';

import 'package:arwa/AppModules/AddressModule/Model/address_model.dart';
import 'package:arwa/AppModules/AddressModule/Services/address_services.dart';
import 'package:arwa/AppModules/OrderModule/ViewModel/order_view_model.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AddressViewModel extends GetxController {
  RxInt selectedType = 0.obs;
  var addressData = [].obs;
  RxString addressType = "Home".obs;
  RxString address = "".obs;
  RxString latitude = "".obs;
  RxString longitude = "".obs;
  // RxInt id = 0.obs;
  RxString addressBuilding = "".obs;
  RxString street = "".obs;
  RxString area = "".obs;
  RxString floorUnit = "".obs;
  RxString notes = "".obs;
  RxBool isLoading = false.obs;
  final orderVM = Get.put(OrderViewModel());
  onSaveAddress(
      {required BuildContext context, required Function onSave, required int id}) async {
    if (addressBuilding.value == "") {
      ShowMessage().showErrorMessage(context, "Building Address is Required");
    } else if (street.value == "") {
      ShowMessage().showErrorMessage(context, "Street No/Name is Required");
    } else if (area.value == "") {
      ShowMessage().showErrorMessage(context, "Area  is Required");
    } else if (address.value == "") {
      ShowMessage().showErrorMessage(context, "Address  is Required");
    } else {
      // isLoading.value = true;
      showLoadingIndicator(context: context);
      print("Address Call");
      bool addressResult = await addToAddressService(
        context: context,
        id: id,
        addresstype: selectedType.value == 0
            ? "Home"
            : selectedType.value == 1
                ? "Office"
                : "Other",
        area: area.value,
        building: addressBuilding.value,
        floorunit: floorUnit.value,
        street: street.value,
        country: "AE",
        latitude: latitude.value,
        longitude: longitude.value,
        address: address.value,
        note: notes.value,
      );

      hideOpenDialog(context: context);
      if (addressResult) {
        await getAddressData(context, (data) {});
        selectedType.value = 0;
        addressBuilding.value = "Building";
        street.value = "Street";
        area.value = "Area";
        floorUnit.value = "Floor / Unit (Optional)";
        notes.value = "Note to Rider (Optional)";
      }

      onSave(addressResult);
    }
  }

  Future<List<AllAddressModel>> getAddressData(
      BuildContext context, Function onGetData) async {
    List<AllAddressModel> addressModel = [];
    // productsModel =
    // await getProductsDataService(context: context, brandCode: brandCode);
    if ((await DBProvider.db.checkDataExistenceByLength("Address")) == 0) {
      addressModel = await getAddressDataService(context: context);
    } else {
      print("AddressModel is fetched in from local");
      //
      var localData = await DBProvider.db.getAddress();
      // String value=jsonEncode(localData[0]["home"]);
      print(jsonDecode(localData[0]["address"])["data"]);

      addressModel = AllAddressModel.jsonToAddressList(
          jsonDecode(localData[0]["address"])["data"]);

      getAddressDataService(context: context).then((value) => {
            print("AddressModel is fetched in background"),
            addressData.value = value,
          });
    }

    addressData.value = addressModel;
    if(addressData.isNotEmpty)
      orderVM.selectedAddressID.value =
          addressData[0].id;
    onGetData(addressModel);
    return addressModel;
  }

  onUpdateAddressType(String type) {
    addressType.value = type;
  }

  onUpdateBuilding(String building) {
    addressBuilding.value = building;
  }

  onUpdateStreet(String value) {
    street.value = value;
  }

  onUpdateArea(String value) {
    area.value = value;
  }

  onUpdateFloor(String value) {
    floorUnit.value = value;
  }

  onUpdateNotes(String value) {
    notes.value = value;
  }
  onUpdateAddress(String value) {
    address.value = value;
  }
  onUpdateLocation(String lat,String lang) {
    latitude.value = lat;
    longitude.value = lang;
  }
}
