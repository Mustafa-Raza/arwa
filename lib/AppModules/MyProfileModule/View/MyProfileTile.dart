import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyProfileTile extends StatelessWidget {
 final String title;
final String icon;
final VoidCallback callback;
final bool isLogOut;
 MyProfileTile({Key? key,required this.callback,required this.icon,required this.title,this.isLogOut=false}) : super(key: key,);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){callback();
      },
      child: Container(
        height: 50,
        width: AppConfig(context).width,
        child: Row(
         children: [
           SizedBox( width: 20,),
           Image(
             image: AssetImage(icon),
             width: 40,
             height: 40,
             fit: BoxFit.fill,
           ),
           SizedBox( width: 15,),
           AppText.customText(
               title: title,
               color: AppColor.DarkText,
               fontWeight: FontWeights.medium,
               size: 16),
           Spacer(),
           if(!isLogOut)
           Icon(Icons.arrow_forward_ios_rounded,color: AppColor.textGREY,size: 10,),
           SizedBox( width: 20,),
         ],
        ),
      ),
    );
  }
}
