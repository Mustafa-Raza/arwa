
import 'dart:io';

import 'package:arwa/AppModules/MyProfileModule/ViewModel/edit_profile_view_model.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/image_picker_custom.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/image/gf_image_overlay.dart';

class ProfilePicView extends StatefulWidget {
  const ProfilePicView({Key? key}) : super(key: key);

  @override
  _ProfilePicViewState createState() => _ProfilePicViewState();
}

class _ProfilePicViewState extends State<ProfilePicView> {
  String imageProfile="";

  final editProfileVM =Get.put(EditProfileViewModel());
  @override
  Widget build(BuildContext context) {
    return  Container(
      color: Colors.transparent,
      width: 105,
      height: 105,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: Colors.white, width: 5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.shade300,
                      blurRadius: 15,
                    )
                  ]
              ),
              child:Obx(()=>editProfileVM.profileNetwork.value!="" && editProfileVM.imageProfile.value==""?  GFImageOverlay(
                width: 105,
                height: 105,
                // border: Border.all(
                //     color: Color(0xFF7F7F7F75),
                //     width: 10),
                shape: BoxShape.circle,
                image:NetworkImage(editProfileVM.profileNetwork.value),

              ):editProfileVM.imageProfile.value==""?
              // GFImageOverlay(
              //   width: 105,
              //   height: 105,
              //   // border: Border.all(
              //   //     color: Color(0xFF7F7F7F75),
              //   //     width: 10),
              //   shape: BoxShape.circle,
              //   image:AssetImage(
              //       "assets/profile/Avatar.png"),
              //
              // )
              CircleAvatar(
                radius: 50,
                child: Center(
                  child: AppText.customText(
                      alignment:
                      TextAlign.center,
                      title: "Upload Image",
                      color: AppColor.white,
                      size: 13),
                ),
              )
                  :GFImageOverlay(
                width: 105,
                height: 105,
                // border: Border.all(
                //     color: Color(0xFF7F7F7F75),
                //     width: 10),
                shape: BoxShape.circle,
                image:
                FileImage(
                    File(editProfileVM.imageProfile.value)
                ),

              ),
            ),
          ),
          ),
          InkWell(
            onTap: (){

              CustomImagePicker().showPicker(context: context, onGetImage:(image){
print(image);
editProfileVM.updateProfileImage(image, context);
// setState(() {
//   editProfileVM.imageProfile.value=image;
// });

              });
            },
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Align(
                  alignment: Alignment.bottomRight,
                  child: Image(
                    image: AssetImage(
                        "assets/profile/Edit_Profile/camera.png"),
                    height: 30,
                    width: 30,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
