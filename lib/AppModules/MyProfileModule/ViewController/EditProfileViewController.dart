import 'dart:io';

import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/CitySelector.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/show_country_picker.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyProfileModule/View/ProfilePicView.dart';
import 'package:arwa/AppModules/MyProfileModule/ViewModel/edit_profile_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/AddCardField.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EditProfileViewController extends StatefulWidget {
  const EditProfileViewController({Key? key}) : super(key: key);

  @override
  _EditProfileViewControllerState createState() =>
      _EditProfileViewControllerState();
}

class _EditProfileViewControllerState extends State<EditProfileViewController> {
  final editProfileVM = Get.put(EditProfileViewModel());

  FocusNode firstNameFocusNode = FocusNode();
  FocusNode lastNameFocusNode = FocusNode();
  FocusNode genderFocusNode = FocusNode();
  FocusNode phoneNumberFocusNode = FocusNode();
  FocusNode newPasswordFocusNode = FocusNode();
  FocusNode companyFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  FocusNode userNameFocusNode = FocusNode();
  FocusNode emailFocusNode = FocusNode();
  FocusNode countryFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllCity(context);
    // editProfileVM. onGetUser(context);
  }

  List<String> citesList = [];

  final categoryController = Get.put(HomeCategoryController());

  getAllCity(BuildContext context) async {
    citesList = await getAllCities(context: context);

    print(citesList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: SafeArea(
          child: Container(
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            color: Color(int.parse(
                "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30)),
                    ),
                    width: AppConfig(context).width,
                    height: AppConfig(context).height - 50,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: InnerAppBar(title: "Edit Profile"),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            // physics: NeverScrollableScrollPhysics(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30),
                              child: Column(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 30,
                                  ),
                                  ProfilePicView(),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Obx(
                                    () => AddCardInputField(


                                      controller: editProfileVM
                                          .firstNameController.value,
                                      width: double.infinity,
                                      focusNode: firstNameFocusNode,
                                      hint: "First Name",
                                      label: "First Name",
                                      isImgIcon: true,
                                      onChange: (value) {
                                        // editProfileVM.firstNameValue(value);
                                      },
                                      imgIcon:
                                          "assets/profile/Edit_Profile/user-circle.png",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Obx(
                                    () => AddCardInputField(
                                      focusNode: lastNameFocusNode,
                                      width: double.infinity,
                                      controller: editProfileVM
                                          .lastNameController.value,
                                      hint: "Last Name",
                                      label: "Last Name",
                                      isImgIcon: true,
                                      onChange: (value) {
                                        // editProfileVM.lastNameValue(value);
                                      },
                                      imgIcon:
                                          "assets/profile/Edit_Profile/user-circle.png",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Obx(
                                    () => AddCardInputField(
                                      readOnly: true,
                                      controller:
                                          editProfileVM.emailController.value,
                                      onTap: () {},
                                      focusNode: emailFocusNode,
                                      width: double.infinity,
                                      hint: "Email Address",
                                      label: "Email Address",
                                      isImgIcon: true,
                                      imgIcon:
                                          "assets/profile/Edit_Profile/email.png",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Obx(
                                    () => Row(
                                      children: [
                                        // SizedBox(
                                        //   width: 80,
                                        //   child: AddCardInputField(
                                        //     label: "+971",
                                        //     focusNode: countryFocusNode,
                                        //     controller: editProfileVM
                                        //         .countryCodeController.value,
                                        //     inputType: TextInputType.phone,
                                        //     onChange: (value) {},
                                        //     width: double.infinity,
                                        //     hint: "+971",
                                        //     readOnly: true,
                                        //     onTap: () {
                                        //       showCountries(
                                        //           context: context,
                                        //           onSelectCountry: (country) {
                                        //             setState(() {
                                        //               editProfileVM
                                        //                       .countryCodeController
                                        //                       .value
                                        //                       .text =
                                        //                   "+${country.phoneCode.toString()}";
                                        //             });
                                        //           });
                                        //     },
                                        //   ),
                                        // ),
                                        // SizedBox(
                                        //   width: 10,
                                        // ),
                                        Expanded(
                                          child: AddCardInputField(
                                            inputType: TextInputType.phone,
                                            controller: editProfileVM
                                                .phoneController.value,
                                            onChange: (value) {
                                              if (editProfileVM
                                                  .countryCodeController
                                                  .value
                                                  .text
                                                  .isNotEmpty) {
                                                editProfileVM.updatePhoneNumber(
                                                    editProfileVM
                                                            .countryCodeController
                                                            .value
                                                            .text +
                                                        value);
                                              } else {
                                                editProfileVM.updatePhoneNumber(
                                                    "+971" + value);
                                              }
                                            },
                                            width: double.infinity,
                                            focusNode: phoneNumberFocusNode,
                                            hint: "Phone Number",
                                            label: "Phone Number",
                                            isImgIcon: true,
                                            imgIcon:
                                                "assets/auth/telephone.png",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Obx(
                                    () => AddCardInputField(
                                      controller:
                                          editProfileVM.cityController.value,
                                      width: double.infinity,
                                      hint: "City",
                                      label: "City",
                                      isImgIcon: true,
                                      focusNode: cityFocusNode,
                                      readOnly: true,
                                      onTap: () {
                                        citySelector(
                                            context: context,
                                            listOfCity: citesList,
                                            onCitySelector: (value) {
                                              editProfileVM.updateCity(value);
                                            });
                                      },
                                      imgIcon:
                                          "assets/profile/Edit_Profile/global.png",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Obx(
                                    () => editProfileVM.isLoading.value
                                        ? spinKitButton(
                                            context,
                                            46,
                                            AppConfig(context).width - 60,
                                            Color(int.parse(
                                                "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")))
                                        : primaryButton(
                                            buttonTitle: "Update Profile",
                                            callback: () {
                                              editProfileVM.onUpdateUser(
                                                  onSubmit: (value) {
                                                    if(value){
                                                      Get.back();
                                                    }
                                                  },
                                                  context: context);
                                              // getUserById(context: context);
                                              // Get.back();
                                              // Navigator.of(context).pop();
                                            },
                                            width:
                                                AppConfig(context).width - 60,
                                            btnColor: Color(int.parse(
                                                "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                                            txtColor: AppColor.white,
                                            height: 46,
                                            fontweight: FontWeights.medium,
                                          ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
