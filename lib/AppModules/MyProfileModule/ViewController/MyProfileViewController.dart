import 'dart:ffi';
import 'dart:io';
import 'package:arwa/AppModules/AddressModule/ViewController/MyAddressViewController.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/HistoryModule/ViewController/OrderHistoryViewController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/ViewController/MyFavouriteViewController.dart';
import 'package:arwa/AppModules/MyProfileModule/View/MyProfileTile.dart';
import 'package:arwa/AppModules/MyProfileModule/ViewController/EditProfileViewController.dart';
import 'package:arwa/AppModules/MyProfileModule/ViewModel/edit_profile_view_model.dart';
import 'package:arwa/AppModules/PaymentModule/ViewController/MyCardViewController.dart';
import 'package:arwa/AppModules/SettingModule/ViewController/SettingViewController.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppBar/SecondaryAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/alert_box_custom.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/image/gf_image_overlay.dart';

class MyProfileViewController extends StatefulWidget {
  const MyProfileViewController({Key? key}) : super(key: key);

  @override
  _MyProfileViewControllerState createState() =>
      _MyProfileViewControllerState();
}

class _MyProfileViewControllerState extends State<MyProfileViewController> {
  final categoryController = Get.put(HomeCategoryController());

  final editProfileVM = Get.put(EditProfileViewModel());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    editProfileVM.onGetUser(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Container(
          width: AppConfig(context).width,
          height: AppConfig(context).height,
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              // height: AppConfig(context).height - 110,
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: SecondaryAppBar(
                      title: "My Profile",
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          SizedBox(
                            height: 100,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 20,
                                ),
                                Obx(() => editProfileVM.profileNetwork.value !=
                                        ""
                                    ?
                                    // Container(
                                    //   decoration: BoxDecoration(
                                    //     image: DecorationImage(image: CachedNetworkImageProvider(""))
                                    //   ),
                                    // ),
                                    // CircleAvatar(
                                    //
                                    //   radius: 45,
                                    //   child: cacheImageView(image: editProfileVM.profileNetwork.value,boxfit: BoxFit.fill,),
                                    // )
                                    GFImageOverlay(
                                        width: 90,
                                        height: 90,
                                        // border: Border.all(
                                        //     color: Color(0xFF7F7F7F75),
                                        //     width: 10),
                                        shape: BoxShape.circle,
                                        image: CachedNetworkImageProvider(
                                            editProfileVM.profileNetwork.value),
                                      )
                                    : CircleAvatar(
                                        radius: 50,
                                        child: Center(
                                          child: AppText.customText(
                                              alignment: TextAlign.center,
                                              title: "No Image",
                                              color: AppColor.white,
                                              size: 13),
                                        ),
                                      )),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Obx(
                                      () => AppText.customText(
                                          title:
                                              "${editProfileVM.firstNameValue.value}",
                                          color: AppColor.DarkText,
                                          fontWeight: FontWeights.semi_bold,
                                          size: 24),
                                    ),
                                    Obx(
                                      () => AppText.customText(
                                          title:
                                              "${editProfileVM.emailValue.value}",
                                          color: AppColor.textGREY,
                                          fontWeight: FontWeights.regular,
                                          size: 12),
                                    ),
                                  ],
                                ))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => EditProfileViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_Edit-Profile.png",
                              title: "Edit Profile"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => OrderHistoryViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_History-1.png",
                              title: "Order History"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => MyAddressListView(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_Location.png",
                              title: "My Addresses"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => MyFavouriteViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_Wishlist.png",
                              title: "My Favourite"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => MyCardsViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_History.png",
                              title: "My Cards"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              callback: () {
                                Get.to(() => SettingViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              icon: "assets/profile/Icon_Order.png",
                              title: "Settings"),
                          SizedBox(
                            height: 20,
                          ),
                          MyProfileTile(
                              isLogOut: true,
                              callback: () {
                                onLogoutPop(context);
                                // Get.offAll(()=>LoginViewController(),);
                              },
                              icon: "assets/profile/Icon_Exit.png",
                              title: "Log Out"),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
