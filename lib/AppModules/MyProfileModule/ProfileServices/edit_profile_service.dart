import 'dart:convert';

import 'package:arwa/AppModules/AuthenticationModule/Model/user_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<User> getUserById({
  required BuildContext context,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  print(user["id"]);
  print(user["customerid"]);
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_USER_BY_ID + "${user["customerid"]}", context: context);

  if (response == null) {
    return User(id: 0, customerid: "", firstname: "", lastname: "", username: "", email: "", password: "", gender: "", contact: "", companyname: "", city: "", country: "", creationdate: "", loginstatus: 0, passwordexpirydays: 0, passwordchangedon: "", profileimage: "");
  } else if (jsonDecode(response.body)["status"] == 200) {
    // if ((await DBProvider.db.checkDataExistenceByLength("User")) == 0) {
    //   print("User Inserted to SQL");
    //   await DBProvider.db
    //       .createUser(jsonDecode(response.body)["data"]["customer"].toString());
    // } else {
    //   print("User Updated to SQL");
    //   await DBProvider.db
    //       .updateUser(jsonDecode(response.body)["data"]["customer"].toString());
    // }
    //
    // print("SQLITE");
    // var localData=await DBProvider.db.getUser();
    //
    // print(localData["user"]);
    // print(User.fromJson(jsonDecode(response.body)["data"]));

    return User.fromJson(jsonDecode(response.body)["data"]);
  } else {
    return User(id: 0, customerid: "", firstname: "", lastname: "", username: "", email: "", password: "", gender: "", contact: "", companyname: "", city: "", country: "", creationdate: "", loginstatus: 0, passwordexpirydays: 0, passwordchangedon: "", profileimage: "");
  }
}

Future<bool> updateUserService({
  required String username,
  String password = "",
  required BuildContext context,
  required String email,
  String profileImage = "",
  String lastname = "",
  String firstname = "",
  String companyName = "",
  String gender = "",
  String city = "",
  String contact = "",
  String country = "",
  String dob = "",
}) async {
  var data = {
    "city": city,
    "companyname": companyName,
    "contact": contact,
    "country": country,
    "dob": dob,
    "email": email,
    "firstname": firstname,
    "gender": gender,
    "lastname": lastname,
    // "password": password,
    "username": email,
    "profileimage": profileImage,
  };
  print(data);
  var response = await ApiCall().createAccount(
      authorization: true, apiUrl: UPDATE_USER, data: data, context: context);
  // print(jsonDecode(response));
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {

    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);
    return true;
  } else {
    print(jsonDecode(response.body));
    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);

    return false;
  }
}
