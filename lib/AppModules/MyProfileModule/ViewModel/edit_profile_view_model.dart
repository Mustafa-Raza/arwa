import 'dart:convert';

import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/Model/user_model.dart';
import 'package:arwa/AppModules/MyProfileModule/ProfileServices/edit_profile_service.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class EditProfileViewModel extends GetxController {

  var firstNameController = TextEditingController().obs;
  var lastNameController = TextEditingController().obs;
  var emailController = TextEditingController().obs;
  var cityController = TextEditingController().obs;
  var phoneController = TextEditingController().obs;
  var countryCodeController = TextEditingController().obs;

  RxString firstNameValue = "".obs;
  RxString emailValue = "".obs;
  RxString profileImageValue = "".obs;
  RxString profileNetwork = "".obs;
  RxString imageProfile = "".obs;
  RxBool userNameExist = false.obs;
  RxBool emailExist = false.obs;
  RxBool isLoading = false.obs;

  onUpdateUser({
    required BuildContext context,
    required Function onSubmit,
  }) async {
    isLoading.value = true;
    bool userSaved = await updateUserService(
      context: context,
      username: emailController.value.text,
      password: "",
      email: emailController.value.text,
      lastname: lastNameController.value.text,
      firstname: firstNameController.value.text,
      companyName: "",
      gender: "",
      contact:phoneController.value.text,
      profileImage: profileImageValue.value,
      city: cityController.value.text,
    );
    isLoading.value = false;
    onGetUser(context);
    onSubmit(userSaved);
    // }
  }

  onGetUser(BuildContext context) async {
    User userData = User(
        id: 0,
        customerid: "",
        firstname: "",
        lastname: "",
        username: "",
        email: "",
        password: "",
        gender: "",
        contact: "",
        companyname: "",
        city: "",
        country: "",
        creationdate: "",
        loginstatus: 0,
        passwordexpirydays: 0,
        passwordchangedon: "",
        profileimage: "");

    // if ((await DBProvider.db.checkDataExistenceByLength("User")) == 0) {
    //   userData = await getUserById(context: context);
    // } else {
    //   print("User is fetched in from local");
    //   //
    //   var localData = await DBProvider.db.getUser();
    //   // String value=jsonEncode(localData[0]["home"]);
    //   print(localData["user"]["id"]);
    //
    //   userData = User.fromJson(jsonDecode(localData["user"].toString()));
    //
    //   getUserById(context: context).then((value) => {
    //         userData = value,
    //       });
    // }
    userData = await getUserById(context: context);
    if (userData.customerid != "") {
      firstNameController.value.text = userData.firstname;
      lastNameController.value.text = userData.lastname;
      profileNetwork.value = userData.profileimage;
      emailValue.value = userData.email;
      firstNameValue.value = userData.firstname;
      emailController.value.text = userData.email;
      // genderValue.value = userData.gender;
      // companyNameValue.value = userData.companyname;
      cityController.value.text = userData.city;
      phoneController.value.text = userData.contact;
    }
  }
  //
  // checkUserNameExistence(
  //   BuildContext context,
  // ) async {
  //   if (userNameValue.value != "") {
  //     bool isExist =
  //         await checkExistence(context: context, username: userNameValue.value);
  //     userNameExist.value = isExist;
  //     if (isExist) {
  //       ShowMessage().showErrorMessage(context, "User Name is Already Exist");
  //     }
  //   }
  // }

  checkEmailExistence(BuildContext context) async {
    if (emailController.value.value.text.isNotEmpty ) {
      bool isExist =
          await checkExistence(context: context, email: emailController.value.value.text);
      emailExist.value = isExist;
      if (isExist) {
        ShowMessage().showErrorMessage(context, "Email is Already Exist");
      }
    }
  }

  updateProfileImage(String image, BuildContext context) async {
    imageProfile.value = image;
    var imageResponse =
        await profileImageService(imagePath: image, context: context);
    profileImageValue.value = imageResponse;
  }

  updateFirstName(String firstname) {
    firstNameValue.value = firstname;
  }

  updateLastName(String lastname) {
    // lastNameValue.value = lastname;
  }

  updateUserName(String username) {
    // userNameValue.value = username;
  }

  updateEmail(String email) {
    emailValue.value = email;
  }

  updateCompanyName(String company) {
    // companyNameValue.value = company;
  }

  updatePassword(String password) {
    // passwordValue.value = password;
  }

  updateGender(String gender) {
    // genderValue.value = gender;
  }

  updatePhoneNumber(String phone) {
    // phoneNumberValue.value = phone;
  }

  updateCity(String city) {
    // cityValue.value = city;
    cityController.value.text = city;
  }
}
