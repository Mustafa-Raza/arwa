import 'dart:convert';

import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<bool> updatePasswordService({
  required String oldPassword,
  required BuildContext context,
  required String newPassword,


}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var data = {
    "customerid": user["customerid"],
    "newpassword": newPassword,
    "oldpassword": oldPassword
  };
  var response = await ApiCall().postRequestHeader(apiUrl: CHANGE_PASSWORD, bodyParameter: data, context: context);
  print(jsonDecode(response.body));
  if(response==null){
    return false;
  }else
  if (jsonDecode(response.body)["status"] == 200) {



    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);
    return true;
  } else {
    print(jsonDecode(response.body));
    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);

    return false;
  }
}