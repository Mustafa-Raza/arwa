import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/Model/user_model.dart';
import 'package:arwa/AppModules/ChangePasswordModule/Services/change_password_service.dart';
import 'package:arwa/AppModules/MyProfileModule/ProfileServices/edit_profile_service.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class ChangePasswordViewModel extends GetxController {
  // @override
  // void onInit() async {
  //
  //   super.onInit();
  // }

  RxString oldPasswordValue = "".obs;
  RxString newPasswordValue = "".obs;
  RxString new1PasswordValue = "".obs;

  RxBool isLoading = false.obs;

  onUpdatePassword({
    required BuildContext context,
    required Function onSubmit,
  }) async {
    if (oldPasswordValue.value == "") {
      ShowMessage().showErrorMessage(context, "Old Password is Required");
    } else if (newPasswordValue.value == "") {
      ShowMessage().showErrorMessage(context, "New Password is Required");
    } else {
      isLoading.value = true;

      bool dataValue = await updatePasswordService(
          oldPassword: oldPasswordValue.value,
          context: context,
          newPassword: newPasswordValue.value);

      isLoading.value = false;

      onSubmit(dataValue);
    }
  }

  updateOldPasswordValue(String password) {
    oldPasswordValue.value = password;
  }

  updateNewPasswordValue(String password) {
    newPasswordValue.value = password;
  }

  updateNew1PasswordValue(String password) {
    new1PasswordValue.value = password;
  }
}
