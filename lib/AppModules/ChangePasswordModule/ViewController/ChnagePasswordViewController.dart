import 'package:arwa/AppModules/ChangePasswordModule/ViewModel/change_password_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/PaymentModule/View/Component/AddCardField.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class ChangePasswordViewController extends StatefulWidget {
  const ChangePasswordViewController({Key? key}) : super(key: key);

  @override
  _ChangePasswordViewControllerState createState() =>
      _ChangePasswordViewControllerState();
}

class _ChangePasswordViewControllerState
    extends State<ChangePasswordViewController> {
  final changePasswordVM = Get.put(ChangePasswordViewModel());
  FocusNode oldPasswordFocusNode = FocusNode();
  FocusNode newPasswordFocusNode = FocusNode();

  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();

  final categoryController = Get.put(HomeCategoryController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: AppConfig(context).width,
      height: AppConfig(context).height,
      color: Color(int.parse(
          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
      child: Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          ),
          width: AppConfig(context).width,
          height: AppConfig(context).height - 50,
          child: Column(
            children: [
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: InnerAppBar(title: "Change Password"),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 29,
                          ),
                          Obx(
                            () => AddCardInputField(
                              controller: oldPasswordController,
                              focusNode: oldPasswordFocusNode,
                              width: double.infinity,
                              hint: changePasswordVM.oldPasswordValue.value,
                              label: "Old Password",

                              isImgIcon: true,
                              // isIcon: true,
                              onChange: (value) {
                                changePasswordVM.updateOldPasswordValue(value);
                              },
                              imgIcon: "assets/auth/lock.png",
                            ),
                          ),
                          const SizedBox(
                            height: 20.5,
                          ),
                          Obx(
                            () => AddCardInputField(
                              controller: newPasswordController,
                              width: double.infinity,
                              focusNode: newPasswordFocusNode,
                              hint: changePasswordVM.newPasswordValue.value,
                              label: "New Password",
                              isImgIcon: true,
                              onChange: (value) {
                                changePasswordVM.updateNewPasswordValue(value);
                              },
                              imgIcon: "assets/auth/lock.png",
                            ),
                          ),
                          // const SizedBox(
                          //   height: 20.5,
                          // ),
                          // Obx(
                          //   () => AddCardInputField(
                          //     width: double.infinity,
                          //     hint: changePasswordVM.new1PasswordValue.value,
                          //     label: "Re-Enter Password",
                          //     isImgIcon: true,
                          //     imgIcon: "assets/auth/lock.png",
                          //   ),
                          // ),
                          const SizedBox(
                            height: 50,
                          ),
                          Obx(
                            () => changePasswordVM.isLoading.value
                                ? spinKitButton(
                                    context,
                                    46,
                                    AppConfig(context).width - 60,
                                Color(int.parse(
                                    "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}"))
                                  )
                                : primaryButton(
                                    buttonTitle: "Update Password",
                                    callback: () {
                                      changePasswordVM.onUpdatePassword(
                                          onSubmit: (value) {},
                                          context: context);
                                      // getUserById(context: context);
                                      // Get.back();
                                      // Navigator.of(context).pop();
                                    },
                                    width: AppConfig(context).width - 60,
                                    btnColor: Color(int.parse(
                                        "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                                    txtColor: AppColor.white,
                                    height: 46,
                                    fontweight: FontWeights.medium,
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
