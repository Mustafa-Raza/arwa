import 'dart:convert';

import 'package:arwa/AppModules/CategoriesModule/Model/BrandsModel.dart';
import 'package:arwa/AppModules/CategoriesModule/Services/category_services.dart';
import 'package:arwa/AppModules/HomeModule/Model/home_model.dart';
import 'package:arwa/AppModules/HomeModule/Services/home_service.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/Services/offers_services.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class OfferViewModel extends GetxController {
  var offersData = [].obs;


  Future<List<OfferModel>> getOfferData(BuildContext context) async {
    List<OfferModel> offerModel = [];
    if ((await DBProvider.db.checkDataExistenceByLength("Offer")) == 0) {
      offerModel = await getOfferDataService(context: context);
    } else {
      print("Offers is fetched in from local");
      //
      var localData = await DBProvider.db.getOffers();

      print(jsonDecode(localData[0]["offers"])["data"]);

      offerModel = OfferModel.jsonToOfferList(
          jsonDecode(localData[0]["offers"])["data"]);

      getOfferDataService(context: context).then((value) => {
        print("Offers is fetched in background"),
        offersData.value = value,
        // homeModel = value,
      });
    }

    offersData.value = offerModel;

    return offerModel;
  }
}
