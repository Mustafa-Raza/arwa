class OfferModel{
  int id;
  String image;
  String dealNo;
  String dealText;
  OfferModel({
    required this.image,required this.id,required this.dealNo,required this.dealText
  });
  factory OfferModel.fromJson(Map<String, dynamic> json) =>
      OfferModel(
        id: json["id"],
        image: json["image"],
        dealNo: json["dealno"],
        dealText: json["dealtext"],
      );
  static List<OfferModel> jsonToOfferList(List<dynamic> emote) =>
      emote.map<OfferModel>((item) => OfferModel.fromJson(item)).toList();
}