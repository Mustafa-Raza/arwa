import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:arwa/AppModules/OfferAndDealsModule/ViewModel/offer_view_model.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppBar/SecondaryAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';

class OfferAndDealsViewController extends StatefulWidget {
final bool inner;
  OfferAndDealsViewController({Key? key,required this.inner}) : super(key: key);

  @override
  _OfferAndDealsViewControllerState createState() =>
      _OfferAndDealsViewControllerState();
}

class _OfferAndDealsViewControllerState
    extends State<OfferAndDealsViewController> {
  final categoryController = Get.put(HomeCategoryController());

  final offerVM=Get.put(OfferViewModel());
  // List<String> deals = [
  //   "assets/deals/Offer1.png",
  //   "assets/deals/Offer1-1.png",
  //   "assets/deals/Offer1-2.png",
  //   "assets/deals/Offer1-3.png",
  //   "assets/deals/Offer1-4.png",
  // ];
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    offerVM.getOfferData(context);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            height: AppConfig(context).height,
            width: AppConfig(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.inner?  InnerAppBar(
                      title: "Deals & Discounts",
                    )
                   : SecondaryAppBar(
                      title: "Deals & Discounts",
                    ),
                    Expanded(
                      child:FutureBuilder<List<OfferModel>>(
                          future: offerVM.getOfferData(context),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: FavShimmerView(),
                              );
                            }
                          return ListView.builder(
                            itemCount: offerVM.offersData.length,
                            // physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,

                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 20),
                                child: Container(
                                  height: 150,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(13),
                                    child: cacheImageView(image: offerVM.offersData[index].image)
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
