import 'dart:convert';

import 'package:arwa/AppModules/OfferAndDealsModule/Model/offers_model.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';

Future<List<OfferModel>> getOfferDataService({
  required BuildContext context,
}) async {
  // var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_ALL_OFFERS , context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get Offer Data");

    ///TODO:Save offer data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("Offer")) == 0) {
      print("Offer Inserted to SQL");
      await DBProvider.db
          .createOffers(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getOffers());
    } else {
      print("offer Updated to SQL");
      await DBProvider.db
          .updateOffers(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getOffers());
    }


    return OfferModel.jsonToOfferList(jsonDecode(response.body)["data"]);
  } else {
    return [];
  }
}
