import 'package:arwa/AppModules/GoogleMapModule/getLocationService.dart';
import 'package:location/location.dart';

class LoginLocationServices {
  bool? _serviceEnabled;

  PermissionStatus? _permissionGranted;
  String? currentLocationName;
  LocationData? _locationData;
  Location location = new Location();

  Future<SocialLocation> initCurrentLocation() async {
    try {
      print("current location");

      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled!) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled!) {
          return SocialLocation(country: "", city: "");
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return SocialLocation(country: "", city: "");
        }
      }

      _locationData = await location.getLocation();
      print(_locationData);

     return  getLocation(_locationData!.latitude!, _locationData!.longitude!);

      // _locationData = await location.getLocation();
    } catch (e) {
      print(e);
    }
    return SocialLocation(country: "", city: "");
  }
}
