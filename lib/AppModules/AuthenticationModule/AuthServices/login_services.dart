import 'dart:convert';

import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Map<String, dynamic> toMap(String data) {
  return {
    'id': 0,
    'name': data,
  };
}

Future<bool> loginService({
  required String username,
  String password = "",
  required BuildContext context,
}) async {
  var data = {
    "password": password,
    "username": username,
  };
  var response = await ApiCall()
      .postRequest(apiUrl: LOGIN_USER, bodyParameter: data, context: context);
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {

    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);
    await DatabaseHandler()
        .setToken(jsonDecode(response.body)["data"]["token"]);
    await DatabaseHandler()
        .setCurrentUser(jsonDecode(response.body)["data"]["customer"]);
    if ((await DBProvider.db.checkDataExistenceByLength("User")) == 0) {
      print("User Inserted to SQL");
      await DBProvider.db
          .createUser(jsonDecode(response.body)["data"]["customer"].toString());
    } else {
      print("User Updated to SQL");
      await DBProvider.db
          .updateUser(jsonDecode(response.body)["data"]["customer"].toString());
    }

    print("SQLITE");
    print(await DBProvider.db.getUser());
    return true;
  } else {
    print(jsonDecode(response.body));
    ShowMessage()
        .showErrorMessage(context, jsonDecode(response.body)["message"]);

    return false;
  }
}
Future<bool> socialLoginService({

  required String  name,
  required String  city,
  required String  profile,
  required String  email,
  required String  UID,
  required String  loginType,
  required BuildContext context,
}) async {
  var data = {
    "auth": UID,
    "city": city,
    "country": "AE",
    "email": email,
    "firstname": name,
    "lastname": name,
    "profileimage": profile,
    "source": loginType,
    "username": email,
  };
  var response = await ApiCall()
      .postRequest(apiUrl: SOCIAL_LOGIN_USER, bodyParameter: data, context: context);
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {

    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);
    await DatabaseHandler()
        .setToken(jsonDecode(response.body)["data"]["token"]);
    await DatabaseHandler()
        .setCurrentUser(jsonDecode(response.body)["data"]["customer"]);
    if ((await DBProvider.db.checkDataExistenceByLength("User")) == 0) {
      print("User Inserted to SQL");
      await DBProvider.db
          .createUser(jsonDecode(response.body)["data"]["customer"].toString());
    } else {
      print("User Updated to SQL");
      await DBProvider.db
          .updateUser(jsonDecode(response.body)["data"]["customer"].toString());
    }

    print("SQLITE");
    print(await DBProvider.db.getUser());
    return true;
  } else {
    print(jsonDecode(response.body));
    ShowMessage()
        .showErrorMessage(context, jsonDecode(response.body)["message"]);

    return false;
  }
}
