import 'dart:convert';
import 'dart:io';

import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';

Future<bool> registerService({
  required String username,
  String password = "",
  required BuildContext context,
  required String email,
  String profileImage = "",
  String lastname = "",
  String firstname = "",
  String companyName = "",
  String gender = "",
  String city = "",
  String contact = "",
  String country = "",
  String dob = "",
}) async {
  var data = {
    "city": city,
    "companyname": "",
    "contact": contact,
    "country": "AE",
    "dob": "",
    "email": email,
    "firstname": firstname,
    "gender": "",
    "lastname": lastname,
    "password": password,
    "username": email,
    "profileimage": profileImage,
  };
  var response = await ApiCall()
      .createAccount(apiUrl: REGISTER_USER, data: data, context: context);
  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    // print(jsonDecode(response.body));
    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);
    await DatabaseHandler()
        .setToken(jsonDecode(response.body)["data"]["token"]);
    await DatabaseHandler()
        .setCurrentUser(jsonDecode(response.body)["data"]["customer"]);
    if ((await DBProvider.db.checkDataExistenceByLength("User")) == 0) {
      print("User Inserted to SQL");
      await DBProvider.db
          .createUser(jsonDecode(response.body)["data"]["customer"].toString());
    } else {
      print("User Updated to SQL");
      await DBProvider.db
          .updateUser(jsonDecode(response.body)["data"]["customer"].toString());
    }

    print("SQLITE");
    print(await DBProvider.db.getUser());
    return true;
  } else {
    print(jsonDecode(response.body));
    ShowMessage().showMessage(context, jsonDecode(response.body)["message"]);

    return false;
  }
}

Future<String> profileImageService({
  required String imagePath,
  required BuildContext context,
}) async {
  var response = await ApiCall()
      .uploadFile(url: UPLOAD_PROFILE, name: "", imgFile: File(imagePath));

  if (response.statusCode == 200) {
    print(jsonDecode(response.body));
    ShowMessage().showMessage(context, "Image Added");
    return jsonDecode(response.body)["data"]["image"];
  } else {
    ShowMessage().showMessage(context, "Profile Image is Not Uploaded");
    print(jsonDecode(response.body));
    return "";
  }
}

Future<bool> checkExistence({
  String email = "",
  String username = "",
  required BuildContext context,
}) async {
  String url;
  if (email != "") {
    url = CHECK_EMAIL + "?email=$email";
  } else {
    url = CHECK_EMAIL + "?username=$username";
  }
  var response = await ApiCall().getRequest(apiUrl: url, context: context);

  if (jsonDecode(response.body)["message"] == "username available" ||
      jsonDecode(response.body)["message"] == "email available") {
    return false;
  } else {
    return true;
  }
}

Future<List<String>> getAllCities({
  required BuildContext context,
}) async {
  var response =
      await ApiCall().getRequest(apiUrl: GET_ALL_CITES, context: context);

  if (response.statusCode == 200) {
    return List<String>.from(jsonDecode(response.body)["data"]);
  } else {
    return [];
  }
}

Future<bool> setFCMService({
  required BuildContext context,
  required String fcmToken,
}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    "customerid": "${user["customerid"]}",
    "fcmcode": fcmToken,
    "id": 0
  };

  var response = await ApiCall().postRequestHeader(
      apiUrl: SET_FCM_TOKEN, bodyParameter: data, context: context);

  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    // ShowMessage().showMessage(context, "Added to Cart");
    print("FCM is Set");
    return true;
  } else {
    return false;
  }
}

Future<bool> deleteFCMService({
  required BuildContext context,

}) async {
  var user = await DatabaseHandler().getCurrentUser();

  var data = {
    "customerid": "${user["customerid"]}",
    // "fcmcode": fcmToken,
    // "id": 0
  };

  var response = await ApiCall().postRequestHeader(
      apiUrl: DELETE_FCM_TOKEN, bodyParameter: data, context: context);

  if (response == null) {
    return false;
  } else if (jsonDecode(response.body)["status"] == 200) {
    // ShowMessage().showMessage(context, "Added to Cart");
    print("FCM is Deleted");
    return true;
  } else {
    return false;
  }
}
