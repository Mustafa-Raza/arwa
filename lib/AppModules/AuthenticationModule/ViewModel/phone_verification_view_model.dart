import 'package:arwa/AppModules/AuthenticationModule/AuthServices/login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class PhoneVerifyViewModel extends GetxController {
  RxString phoneNumber = "".obs;
  RxString city = "".obs;
  RxBool loading = false.obs;

  updateCity(String password) {
    city.value = password;
  }

  updatePhoneNumber(String password) {
    phoneNumber.value = password;
  }

  onSocialLogin({
    required BuildContext context,
    required Function onSubmit,
    required String name,
    required String profile,
    required String email,
    required String UID,
    required String loginType,
  }) async {
    if (phoneNumber.value == "") {
      ShowMessage().showErrorMessage(context, "Phone Number is Required");
    } else if (city.value == "") {
      ShowMessage().showErrorMessage(context, "City is Required");
    } else {
      loading.value = true;
      bool userSaved = await socialLoginService(
        context: context,
        email: email,
        city: city.value,
        loginType: loginType,
        name: name,
        profile: profile,
        UID: UID,
      );
      if (userSaved) {
        String? token = await FirebaseMessaging.instance.getToken();
        print('FCM TOKEN' + token.toString());
        await setFCMService(context: context, fcmToken: token.toString());
      }

      loading.value = false;

      onSubmit(userSaved);
    }
  }
}
