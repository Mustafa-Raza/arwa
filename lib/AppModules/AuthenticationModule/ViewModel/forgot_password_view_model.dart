import 'package:arwa/AppModules/AuthenticationModule/AuthServices/login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class ForgotPasswordViewModel extends GetxController {
  RxString phoneNumber = "".obs;
  RxString email = "".obs;


  updatePhoneNumber(String password) {
    phoneNumber.value = password;
  }

  updateEmail(String password) {
    email.value = password;
  }

}
