import 'package:arwa/AppModules/AuthenticationModule/AuthServices/login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class LoginViewModel extends GetxController {
  RxString userNameValue = "".obs;
  RxString passwordValue = "".obs;
  RxBool isLoading = false.obs;

  updatePassword(String password) {
    passwordValue.value = password;
  }

  updateUserName(String username) {
    userNameValue.value = username;
  }

  onSubmit({
    required BuildContext context,
    required Function onSubmit,
  }) async {
    if (userNameValue.value == "") {
      ShowMessage().showErrorMessage(context, "Email Address is Required");
    } else if (passwordValue.value == "") {
      ShowMessage().showErrorMessage(context, "Email is Required");
    } else {
      print(userNameValue);
      print(passwordValue);

      isLoading.value = true;
      print("Login Call");
      bool userSaved = await loginService(
        context: context,
        username: userNameValue.value,
        password: passwordValue.value,
      );
      print("Login Call back");
      if (userSaved) {
        String? token = await FirebaseMessaging.instance.getToken();
        print('FCM TOKEN' + token.toString());
        await setFCMService(context: context, fcmToken: token.toString());
      }

      isLoading.value = false;
      onSubmit(userSaved);
    }
  }


}
