import 'package:arwa/AppModules/AuthenticationModule/AuthServices/login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class NewPasswordViewModel extends GetxController {
  RxString password = "".obs;
  RxString confirmPassword = "".obs;



  updatePassword(String value) {
    password.value = value;
  }

  updateConfirmPassword(String value) {
    confirmPassword.value = value;
  }

}
