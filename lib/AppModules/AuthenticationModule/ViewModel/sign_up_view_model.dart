import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/Utills/show_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SignUpViewModel extends GetxController {
  // @override
  // void onInit() async {
  //
  //   super.onInit();
  // }
  RxString genderValue = "".obs;
  RxString firstNameValue = "".obs;
  RxString lastNameValue = "".obs;
  RxString userNameValue = "".obs;
  RxString emailValue = "".obs;
  RxString cityValue = "".obs;
  RxString companyNameValue = "".obs;
  RxString phoneNumberValue = "".obs;
  RxString passwordValue = "".obs;
  RxString profileImageValue = "".obs;
  RxString imageProfile = "".obs;
  RxBool userNameExist = false.obs;
  RxBool emailExist = false.obs;
  RxBool isLoading = false.obs;

  onSubmit({
    required BuildContext context,
    required Function onSubmit,
  }) async {
    print(emailValue);
    print(userNameValue);

  if (emailValue.value == "") {
      ShowMessage().showErrorMessage(context, "Email is Required");
    } else if (phoneNumberValue.value == "") {
      ShowMessage().showErrorMessage(context, "Phone Number is Required");
    } else if (passwordValue.value == "") {
      ShowMessage().showErrorMessage(context, "Password is Required");
    } else {
      print(emailValue);
      print(userNameValue);

      isLoading.value = true;
      bool userSaved = await registerService(
          context: context,
          username: userNameValue.value,
          password: passwordValue.value,
          email: emailValue.value,
          lastname: lastNameValue.value,
          firstname: firstNameValue.value,
          companyName: companyNameValue.value,
          gender: genderValue.value,
          profileImage: profileImageValue.value,
          contact: phoneNumberValue.value,
          city: cityValue.value);
      if (userSaved)
        await setFCMService(
            context: context,
            fcmToken:
                "eHFRvw69Q4usS6ijtV_WPw:APA91bE2_R6eAiJ2sMDzBGx-O6onIIoLr-pqU12goAxxoqXZD4FCDup6XMLI4XdAVofP-cQk6GiDasgTmVVSKqcslUU_DtxrL5GBG53RnRRppd7NRx5MBZT9NBJ09WfBDgl73jeXGpXS");
      isLoading.value = false;
      onSubmit(userSaved);
    }
  }

  checkUserNameExistence(
    BuildContext context,
  ) async {
    if (userNameValue.value != "") {
      bool isExist =
          await checkExistence(context: context, username: userNameValue.value);
      userNameExist.value = isExist;
      if (isExist) {
        ShowMessage().showErrorMessage(context, "User Name is Already Exist");
      }
    }
  }

  checkEmailExistence(BuildContext context) async {
    if (emailValue.value != "") {
      bool isExist =
          await checkExistence(context: context, email: emailValue.value);
      emailExist.value = isExist;
      if (isExist) {
        ShowMessage().showErrorMessage(context, "Email is Already Exist");
      }
    }
  }

  updateProfileImage(String image, BuildContext context) async {
    imageProfile.value = image;
    var imageResponse =
        await profileImageService(imagePath: image, context: context);
    profileImageValue.value = imageResponse;
  }

  updateFirstName(String firstname) {
    firstNameValue.value = firstname;
  }

  updateLastName(String lastname) {
    lastNameValue.value = lastname;
  }

  updateUserName(String username) {
    userNameValue.value = username;
  }

  updateEmail(String email) {
    emailValue.value = email;
  }

  updateCompanyName(String company) {
    companyNameValue.value = company;
  }

  updatePhoneNumber(String phone) {
    phoneNumberValue.value = phone;
  }

  updatePassword(String password) {
    passwordValue.value = password;
  }

  updateGender(String gender) {
    genderValue.value = gender;
  }

  updateCity(String city) {
    cityValue.value = city;
  }
}
