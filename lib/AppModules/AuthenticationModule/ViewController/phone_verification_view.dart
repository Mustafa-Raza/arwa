import 'dart:async';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/CitySelector.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/show_country_picker.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/otp_verification_view_controller.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/phone_verification_view_model.dart';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:pin_code_fields/pin_code_fields.dart';

class PhoneVerificationView extends StatefulWidget {
  final String name;
  final String email;
  final String profile;
  final String UID;
  final String authType;

  PhoneVerificationView({
    required this.email,
    required this.profile,
    required this.name,
    required this.authType,
    required this.UID,
    Key? key,
  }) : super(key: key);

  @override
  _PhoneVerificationViewState createState() => _PhoneVerificationViewState();
}

class _PhoneVerificationViewState extends State<PhoneVerificationView> {
  final phoneVerifyVM = Get.put(PhoneVerifyViewModel());
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllCity(context);
    // connectivityServices.initiateConnectivity(callBack: (isInternet) {
    //   print(isInternet.toString());
    // });
  }

  List<String> citesList = [];

  getAllCity(BuildContext context) async {
    citesList = await getAllCities(context: context);

    print(citesList);
  }

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 320);
    return Scaffold(
        body: Container(
      width: AppConfig(context).width,
      height: AppConfig(context).height,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage("assets/auth/login_back.png"),
              width: AppConfig(context).width,
              height: 322,
              fit: BoxFit.fill,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              height: AppConfig(context).height - 300,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 45),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText.customText(
                        title: "Enter Phone Number",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 24,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppText.customText(
                        title:
                            "Please enter your 09 digits phone number to receive verification code.",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 14,
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      AppText.customText(
                        title: "Enter your phone number",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 80,
                            child: InputField(
                              controller: countryController,
                              inputType: TextInputType.phone,
                              onChange: (value) {},
                              width: double.infinity,
                              hint: "+971",
                              prefixIcon: "assets/auth/phoneVerifiy.png",
                              textFieldType: TextFieldType.Simple,
                              isPrefixIcon: true,
                              readOnly: true,
                              onTap: () {
                                showCountries(
                                    context: context,
                                    onSelectCountry: (country) {
                                      setState(() {
                                        countryController.text =
                                            "+${country.phoneCode.toString()}";
                                      });
                                    });
                              },
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: InputField(
                              controller: phoneNumberController,
                              inputType: TextInputType.phone,
                              onChange: (value) {
                                phoneVerifyVM.updatePhoneNumber(value);
                              },
                              width: double.infinity,
                              hint: "Phone Number",
                              prefixIcon: "assets/auth/phoneVerifiy.png",
                              textFieldType: TextFieldType.Simple,
                              isPrefixIcon: true,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      InputField(
                        controller: cityController,
                        width: double.infinity,
                        hint: "City",
                        isPassword: false,
                        prefixIcon: "assets/auth/world.png",
                        isPrefixIcon: true,
                        // hintColor:  signUpVM.cityValue.value=="City"?AppColor.greyText:AppColor.black,
                        textFieldType: TextFieldType.PrefixWithSufix,
                        readOnly: true,
                        onTap: () {
                          citySelector(
                              listOfCity: citesList,
                              context: context,
                              onCitySelector: (city) {
                                phoneVerifyVM.updateCity(city);
                                setState(() {
                                  cityController.text = city;
                                });
                              });
                        },
                        sufixIcon: "assets/auth/caret-down.png",
                        isSufixIcon: true,
                      ),
                      SizedBox(
                        height: 34,
                      ),
                      AppText.customText(
                        title:
                            "Arwa will send a one time SMS message to verify your phone number. Carrier SMS charges may apply.",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.regular,
                        size: 12,
                      ),
                      SizedBox(
                        height: 54,
                      ),
                      Obx(
                        () => phoneVerifyVM.loading.value
                            ? spinKitButton(
                                context,
                                46,
                            AppConfig(context).width - 40,
                                AppColor.primaryColor)
                            : primaryButton(
                                buttonTitle: "Next",
                                callback: () {
                                  phoneVerifyVM.onSocialLogin(
                                      context: context,
                                      onSubmit: (value) {
                                        if (value) {
                                          Navigator.push(
                                            // Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    OTPVerificationViewController(phoneNumber: phoneNumberController.value.text,),
                                              ));
                                          // Navigator.pushReplacement(
                                          //     context,
                                          //     MaterialPageRoute(
                                          //       builder: (context) =>
                                          //           OnBoardingViewController(),
                                          //     ));
                                        }
                                      },
                                      name: widget.name,
                                      profile: widget.profile,
                                      email: widget.email,
                                      UID: widget.UID,
                                      loginType: widget.authType);

                                },
                                width: AppConfig(context).width - 40,
                                btnColor: AppColor.primaryColor,
                                txtColor: AppColor.white,
                                height: 46,
                                fontweight: FontWeights.medium,
                              ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
