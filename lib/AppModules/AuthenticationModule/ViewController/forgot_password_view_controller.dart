import 'dart:async';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/show_country_picker.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/new_password_view_controller.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/forgot_password_view_model.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/phone_verification_view_model.dart';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';



class ForgotPasswordViewController extends StatefulWidget {
  ForgotPasswordViewController({
    Key? key,
  }) : super(key: key);

  @override
  _ForgotPasswordViewControllerState createState() => _ForgotPasswordViewControllerState();
}

class _ForgotPasswordViewControllerState extends State<ForgotPasswordViewController> {
  final forgotPasswordVM = Get.put(ForgotPasswordViewModel());
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 320);
    return Scaffold(
        body: Container(
          width: AppConfig(context).width,
          height: AppConfig(context).height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Image(
                  image: AssetImage("assets/auth/login_back.png"),
                  width: AppConfig(context).width,
                  height: 322,
                  fit: BoxFit.fill,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                  ),
                  width: AppConfig(context).width,
                  height: AppConfig(context).height - 300,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20, top: 45),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.customText(
                            title: "Forgot Password",
                            color: AppColor.DarkText,
                            fontWeight: FontWeights.semi_bold,
                            size: 24,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          AppText.customText(
                            title:
                            "Please enter your email address to receive a link for new password",
                            color: AppColor.textGREY,
                            fontWeight: FontWeights.regular,
                            size: 14,
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          AppText.customText(
                            title: "Enter your Email Address",
                            color: AppColor.textGREY,
                            fontWeight: FontWeights.medium,
                            size: 14,
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          // Row(
                          //   children: [
                          //     SizedBox(
                          //       width: 80,
                          //       child: InputField(
                          //         controller: countryController,
                          //         inputType: TextInputType.phone,
                          //         onChange: (value) {},
                          //         width: double.infinity,
                          //         hint: "+971",
                          //         prefixIcon: "assets/auth/phoneVerifiy.png",
                          //         textFieldType: TextFieldType.Simple,
                          //         isPrefixIcon: true,
                          //         readOnly: true,
                          //         onTap: () {
                          //           showCountries(
                          //               context: context,
                          //               onSelectCountry: (country) {
                          //                 setState(() {
                          //                   countryController.text =
                          //                   "+${country.phoneCode.toString()}";
                          //                 });
                          //               });
                          //         },
                          //       ),
                          //     ),
                          //     SizedBox(
                          //       width: 10,
                          //     ),
                          //     Expanded(
                          //       child: InputField(
                          //         controller: phoneNumberController,
                          //         inputType: TextInputType.phone,
                          //         onChange: (value) {},
                          //         width: double.infinity,
                          //         hint: "Phone Number",
                          //         prefixIcon: "assets/auth/phoneVerifiy.png",
                          //         textFieldType: TextFieldType.Simple,
                          //         isPrefixIcon: true,
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          InputField(
                            controller: emailController,
                            inputType: TextInputType.emailAddress,

                            onChange: (value) {
                              forgotPasswordVM.updateEmail(value);
                            },
                            width: double.infinity,
                            hint: "Email Address",
                            prefixIcon: "assets/auth/email.png",
                            textFieldType: TextFieldType.PrefixOnly,
                            isPrefixIcon: true,
                          ),
                          SizedBox(
                            height: 34,
                          ),
                          SizedBox(
                            height: 43,
                          ),
                          primaryButton(
                            buttonTitle: "Submit",
                            callback: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        NewPasswordViewController(),
                                  ));
                            },
                            width: AppConfig(context).width - 40,
                            btnColor: AppColor.primaryColor,
                            txtColor: AppColor.white,
                            height: 46,
                            fontweight: FontWeights.medium,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
