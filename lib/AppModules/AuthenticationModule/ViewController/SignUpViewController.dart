import 'dart:io';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/sign_up_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/CitySelector.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/show_country_picker.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/otp_verification_view_controller.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/sign_up_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/image_picker_custom.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/image/gf_image_overlay.dart';

class SignUpViewController extends StatefulWidget {
  const SignUpViewController({Key? key}) : super(key: key);

  @override
  _SignUpViewControllerState createState() => _SignUpViewControllerState();
}

class _SignUpViewControllerState extends State<SignUpViewController> {
  final signUpVM = Get.put(SignUpViewModel());
  final categoryController = Get.put(HomeCategoryController());

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllCity(context);
  }

  List<String> citesList = [];

  getAllCity(BuildContext context) async {
    citesList = await getAllCities(context: context);

    print(citesList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColor.primaryColor,
        body: SafeArea(
          child: Container(
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            color: AppColor.primaryColor,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 35,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30)),
                    ),
                    // width: AppConfig(context).width,
                    height: AppConfig(context).height - 35,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: SingleChildScrollView(
                        child: Column(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            InkWell(
                              onTap: () {
                                CustomImagePicker().showPicker(
                                    context: context,
                                    onGetImage: (image) {
                                      print(image);
                                      signUpVM.updateProfileImage(
                                          image, context);
                                    });
                              },
                              child: Container(
                                color: Colors.transparent,
                                width: 110,
                                height: 110,
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Obx(
                                        () => Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: Color(0xFFE8E7E9),
                                                  width: 10),
                                              // boxShadow: [
                                              //   BoxShadow(
                                              //     color: Colors.grey.shade300,
                                              //     blurRadius: 15,
                                              //   )
                                              // ]
                                            ),
                                            child: signUpVM
                                                        .imageProfile.value !=
                                                    ""
                                                ? GFImageOverlay(
                                                    width: 110,
                                                    height: 110,
                                                    // border: Border.all(
                                                    //     color: Color(0xFF7F7F7F75),
                                                    //     width: 10),
                                                    shape: BoxShape.circle,
                                                    image: FileImage(File(
                                                        signUpVM.imageProfile
                                                            .value)))
                                                : CircleAvatar(
                                                    radius: 50,
                                                    child: Center(
                                                      child: AppText.customText(
                                                          alignment:
                                                              TextAlign.center,
                                                          title: "Upload Image",
                                                          color: AppColor.white,
                                                          size: 13),
                                                    ),
                                                  )
                                            // GFImageOverlay(
                                            //         width: 110,
                                            //         height: 110,
                                            //         // border: Border.all(
                                            //         //     color: Color(0xFF7F7F7F75),
                                            //         //     width: 10),
                                            //         shape: BoxShape.circle,
                                            //         image: AssetImage(
                                            //             "assets/profile/Avatar.png")),
                                            ),
                                      ),
                                    ),
                                    Align(
                                        alignment: Alignment.bottomRight,
                                        child: Image(
                                          image: AssetImage(
                                              "assets/auth/Change_image_button.png"),
                                          height: 40,
                                          width: 40,
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child:
                                      // Obx(
                                      //   () =>
                                      InputField(
                                        captilization: TextCapitalization.sentences,

                                    controller: firstnameController,
                                    inputType: TextInputType.name,
                                    hint: "First Name",
                                    prefixIcon: "assets/auth/user.png",
                                    isPrefixIcon: true,
                                    textFieldType: TextFieldType.PrefixOnly,
                                    onChange: (value) {
                                      signUpVM.updateFirstName(value);
                                    },
                                    // width: 147,
                                    // )
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child:
                                      // Obx(
                                      //   () =>
                                      InputField(
                                        captilization: TextCapitalization.sentences,

                                        controller: lastnameController,
                                    inputType: TextInputType.name,
                                    hint: "Last Name",
                                    prefixIcon: "assets/auth/user.png",
                                    isPrefixIcon: true,
                                    textFieldType: TextFieldType.PrefixOnly,
                                    onChange: (value) {
                                      signUpVM.updateLastName(value);
                                    },
                                    // width: 147,
                                    // ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // Obx(
                            //   () => Focus(
                            //     onFocusChange: (hasFocus) {
                            //       print("hasFocus");
                            //       print(hasFocus);
                            //       if (!hasFocus) {
                            //         signUpVM.checkUserNameExistence(context);
                            //       }
                            //     },
                            //     child: InputField(
                            //       width: double.infinity,
                            //       hint: signUpVM.userNameValue.value,
                            //       isPassword: false,
                            //       prefixIcon: "assets/auth/user1.png",
                            //       isPrefixIcon: true,
                            //       textFieldType: TextFieldType.PrefixOnly,
                            //       onChange: (value) {
                            //         signUpVM.updateUserName(value);
                            //       },
                            //     ),
                            //   ),
                            // ),
                            // Obx(
                            //   () => signUpVM.userNameExist.value
                            //       ? Padding(
                            //           padding: const EdgeInsets.symmetric(
                            //               vertical: 5),
                            //           child: Row(
                            //             children: [
                            //               AppText.customText(
                            //                 title: "User name is Already Exist",
                            //                 color: AppColor.red,
                            //                 fontWeight: FontWeights.regular,
                            //                 size: 12,
                            //               ),
                            //             ],
                            //           ),
                            //         )
                            //       : SizedBox(
                            //           height: 20,
                            //         ),
                            // ),
                            // Obx(
                            //   () =>
                            Focus(
                              onFocusChange: (hasFocus) {
                                print("hasFocus");
                                print(hasFocus);
                                if (!hasFocus) {
                                  signUpVM.checkEmailExistence(context);
                                }
                              },
                              child: InputField(
                                controller: emailController,
                                inputType: TextInputType.emailAddress,
                                width: double.infinity,
                                hint: "Email Address",
                                isPassword: false,
                                prefixIcon: "assets/auth/email.png",
                                isPrefixIcon: true,
                                textFieldType: TextFieldType.PrefixOnly,
                                onChange: (value) {
                                  signUpVM.updateEmail(value);
                                },
                              ),
                            ),
                            // ),
                            Obx(
                              () => signUpVM.emailExist.value
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5),
                                      child: Row(
                                        children: [
                                          AppText.customText(
                                            title: "Email is Already Exist",
                                            color: AppColor.red,
                                            fontWeight: FontWeights.regular,
                                            size: 12,
                                          ),
                                        ],
                                      ),
                                    )
                                  : SizedBox(
                                      height: 20,
                                    ),
                            ),
                            // Obx(
                            //   () => InputField(
                            //     width: double.infinity,
                            //     hint: signUpVM.genderValue.value,
                            //     isPassword: false,
                            //     prefixIcon: "assets/auth/gender.png",
                            //     isPrefixIcon: true,
                            //     textFieldType: TextFieldType.PrefixWithSufix,
                            //     readOnly: true,
                            //     onTap: () {
                            //       genderSelector(
                            //           context: context,
                            //           onSelectGender: (value) {
                            //             signUpVM.updateGender(value);
                            //           });
                            //     },
                            //     sufixIcon: "assets/auth/caret-down.png",
                            //     isSufixIcon: true,
                            //   ),
                            // ),

                            Row(
                              children: [
                                SizedBox(
                                  width: 80,
                                  child: InputField(
                                    captilization: TextCapitalization.sentences,

                                    controller: countryController,
                                    inputType: TextInputType.phone,
                                    onChange: (value) {},
                                    hintColor: AppColor.black,
                                    width: double.infinity,
                                    hint: "+971",
                                    prefixIcon: "assets/auth/phoneVerifiy.png",
                                    textFieldType: TextFieldType.Simple,
                                    isPrefixIcon: true,
                                    readOnly: true,
                                    onTap: () {
                                      showCountries(
                                          context: context,
                                          onSelectCountry: (country) {
                                            setState(() {
                                              countryController.text =
                                                  "+${country.phoneCode.toString()}";
                                            });
                                          });
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: InputField(
                                    controller: phoneNumberController,
                                    width: double.infinity,
                                    hint: "Phone Number",
                                    isPassword: false,
                                    prefixIcon: "assets/auth/telephone.png",
                                    isPrefixIcon: true,
                                    inputType: TextInputType.phone,
                                    textFieldType: TextFieldType.Simple,
                                    onChange: (value) {
                                      if (countryController.text.isNotEmpty) {
                                        signUpVM.updatePhoneNumber(
                                            countryController.text + value);
                                      } else {
                                        signUpVM
                                            .updatePhoneNumber("+971" + value);
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(
                              height: 20,
                            ),
                            // Obx(
                            //   () => InputField(
                            //     width: double.infinity,
                            //     hint: signUpVM.companyNameValue.value,
                            //     isPassword: false,
                            //     prefixIcon: "assets/auth/companyName.png",
                            //     isPrefixIcon: true,
                            //     textFieldType: TextFieldType.PrefixOnly,
                            //     onChange: (value) {
                            //       signUpVM.updateCompanyName(value);
                            //     },
                            //   ),
                            // ),
                            // SizedBox(
                            //   height: 20,
                            // ),
                            InputField(
                              captilization: TextCapitalization.sentences,

                              controller: cityController,
                              width: double.infinity,
                              hint: "City",
                              isPassword: false,
                              prefixIcon: "assets/auth/world.png",
                              isPrefixIcon: true,
                              // hintColor:  signUpVM.cityValue.value=="City"?AppColor.greyText:AppColor.black,
                              textFieldType: TextFieldType.PrefixWithSufix,
                              readOnly: true,
                              onTap: () {
                                citySelector(
                                    listOfCity: citesList,
                                    context: context,
                                    onCitySelector: (city) {
                                      signUpVM.updateCity(city);
                                      setState(() {
                                        cityController.text = city;
                                      });
                                    });
                              },
                              sufixIcon: "assets/auth/caret-down.png",
                              isSufixIcon: true,
                            ),

                            SizedBox(
                              height: 20,
                            ),
                            InputField(
                              controller: passwordController,
                              width: double.infinity,
                              hint: "Password",
                              isPassword: true,
                              prefixIcon: "assets/auth/lock.png",
                              isPrefixIcon: true,
                              textFieldType: TextFieldType.PrefixWithSufix,
                              onChange: (value) {
                                signUpVM.updatePassword(value);
                              },
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Obx(
                              () => signUpVM.isLoading.value
                                  ? spinKitButton(
                                      context,
                                      46,
                                      AppConfig(context).width - 60,
                                      Color(int.parse(
                                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")))
                                  : primaryButton(
                                      buttonTitle: "Sign Up",
                                      callback: () {
                                        signUpVM.onSubmit(
                                            context: context,
                                            onSubmit: (value) {
                                              if (value) {
                                                Navigator.push(
                                                    // Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          OTPVerificationViewController(
                                                        phoneNumber:
                                                            phoneNumberController
                                                                .value.text,
                                                      ),
                                                    ));
                                              } else {}
                                            });
                                      },
                                      width: AppConfig(context).width - 60,
                                      btnColor: AppColor.primaryColor,
                                      txtColor: AppColor.white,
                                      height: 46,
                                      fontweight: FontWeights.medium,
                                    ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                AppText.customText(
                                  title: "Already have an account?",
                                  color: AppColor.greyText,
                                  fontWeight: FontWeights.regular,
                                  size: 12,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              LoginViewController(),
                                        ));
                                  },
                                  child: AppText.customText(
                                    title: "Sign In",
                                    color: AppColor.DarkText,
                                    fontWeight: FontWeights.regular,
                                    size: 12,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
