import 'dart:async';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/forgot_password_view_model.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/new_password_view_model.dart';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class NewPasswordViewController extends StatefulWidget {
  NewPasswordViewController({
    Key? key,
  }) : super(key: key);

  @override
  _NewPasswordViewControllerState createState() =>
      _NewPasswordViewControllerState();
}

class _NewPasswordViewControllerState extends State<NewPasswordViewController> {
  final newPasswordVM = Get.put(NewPasswordViewModel());
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 320);
    return Scaffold(
        body: Container(
      width: AppConfig(context).width,
      height: AppConfig(context).height,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage("assets/auth/login_back.png"),
              width: AppConfig(context).width,
              height: 322,
              fit: BoxFit.fill,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              height: AppConfig(context).height - 300,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 45),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText.customText(
                        title: "New Password",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 24,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppText.customText(
                        title: "Please create a new password.",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 14,
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      AppText.customText(
                        title: "Enter New Password",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InputField(
                        controller: passwordController,
                        inputType: TextInputType.visiblePassword,
                        onChange: (value) {
                          // loginUpVM.updatePassword(value);
                        },
                        width: double.infinity,
                        hint: "Password",
                        isPassword: true,
                        prefixIcon: "assets/auth/lock.png",
                        textFieldType: TextFieldType.PrefixWithSufix,
                        isPrefixIcon: true,
                        // ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      AppText.customText(
                        title: "Enter Confirm Password",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.medium,
                        size: 14,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InputField(
                        controller: confirmPasswordController,
                        inputType: TextInputType.visiblePassword,
                        onChange: (value) {
                          // loginUpVM.updatePassword(value);
                        },
                        width: double.infinity,
                        hint: "Confirm Password",
                        isPassword: true,
                        prefixIcon: "assets/auth/lock.png",
                        textFieldType: TextFieldType.PrefixWithSufix,
                        isPrefixIcon: true,
                        // ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      primaryButton(
                        buttonTitle: "Submit",
                        callback: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => LoginViewController(),
                              ));
                        },
                        width: AppConfig(context).width - 40,
                        btnColor: AppColor.primaryColor,
                        txtColor: AppColor.white,
                        height: 46,
                        fontweight: FontWeights.medium,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
