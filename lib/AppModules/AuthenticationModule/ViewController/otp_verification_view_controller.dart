import 'dart:async';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pin_code_fields/pin_code_fields.dart';

class OTPVerificationViewController extends StatefulWidget {
  final String phoneNumber;
  OTPVerificationViewController({
    Key? key,
    required this.phoneNumber
  }) : super(key: key);

  @override
  _OTPVerificationViewControllerState createState() =>
      _OTPVerificationViewControllerState();
}

class _OTPVerificationViewControllerState
    extends State<OTPVerificationViewController> {
  Timer? _timer;
  int _start = 36;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
          // Navigator.pop(context);
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 320);
    return Scaffold(
        body: Container(
      width: AppConfig(context).width,
      height: AppConfig(context).height,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage("assets/auth/login_back.png"),
              width: AppConfig(context).width,
              height: 322,
              fit: BoxFit.fill,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              width: AppConfig(context).width,
              height: AppConfig(context).height - 300,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText.customText(
                        title: "Enter 4 Digits Code",
                        color: AppColor.DarkText,
                        fontWeight: FontWeights.semi_bold,
                        size: 24,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      AppText.customText(
                        title:
                            "Please enter the verification code, was send to your mobile ${widget.phoneNumber.substring(0,widget.phoneNumber.length-3)}",
                        color: AppColor.textGREY,
                        fontWeight: FontWeights.regular,
                        size: 14,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      PinCodeTextField(
                        appContext: context,
                        // pastedTextStyle: TextStyle(
                        //   color: Colors.green.shade600,
                        //   fontWeight: FontWeight.bold,
                        // ),
                        length: 4,
                        obscureText: true,
                        obscuringCharacter: '*',
                        blinkWhenObscuring: true,
                        animationType: AnimationType.fade,
                        // validator: (v) {
                        //   if (v!.length < 3) {
                        //     return "I'm from validator";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        pinTheme: PinTheme(
                          activeColor: Colors.grey.shade200,
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: 50,
                          fieldWidth: 50,
                          activeFillColor: Colors.white,
                          inactiveFillColor: Colors.white,
                          borderWidth: 2,
                          disabledColor: Colors.black,
                          errorBorderColor: Colors.black,
                          inactiveColor: Colors.grey.shade200,
                          selectedColor: Colors.grey.shade200,
                          selectedFillColor: Colors.white,
                        ),
                        textStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeights.medium),
                        pastedTextStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeights.medium),
                        enablePinAutofill: true,
                        cursorColor: Colors.black,
                        animationDuration: Duration(milliseconds: 300),
                        enableActiveFill: true,
                        // errorAnimationController: errorController,
                        // controller: textEditingController,
                        keyboardType: TextInputType.number,
                        backgroundColor: Colors.white,
                        boxShadows: [
                          BoxShadow(
                            offset: Offset(0, 8),
                            color: Colors.grey.shade200,
                            blurRadius: 15,
                          )
                        ],
                        onCompleted: (v) {
                          print("Completed");
                        },
                        // onTap: () {
                        //   print("Pressed");
                        // },
                        onChanged: (value) {
                          print(value);
                          setState(() {
                            // currentText = value;
                          });
                        },
                        beforeTextPaste: (text) {
                          print("Allowing to paste $text");
                          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                          //but you can show anything you want here, like your pop up saying wrong paste format or etc
                          return true;
                        },
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      primaryButton(
                        buttonTitle: "Verify",
                        callback: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    OnBoardingViewController(),
                              ));
                        },
                        width:  AppConfig(context).width - 40,
                        btnColor: AppColor.primaryColor,
                        txtColor: AppColor.white,
                        height: 46,
                        fontweight: FontWeights.medium,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AppText.customText(
                            title: "00:${_start.toString()}",
                            color: AppColor.primaryColor,
                            fontWeight: FontWeights.regular,
                            size: 14,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AppText.customText(
                            title: "Resend Code",
                            color: _start == 0
                                ? AppColor.primaryColor
                                : AppColor.textGREY,
                            fontWeight: FontWeights.regular,
                            size: 14,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
