import 'dart:io' show Platform;
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/google_login_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/AuthServices/location_services.dart';
import 'package:arwa/AppModules/AuthenticationModule/View/AuthField.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/SignUpViewController.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/forgot_password_view_controller.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/phone_verification_view.dart';
import 'package:arwa/AppModules/AuthenticationModule/ViewModel/login_view_model.dart';
import 'package:arwa/AppModules/GoogleMapModule/getLocationService.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/SplashAndOnboardModule/ViewController/OnBoardViewController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/google_auth_BTN.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/CheckConnectivity.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/spint_kit_view_spinner.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginViewController extends StatefulWidget {
  const LoginViewController({Key? key}) : super(key: key);

  @override
  _LoginViewControllerState createState() => _LoginViewControllerState();
}

class _LoginViewControllerState extends State<LoginViewController> {
  final loginUpVM = Get.put(LoginViewModel());
  final googleLoginVM = Get.put(GoogleLoginService());
  CheckConnectivityServices connectivityServices = CheckConnectivityServices();
  LoginLocationServices locationServices = LoginLocationServices();
  final categoryController = Get.put(HomeCategoryController());
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  changeInternet() async {
    print(connectivityServices.internetStatus);
  }

  @override
  Widget build(BuildContext context) {
    print(AppConfig(context).height - 322);
    return Scaffold(
        backgroundColor: AppColor.primaryColor,
        body: SafeArea(
          child: Container(
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Image(
                    image: AssetImage("assets/auth/login_back.png"),
                    width: AppConfig(context).width,
                    height: 350,
                    fit: BoxFit.cover,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30)),
                    ),
                    width: AppConfig(context).width,
                    height: AppConfig(context).height / 1.7,
                    child: Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppText.customText(
                              title: "Login to Arwa",
                              color: AppColor.DarkText,
                              fontWeight: FontWeights.semi_bold,
                              size: 24,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // Obx(
                            //   () =>
                            InputField(

                              controller: emailController,
                              inputType: TextInputType.emailAddress,
                              onChange: (value) {
                                loginUpVM.updateUserName(value);
                              },
                              width: double.infinity,
                              hint: "Email Address",
                              prefixIcon: "assets/auth/email.png",
                              textFieldType: TextFieldType.PrefixOnly,
                              isPrefixIcon: true,
                            ),
                            // ),
                            SizedBox(
                              height: 20,
                            ),
                            // Obx(
                            //   () =>
                            InputField(
                              controller: passwordController,
                              inputType: TextInputType.visiblePassword,
                              onChange: (value) {
                                loginUpVM.updatePassword(value);
                              },
                              width: double.infinity,
                              hint: "Password",
                              isPassword: true,
                              prefixIcon: "assets/auth/lock.png",
                              textFieldType: TextFieldType.PrefixWithSufix,
                              isPrefixIcon: true,
                              // ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ForgotPasswordViewController(),
                                        ));
                                  },
                                  child: AppText.customText(
                                    title: "Forgot Password?",
                                    color: AppColor.DarkText,
                                    fontWeight: FontWeights.regular,
                                    size: 12,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Obx(
                              () => loginUpVM.isLoading.value
                                  ? spinKitButton(
                                      context,
                                      46,
                                      AppConfig(context).width - 60,
                                      Color(int.parse(
                                          "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")))
                                  : primaryButton(
                                      buttonTitle: "Sign In",
                                      callback: () async {
                                        // print("LocalDBHandler().getBox()");
                                        // print(await LocalDBHandler().getToken());

                                        loginUpVM.onSubmit(
                                            context: context,
                                            onSubmit: (value) {
                                              if (value) {
                                                Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          OnBoardingViewController(),
                                                    ));
                                              } else {}
                                            });
                                      },
                                      width: AppConfig(context).width - 60,
                                      btnColor: AppColor.primaryColor,
                                      txtColor: AppColor.white,
                                      height: 46,
                                      fontweight: FontWeights.medium,
                                    ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                AppText.customText(
                                  title: "Don’t have an account yet?",
                                  color: AppColor.greyText,
                                  fontWeight: FontWeights.regular,
                                  size: 12,
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              SignUpViewController(),
                                        ));
                                  },
                                  child: AppText.customText(
                                    title: "Create an account",
                                    color: AppColor.DarkText,
                                    fontWeight: FontWeights.regular,
                                    size: 12,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            if (Platform.isIOS)
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // InkWell(
                                  //   onTap: () {
                                  //     Navigator.push(
                                  //         context,
                                  //         MaterialPageRoute(
                                  //           builder: (context) =>
                                  //               PhoneVerificationView(),
                                  //         ));
                                  //
                                  //     // onFaceBookSocialAuth();
                                  //   },
                                  //   child: Image(
                                  //     image:
                                  //         AssetImage("assets/auth/facebook.png"),
                                  //     width: 40,
                                  //     height: 40,
                                  //     fit: BoxFit.fill,
                                  //   ),
                                  // ),
                                  // SizedBox(
                                  //   width: 15,
                                  // ),
                                  InkWell(
                                    onTap: () {
                                      // onAppleSocialAuth();
                                    },
                                    child: Image(
                                      image:
                                          AssetImage("assets/auth/apple.png"),
                                      width: 40,
                                      height: 40,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      onGoogleSocialAuth();
                                    },
                                    child: Image(
                                      image:
                                          AssetImage("assets/auth/google.png"),
                                      width: 40,
                                      height: 40,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ],
                              ),
                            if (Platform.isAndroid)
                              googleAuthButton(
                                callback: () {
                                  onGoogleSocialAuth();
                                },
                                height: 46,
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  onGoogleSocialAuth() async {
    print("Google Auth CLicked");
    UserCredential user = await googleLoginVM.onGoogleSignIn();

    final FirebaseAuth auth = FirebaseAuth.instance;
    final User currentUser = auth.currentUser!;
    final uid = currentUser.uid;
    if (uid != null && user != null) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PhoneVerificationView(
                email: user.user!.email.toString(),
                profile: user.user!.photoURL.toString(),
                name: user.user!.displayName.toString(),
                authType: "Google",
                UID: uid),
          ));

      // citySelector(
      //     listOfCity: citesList,
      //     context: context,
      //     onCitySelector: (city) {
      //       loginUpVM.onSocialLogin(
      //           context: context,
      //           onSubmit: (value) {
      //             if (value) {
      //               Navigator.pushReplacement(
      //                   context,
      //                   MaterialPageRoute(
      //                     builder: (context) => OnBoardingViewController(),
      //                   ));
      //             }
      //           },
      //           name: user.user!.displayName.toString(),
      //           profile: user.user!.photoURL.toString(),
      //           email: user.user!.email.toString(),
      //           UID: uid,
      //           city: city,
      //           loginType: "Google");
      //     });
    }

    // SocialLocation userLocation=await  locationServices.initCurrentLocation();
    // print(userLocation.country);
    // print(userLocation.city);
  }

  onAppleSocialAuth() async {
    print("Google Auth CLicked");
    SocialLocation userLocation = await locationServices.initCurrentLocation();
    print(userLocation.country);
    print(userLocation.city);
  }

  onFaceBookSocialAuth() async {
    print("Google Auth CLicked");
    SocialLocation userLocation = await locationServices.initCurrentLocation();
    print(userLocation.country);
    print(userLocation.city);
  }
}
