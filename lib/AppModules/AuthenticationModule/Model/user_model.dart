
import 'dart:convert';

import 'package:flutter/material.dart';

// User userFromJson(String str) => User.fromJson(json.decode(str));

// String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    required this.id,
    required this.customerid,
    required this.firstname,
    required this.lastname,
    required this.username,
    required this.email,
    required this.password,
    required this.gender,
    required this.contact,
    required this.companyname,
    required this.city,
    required this.country,
    required this.creationdate,
    required this.loginstatus,
    required this.passwordexpirydays,
    required this.passwordchangedon,
    required this.profileimage,
  });

  int id;
  String profileimage;
  String customerid;
  String firstname;
  String lastname;
  String username;
  String email;
  String password;
  String gender;
  String contact;
  String companyname;
  String city;
  String country;
  String creationdate;
  int loginstatus;
  int passwordexpirydays;
  String passwordchangedon;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"]??0,
        customerid: json["customerid"]??"",
        firstname: json["firstname"]??"",
        lastname: json["lastname"]??"",
        username: json["username"]??"",
        email: json["email"]??"",
        password: json["password"]??"",
        gender: json["gender"]??"",
        contact: json["contact"]??"",
        companyname: json["companyname"]??"",
        city: json["city"]??"",
        country: json["country"]??"",
        creationdate: json["creationdate"]??"",
        loginstatus: json["loginstatus"]??0,
        passwordexpirydays: json["passwordexpirydays"]??0,
        passwordchangedon: json["passwordchangedon"]??"",
    profileimage: json["profileimage"]??"",
      );


}
Map<String, dynamic> userToJson(User user) => {
  "id": user.id,
  "customerid": user.customerid,
  "firstname": user.firstname,
  "lastname": user.lastname,
  "username": user.username,
  "email": user.email,
  "password": user.password,
  "gender":user. gender,
  "contact":user. contact,
  "companyname": user.companyname,
  "city": user.city,
  "country":user. country,
  "creationdate": user.creationdate,
  "loginstatus": user.loginstatus,
  "passwordexpirydays":user. passwordexpirydays,
  "passwordchangedon": user.passwordchangedon,
  "profileimage": user.profileimage,
};