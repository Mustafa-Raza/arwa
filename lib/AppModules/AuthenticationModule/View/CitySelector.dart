import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future citySelector(
    {required BuildContext context,
    required List<String> listOfCity,
    required Function onCitySelector}) {
  return showModalBottomSheet(
    backgroundColor: Colors.white.withOpacity(0),
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return Container(
            height: 300,
            decoration: BoxDecoration(
              color: Colors.white,
              // color: Colors.white.withOpacity(0),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    AppText.customText(
                        title: "Select Your City",
                        color: AppColor.primaryColor,
                        fontWeight: FontWeights.semi_bold,
                        size: 20),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: listOfCity.isNotEmpty
                        ? Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: ListView.builder(
                              itemCount: listOfCity.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    onCitySelector(listOfCity[index]);
                                    Navigator.pop(context);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        AppText.customText(
                                            title: listOfCity[index]),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  topLeft: Radius.circular(20)),
                            ),
                            child: Center(
                                child: AppText.customText(
                                    title: "No City Found"))),
                  ),
                ),

                // GestureDetector(
                //   onTap: () {
                //     // print("Other");
                //     // onCitySelector("Other");
                //     Navigator.of(context).pop();
                //   },
                //   child:  Padding(
                //     padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                //     child: Container(
                //       height: 50,
                //       // width: 60,
                //       decoration: BoxDecoration(
                //         color: Colors.white,
                //         borderRadius: BorderRadius.circular(10),
                //       ),
                //       child: Center(
                //         child: Text("Cancel",
                //             style: const TextStyle(
                //                 color: Colors.red,
                //                 fontSize: 20,
                //                 fontWeight: FontWeight.w400)),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          );
        },
      );
    },
  );
}
