import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///TODO: Textfield Enum
enum TextFieldType {
  Simple,
  PrefixOnly,
  SufixOnly,
  PrefixWithSufix,
}

///TODO:auth Form Field
class InputField extends StatefulWidget {
  final String hint;
  final Color hintColor;
  final TextInputType inputType;
  final bool isPassword;
  final TextCapitalization captilization;
  final bool isPrefixIcon;
  final String prefixIcon;
  final bool isSufixIcon;
  final bool readOnly;
  final String sufixIcon;
  final TextFieldType textFieldType;
  final double width;
  final double height;
  final VoidCallback? onTap;
  final Function? onChange;
  final TextEditingController controller;

  InputField({
    Key? key,
    this.inputType = TextInputType.text,
     this. captilization=TextCapitalization.none,
    required this.hint,
    required this.controller,
    this.onChange,
    this.textFieldType = TextFieldType.Simple,
    this.width = 300,
    this.height = 50,
    this.onTap,
    this.hintColor = AppColor.greyText,
    this.isPassword = false,
    this.isPrefixIcon = false,
    this.readOnly = false,
    this.prefixIcon = "assets/auth/user.png",
    this.isSufixIcon = false,
    this.sufixIcon = "assets/auth/eye-slash.png",
  }) : super(key: key);

  @override
  _InputFieldState createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  bool _secureText = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.isPassword) {
      setState(() {
        _secureText = !_secureText;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.white,
        // boxShadow: [
        //   BoxShadow(
        //     color: AppColor.border,
        //   )
        // ],
      ),
      height: widget.height,
      child: widget.textFieldType == TextFieldType.PrefixWithSufix
          ? TextFormField(
        textCapitalization: widget.captilization,
        controller: widget.controller,
              readOnly: widget.readOnly,
              obscureText: _secureText,
              cursorColor: AppColor.primaryColor,
              keyboardType: widget.inputType,
              onTap: () {
                if (widget.readOnly) {
                  widget.onTap!();
                }
                print("selected");
                // genderSelector(context: context, onSelectGender:(selected){
                //
                // });
              },
              onChanged: (value) {
                widget.onChange!(value);
              },
              decoration: InputDecoration(
                prefixIcon: Padding(
                  padding: EdgeInsets.all(widget.isPrefixIcon ? 15 : 0),
                  child: widget.isPrefixIcon
                      ? Image(
                          image: AssetImage(
                            widget.prefixIcon,
                          ),
                          width: 20,
                          color: AppColor.greyText,
                          height: 20,
                        )
                      : SizedBox(
                          width: 0,
                        ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(
                    color: AppColor.border,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(
                    color: AppColor.border,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(
                    color: AppColor.border,
                  ),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(
                    color: AppColor.border,
                  ),
                ),
                disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(
                    color: AppColor.border,
                  ),
                ),
                contentPadding:
                    EdgeInsets.only(left: 0, bottom: 0, top: 0, right: 0),
                hintText: widget.hint,
                hintStyle: TextStyle(
                    color: widget.hintColor,
                    fontSize: 12,
                    fontWeight: FontWeights.regular),
                suffixIcon: widget.isPassword || widget.isSufixIcon
                    ? InkWell(
                        onTap: () {
                          if (widget.isPassword)
                            setState(() {
                              _secureText = !_secureText;
                            });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image(
                            image: AssetImage(widget.sufixIcon),
                            height: 16,
                            width: 16,
                          ),
                        ),
                      )
                    : SizedBox(
                        // height: 1,
                        // width: 1,
                        ),
              ),
            )
          : widget.textFieldType == TextFieldType.PrefixOnly
              ? TextFormField(
        textCapitalization: widget.captilization,
        controller: widget.controller,
                  readOnly: widget.readOnly,
                  obscureText: _secureText,
                  cursorColor: AppColor.primaryColor,
                  onTap: () {
                    if (widget.readOnly) {
                      widget.onTap!();
                    }
                    // genderSelector(context: context, onSelectGender:(selected){
                    //
                    // });
                  },
                  onChanged: (value) {
                    widget.onChange!(value);
                  },
                  keyboardType: widget.inputType,
                  decoration: InputDecoration(
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(widget.isPrefixIcon ? 15 : 0),
                      child: widget.isPrefixIcon
                          ? Image(
                              image: AssetImage(
                                widget.prefixIcon,
                              ),
                              width: 20, color: AppColor.greyText,
                              height: 20,
                            )
                          : SizedBox(
                              width: 0,
                            ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: BorderSide(
                        color: AppColor.border,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: BorderSide(
                        color: AppColor.border,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: BorderSide(
                        color: AppColor.border,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: BorderSide(
                        color: AppColor.border,
                      ),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: BorderSide(
                        color: AppColor.border,
                      ),
                    ),
                    contentPadding:
                        EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 10),
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                        color: widget.hintColor,
                        fontSize: 12,
                        fontWeight: FontWeights.regular),
                  ),
                )
              : widget.textFieldType == TextFieldType.SufixOnly
                  ? TextFormField(
        textCapitalization: widget.captilization,
        controller: widget.controller,
                      onChanged: (value) {
                        widget.onChange!(value);
                      },
                      readOnly: widget.readOnly,
                      obscureText: _secureText,
                      cursorColor: AppColor.primaryColor,
                      onTap: () {
                        if (widget.readOnly) {
                          widget.onTap!();
                        }
                        // genderSelector(context: context, onSelectGender:(selected){
                        //
                        // });
                      },
                      keyboardType: widget.inputType,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        contentPadding: EdgeInsets.only(
                            left: 20, bottom: 0, top: 0, right: 10),
                        hintText: widget.hint,
                        hintStyle: TextStyle(
                            color: widget.hintColor,
                            fontSize: 12,
                            fontWeight: FontWeights.regular),
                        suffixIcon: widget.isPassword || widget.isSufixIcon
                            ? InkWell(
                                onTap: () {
                                  if (widget.isPassword)
                                    setState(() {
                                      _secureText = !_secureText;
                                    });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Image(

                                    image: AssetImage(widget.sufixIcon),
                                    height: 16,
                                    width: 16,
                                  ),
                                ),
                              )
                            : SizedBox(
                                // height: 1,
                                // width: 1,
                                ),
                      ),
                    )
                  : TextFormField(
        textCapitalization: widget.captilization,
        controller: widget.controller,
                      onChanged: (value) {
                        widget.onChange!(value);
                      },
                      readOnly: widget.readOnly,
                      obscureText: _secureText,
                      cursorColor: AppColor.primaryColor,
                      onTap: () {
                        if (widget.readOnly) {
                          widget.onTap!();
                        }
                        // genderSelector(context: context, onSelectGender:(selected){
                        //
                        // });
                      },
                      keyboardType: widget.inputType,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                            color: AppColor.border,
                          ),
                        ),
                        contentPadding: EdgeInsets.only(
                            left: 20, bottom: 0, top: 0, right: 10),
                        hintText: widget.hint,
                        hintStyle: TextStyle(
                            color: widget.hintColor,
                            fontSize: 12,
                            fontWeight: FontWeights.regular),
                      ),
                    ),
    );
  }
}
