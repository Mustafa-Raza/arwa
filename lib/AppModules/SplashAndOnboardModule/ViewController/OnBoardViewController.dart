
import 'package:arwa/AppModules/RootPageModule/AppRootPageController.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnBoardingViewController extends StatefulWidget {
  const OnBoardingViewController({Key? key}) : super(key: key);

  @override
  _OnBoardingViewControllerState createState() =>
      _OnBoardingViewControllerState();
}

class _OnBoardingViewControllerState extends State<OnBoardingViewController> {
  int currentIndex=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.primaryColor,
      body: SafeArea(
        child: Container(
          width: AppConfig(context).width,
          height: AppConfig(context).height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: AppConfig(context).width,
                  height: AppConfig(context).height - 230,
                  child: Image(
                    image: AssetImage("assets/onBoard/onbaord.png"),
                    // width: AppConfig(context).width,
                    // height: AppConfig(context).height,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: AppConfig(context).width,
                  height: AppConfig(context).height / 2.2,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          CarouselSlider(
                            options: CarouselOptions(
                              // height: 100,
                              // aspectRatio: 16 / 9,
                              // viewportFraction: 0.8,
                              initialPage: 0,
                              enableInfiniteScroll: false,
                              reverse: false,
                              autoPlay: true,
                              autoPlayInterval: Duration(seconds: 3),
                              autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                              autoPlayCurve: Curves.fastOutSlowIn,
                              // enlargeCenterPage: true,
                              // onPageChanged: callbackFunction,
                              onPageChanged: (index, reason) {
                                print(index);
                               setState(() {
                                 currentIndex=index;
                               });

                              },
                              aspectRatio: 2.0,
                              disableCenter: true,
                              viewportFraction: 1,
                              enlargeCenterPage: false,
                              scrollDirection: Axis.horizontal,
                            ),
                            items: [1,2,3].map((address) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return Column(
                                    children: [
                                      AppText.customText(
                                        title: "Welcome to Arwa",
                                        color: AppColor.DarkText,
                                        fontWeight: FontWeights.semi_bold,
                                        size: 26,
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      AppText.customText(
                                        alignment: TextAlign.center,
                                        title:
                                        "scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.",
                                        color: AppColor.greyText,
                                        fontWeight: FontWeights.regular,
                                        size: 14,
                                      ),
                                    ],
                                  );
                                },
                              );
                            }).toList(),
                          ),
                          // AppText.customText(
                          //   title: "Welcome to Arwa",
                          //   color: AppColor.DarkText,
                          //   fontWeight: FontWeights.semi_bold,
                          //   size: 26,
                          // ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // AppText.customText(
                          //   alignment: TextAlign.center,
                          //   title:
                          //       "scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.",
                          //   color: AppColor.greyText,
                          //   fontWeight: FontWeights.regular,
                          //   size: 14,
                          // ),
                          // SizedBox(
                          //   height: 30,
                          // ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                radius: 4,
                                backgroundColor: currentIndex==0?AppColor.primaryColor:Colors.grey.shade300,

                              ),
                              // Image(
                              //   image: AssetImage("assets/onBoard/dot-2.png"),
                              //   width: 8,
                              //   height: 8,
                              //   color: currentIndex==0?AppColor.primaryColor:Colors.grey,
                              //   fit: BoxFit.fill,
                              // ),
                              SizedBox(
                                width: 8,
                              ),
                              CircleAvatar(
                                radius: 4,
                                backgroundColor: currentIndex==1?AppColor.primaryColor:Colors.grey.shade300,

                              ),
                              // Image(
                              //   image: AssetImage("assets/onBoard/dot-1.png"),
                              //   width: 8,
                              //   height: 8,
                              //   color: currentIndex==1?AppColor.primaryColor:Colors.grey,
                              //   fit: BoxFit.fill,
                              // ),
                              SizedBox(
                                width: 8,
                              ),
                              CircleAvatar(
                                radius: 4,
                                backgroundColor: currentIndex==2?AppColor.primaryColor:Colors.grey.shade300,

                              ),
                              // Image(
                              //   image: AssetImage("assets/onBoard/dot.png"),
                              //   width: 8,
                              //   color: currentIndex==2?AppColor.primaryColor:Colors.grey,
                              //   height: 8,
                              //   fit: BoxFit.fill,
                              // ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          primaryButton(
                            buttonTitle: "Next",
                            callback: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        AppRootPageController(),
                                  ));
                              // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeViewController(),));
                            },
                            width: AppConfig(context).width - 60,
                            btnColor: AppColor.primaryColor,
                            txtColor: AppColor.white,
                            height: 46,
                            fontweight: FontWeights.medium,
                          ),
                          SizedBox(
                            height: 11,
                          ),
                          primaryButton(
                            buttonTitle: "Skip",
                            callback: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        AppRootPageController(),
                                  ));
                            },
                            width: AppConfig(context).width - 60,
                            btnColor: AppColor.white,
                            txtColor: AppColor.DarkText,
                            height: 46,
                            fontweight: FontWeights.medium,
                          ),
                          SizedBox(
                            height: 11,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
