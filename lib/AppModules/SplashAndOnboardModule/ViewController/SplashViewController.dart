import 'dart:async';
import 'package:arwa/AppModules/AuthenticationModule/ViewController/LoginViewController.dart';
import 'package:arwa/AppModules/RootPageModule/AppRootPageController.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashViewController extends StatefulWidget {
  const SplashViewController({Key? key}) : super(key: key);

  @override
  _SplashViewControllerState createState() => _SplashViewControllerState();
}

class _SplashViewControllerState extends State<SplashViewController> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    createLocalDB();
    Timer(Duration(seconds: 2), () {
      navigationPage();
    });
  }
  void createLocalDB()async{
    if(await DBProvider.db.databaseExists()){
      print("local db exsit");

    }else{
      print("local db not exsit");
      await DBProvider.db.initDB();
    }
  }
  void navigationPage() async{
    if(await DatabaseHandler().isExists('currentUser')){
      print("accces token");
      print(await DatabaseHandler().getCurrentUser());
      print(await DatabaseHandler().getToken());
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            // builder: (context) =>testhtml(),
            builder: (context) =>AppRootPageController(),
          ));
    }else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginViewController(),
          ));
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          width: AppConfig(context).width,
          height: AppConfig(context).height,
          child: Image(
            image: AssetImage("assets/splash/splash.png"),
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
