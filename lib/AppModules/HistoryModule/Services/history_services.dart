import 'dart:convert';

import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/Network/api_service.dart';
import 'package:arwa/AppModules/Utills/Network/api_url.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:arwa/AppModules/Utills/share_preferece_handler.dart';
import 'package:flutter/cupertino.dart';

Future< List<dynamic>> getHistoryDataService({
  required BuildContext context,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_ORDERS_HISTORY + "${user["customerid"]}", context: context);
  if (response == null) {
    return [];
  } else if (jsonDecode(response.body)["status"] == 200) {
    print("Get History Data");

    ///TODO:Save home data to Local DB
    if ((await DBProvider.db.checkDataExistenceByLength("History")) == 0) {
      print("History Inserted to SQL");
      await DBProvider.db.createHistory(response.body.toString());
      // .createHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getHistory());
    } else {
      print("History Updated to SQL");
      await DBProvider.db.updateHistory(response.body.toString());
      // .updateHome(jsonDecode(response.body)["data"].toString());
      print("SQLITE");
      print(await DBProvider.db.getHistory());
    }

    return jsonDecode(response.body)["data"];
  } else {
    return [];
  }
}
Future<List<ProductModel>> getHistoryDetailProductsDataService({
  required BuildContext context,
  required String orderID,
}) async {
  var user = await DatabaseHandler().getCurrentUser();
  var response = await ApiCall().getRequestHeader(
      apiUrl: GET_ORDERS_HISTORY_DETAIL + "$orderID",
      context: context);

  if (jsonDecode(response.body)["status"] == 200) {
    print("Get orderdetail Data");

    // ///TODO:Save home data to Local DB
    // if ((await DBProvider.db.checkDataExistenceByLength("Product")) == 0) {
    //   print("brand Inserted to SQL");
    //   await DBProvider.db.createProducts(response.body.toString());
    //   // .createHome(jsonDecode(response.body)["data"].toString());
    //   print("SQLITE");
    //   print(await DBProvider.db.getProducts());
    // } else {
    //   print("brand Updated to SQL");
    //   await DBProvider.db.updateProducts(response.body.toString());
    //   // .updateHome(jsonDecode(response.body)["data"].toString());
    //   print("SQLITE");
    //   print(await DBProvider.db.getProducts());
    // }

    return ProductModel.jsonToProductsList(
        jsonDecode(response.body)["data"]["orderdetail"]);
  } else {
    return [];
  }
}
