import 'dart:convert';

import 'package:arwa/AppModules/HistoryModule/Model/HistoryModel.dart';
import 'package:arwa/AppModules/HistoryModule/Services/history_services.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/Services/product_services.dart';
import 'package:arwa/AppModules/Utills/local_db_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class HistoryViewModel extends GetxController {
  // final List<Map<String, List<HistoryModel>>>
  var historyData = [].obs;
  var orderDetailData = [].obs;
  RxDouble totalPrice = 0.0.obs;
  RxDouble totalDiscount = 0.0.obs;
  RxDouble grossPrice = 0.0.obs;

  Future<List<Map<String, List<HistoryModel>>>> getHistoryData(
      {required BuildContext context}) async {
    List<Map<String, List<HistoryModel>>> historyModel = [];
    List<dynamic> resultData = [];
    // await getHistoryDataService(context: context);

    if ((await DBProvider.db.checkDataExistenceByLength("History")) == 0) {
      resultData = await getHistoryDataService(context: context);
    } else {
      print("productsModel is fetched in from local");
      //
      var localData = await DBProvider.db.getHistory();

      resultData = jsonDecode(localData[0]["history"])["data"];
      // ProductModel.jsonToProductsList(
      // jsonDecode(localData[0]["history"])["data"]);

      getHistoryDataService(context: context).then((value) => {
            print("History is fetched in background"),
            historyData.clear(),
            for (int i = 0; i < value.length; i++)
              {
                historyData.add({
                  "${value[i].keys.toList()[0]}":
                      HistoryModel.jsonToHistoryList(
                          value[i].values.toList()[0]),
                }),
              }
          });
    }

    for (int i = 0; i < resultData.length; i++) {
      historyModel.add({
        "${resultData[i].keys.toList()[0]}":
            HistoryModel.jsonToHistoryList(resultData[i].values.toList()[0]),
      });
    }
    historyData.value = historyModel;
    debugPrint("resultData");
    debugPrint(resultData.toString());
    return historyModel;
  }

  Future<List<ProductModel>> getOrderDetailData(
      BuildContext context, String orderID) async {
    List<ProductModel> productsModel = [];
    productsModel = await getHistoryDetailProductsDataService(
        context: context, orderID: orderID);

    orderDetailData.value.clear();
    orderDetailData.value = productsModel;
    setPriceAndDiscount();
    return productsModel;
  }

  setPriceAndDiscount() {
    totalPrice.value = 0.0;
    totalDiscount.value = 0.0;
    grossPrice.value = 0.0;
    // discountedPrice.value = 0.0;
    for (int i = 0; i < orderDetailData.value.length; i++) {
      totalPrice.value = totalPrice.value +
          (orderDetailData.value[i].baseprice *
              orderDetailData.value[i].cartQuantity);
      totalDiscount.value = totalDiscount.value +
          (orderDetailData.value[i].discountprice *
              orderDetailData.value[i].cartQuantity);
    }
    grossPrice.value = totalPrice.value - totalDiscount.value;
  }
}
