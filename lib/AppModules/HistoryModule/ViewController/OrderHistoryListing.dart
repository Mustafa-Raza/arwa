// import 'package:arwa/AppModules/HistoryModule/Model/HistoryModel.dart';
// import 'package:arwa/AppModules/HistoryModule/View/ProductTileView.dart';
// import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
// import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
// import 'package:flutter/cupertino.dart';
//
// class OrderHistoryListing extends StatelessWidget {
//   final Map<String, List<HistoryModel>> orderData;
//
//   OrderHistoryListing({Key? key, required this.orderData}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: [
//           Padding(
//               padding: const EdgeInsets.only(
//                   left: 20, right: 20, top: 5, bottom: 20),
//               child: AppText.customText(
//                   title: "${orderData.keys.toList()[0]}",
//                   color: AppColor.textGREY,
//                   fontWeight: FontWeights.regular,
//                   size: 14)),
//           ListView.builder(
//             shrinkWrap: true,
//             itemCount: orderData.values.toList()[0].length,
//             itemBuilder: (context, index) {
//               return productTileView(
//                   context: context,
//                   historyModel: orderData.values.toList()[0][index],
//                   // HistoryModel(
//                   //     images: ['assets/home/brands/arwa.png','assets/home/brands/arwa.png','assets/home/brands/arwa.png','assets/home/brands/arwa.png'],
//                   //     orderPrice: "190.00",
//                   //     orderStatus: OrderStatus.Complete,
//                   //     orderTitle: "OD - 424923192 - N"),
//                   callback: () {});
//             },
//           )
//         ],
//       ),
//     );
//   }
// }
