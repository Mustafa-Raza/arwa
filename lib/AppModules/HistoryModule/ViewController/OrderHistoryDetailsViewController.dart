import 'package:arwa/AppModules/HistoryModule/Model/HistoryModel.dart';
import 'package:arwa/AppModules/HistoryModule/View/PaymentText.dart';
import 'package:arwa/AppModules/HistoryModule/View/ProductDetailTile.dart';
import 'package:arwa/AppModules/HistoryModule/ViewModel/history_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class OrderHistoryDetailsViewController extends StatefulWidget {
  final HistoryModel historyModel;

  OrderHistoryDetailsViewController({Key? key, required this.historyModel})
      : super(key: key);

  @override
  State<OrderHistoryDetailsViewController> createState() =>
      _OrderHistoryDetailsViewControllerState();
}

class _OrderHistoryDetailsViewControllerState
    extends State<OrderHistoryDetailsViewController> {
  final historyVM = Get.put(HistoryViewModel());
  Future<List<ProductModel>>? getDetailProducts;
  final categoryController = Get.put(HomeCategoryController());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDetailProducts = historyVM.getOrderDetailData(
        context, widget.historyModel.orderid.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: InnerAppBar(title: "Order Details"),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, top: 30, right: 20),
                      height: AppConfig(context).height / 1.2,
                      child: SingleChildScrollView(
                        physics: ScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppText.customText(
                              title:
                                  'Order #${widget.historyModel.orderid.toString()}',
                              fontWeight: FontWeights.semi_bold,
                              size: 30,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                AppText.customText(
                                    title: 'Placed',
                                    fontWeight: FontWeights.regular,
                                    size: 16,
                                    color: AppColor.primaryColor),
                                AppText.customText(
                                    title:
                                        ' ${widget.historyModel.placedon.toString()}',
                                        // ' ${widget.historyModel.deliverydate.toString()}',
                                    // title: ' on 23 Oct, 2021 at 09:23 AM',
                                    fontWeight: FontWeights.regular,
                                    size: 16,
                                    color: AppColor.greyText),
                              ],
                            ),
                            FutureBuilder<List<ProductModel>>(
                                // future: productVM.getProductsData(context,widget.brandCode),
                                future: getDetailProducts,
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return SizedBox(
                                      height: AppConfig(context).width,
                                      child: Center(
                                        child: FavShimmerView(),
                                      ),
                                    );
                                  }

                                  return Obx(() => historyVM
                                          .orderDetailData.value.isNotEmpty
                                      ? ListView.builder(
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          itemCount: historyVM
                                              .orderDetailData.value.length,
                                          shrinkWrap: true,
                                          itemBuilder: (context, index) {
                                            return productDetailsTile(
                                                context: context,
                                                productModel: historyVM
                                                    .orderDetailData
                                                    .value[index]);
                                          },
                                        )
                                      : Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Center(
                                            child: AppText.customText(
                                                title: "No Product in Order"),
                                          ),
                                        ));
                                }),
                            SizedBox(
                              height: 16,
                            ),
                            Row(
                              children: [
                                AppText.customText(
                                    title: 'Paid Online to Arwa',
                                    fontWeight: FontWeights.semi_bold,
                                    size: 16),
                                SizedBox(
                                  width: 8,
                                ),
                                Image(
                                  image: AssetImage(
                                      'assets/paymentAndAddress/Icon_MasterCard.png'),
                                  height: 30,
                                  // width: 20,
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Image(
                                  image: AssetImage(
                                      'assets/paymentAndAddress/Icon_Visa.png'),
                                  height: 34,
                                  // width: 20,
                                  fit: BoxFit.contain,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Obx(
                              () => paymentText(
                                  context: context,
                                  title: "Sub Total",
                                  pay:
                                      "AED ${historyVM.totalPrice.toString()}"),
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            paymentText(
                                context: context,
                                title: "Delivery Fee",
                                pay: "AED 0.0"),
                            SizedBox(
                              height: 17,
                            ),
                            SizedBox(
                              width: AppConfig(context).width / 1.1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  AppText.customText(
                                      title: "Discount",
                                      fontWeight: FontWeights.medium,
                                      size: 14,
                                      color: AppColor.greyText),
                                  Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xFFF2F9FF),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 5, 10, 5),
                                      child: Row(
                                        children: [
                                          AppText.customText(
                                              title: "-",
                                              fontWeight: FontWeights.semi_bold,
                                              size: 14,
                                              color: AppColor.primaryColor),
                                          Obx(
                                            () => AppText.customText(
                                                title:
                                                    "AED ${historyVM.totalDiscount.toString()}",
                                                fontWeight:
                                                    FontWeights.semi_bold,
                                                size: 14,
                                                color: AppColor.primaryColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            SizedBox(
                              width: AppConfig(context).width / 1.1,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    children: [
                                      AppText.customText(
                                          title: "Total",
                                          fontWeight: FontWeights.semi_bold,
                                          size: 16),
                                      AppText.customText(
                                          title: "(incl.VAT)",
                                          fontWeight: FontWeights.medium,
                                          size: 12,
                                          color: AppColor.greyText),
                                    ],
                                  ),
                                  Obx(
                                    () => AppText.customText(
                                        title:
                                            "AED ${historyVM.totalPrice.value - historyVM.totalDiscount.value}",
                                        fontWeight: FontWeights.semi_bold,
                                        size: 16),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10, top: 37, bottom: 30),
                              child: primaryButton(
                                  width: AppConfig(context).width - 40,
                                  buttonTitle: "Cancel Order",
                                  callback: () {},
                                  btnColor: AppColor.red,
                                  txtColor: AppColor.white),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )),
        ));
  }
}
