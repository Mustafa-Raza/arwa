import 'package:arwa/AppModules/HistoryModule/Model/HistoryModel.dart';
import 'package:arwa/AppModules/HistoryModule/View/ProductTileView.dart';
import 'package:arwa/AppModules/HistoryModule/ViewController/OrderHistoryDetailsViewController.dart';
import 'package:arwa/AppModules/HistoryModule/ViewModel/history_view_model.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/MyFavouriteModule/View/favoruite_shimmer_view.dart';
import 'package:arwa/AppModules/ProductModule/View/products_shimer_view.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:group_list_view/group_list_view.dart';

class OrderHistoryViewController extends StatefulWidget {
  OrderHistoryViewController({Key? key}) : super(key: key);

  @override
  State<OrderHistoryViewController> createState() =>
      _OrderHistoryViewControllerState();
}

class _OrderHistoryViewControllerState
    extends State<OrderHistoryViewController> {
  final Map<String, List<HistoryModel>> _elements = {"1": []};
  final historyVM = Get.put(HistoryViewModel());
  Future<List<Map<String, List<HistoryModel>>>>? getHistory;
  final categoryController = Get.put(HomeCategoryController());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getHistory = historyVM.getHistoryData(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse("0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              height: AppConfig(context).height,
              width: AppConfig(context).width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: InnerAppBar(
                      title: "Order History",
                    ),
                  ),
                  // SizedBox(
                  //   height: 10,
                  // ),
                  Expanded(
                    child: FutureBuilder<List<Map<String, List<HistoryModel>>>>(
                        future: getHistory,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Center(
                                child: FavShimmerView(),
                              ),
                            );
                          }

                          return Obx(
                            () => historyVM.historyData.value.isNotEmpty?GroupListView(
                              sectionsCount: historyVM.historyData.value.length,
                              countOfItemInSection: (int section) {
                                return historyVM
                                    .historyData.value[section].values
                                    .toList()[0]
                                    .length;
                              },

                              itemBuilder:
                                  (BuildContext context, IndexPath index) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: productTileView(
                                      context: context,
                                      historyModel: historyVM.historyData
                                          .value[index.section].values
                                          .toList()[0][index.index],
                                      callback: () {
                                        Get.to(
                                            OrderHistoryDetailsViewController(
                                          historyModel: historyVM.historyData
                                              .value[index.section].values
                                              .toList()[0][index.index],
                                        ));
                                      }),
                                );
                              },
                              groupHeaderBuilder:
                                  (BuildContext context, int section) {
                                return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                        top: 5,
                                        bottom: 20),
                                    child: AppText.customText(
                                        title: historyVM
                                            .historyData.value[section].keys
                                            .toList()[0],
                                        color: AppColor.textGREY,
                                        fontWeight: FontWeights.regular,
                                        size: 14));
                              },
                              // separatorBuilder: (context, index) => NotificationTile(),
                              // sectionSeparatorBuilder: (context, section) => SizedBox(height: 10),
                            ):Center(child:  AppText.customText(
                          title: "No History Found",
                              color: AppColor.primaryColor,
                              size: 18),)
                          );
                        }),

                    ///
                    // Obx(()=>   ListView.builder(
                    //      itemCount: historyVM.historyData.value.length,
                    //      itemBuilder: (context, index) {
                    //        return OrderHistoryListing(orderData: historyVM.historyData.value[index],);
                    //      },
                    //    )
                    //    )
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // OrderTileImagesList(),
                ],
              ),
            )));
  }
}
