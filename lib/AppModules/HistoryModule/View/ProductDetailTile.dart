import 'package:arwa/AppModules/ProductModule/Model/ProductModel.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';

Widget productDetailsTile(
    {required BuildContext context, required ProductModel productModel}) {
  return Container(
    width: AppConfig(context).width / 1.1,
    height: AppConfig(context).height / 10,
    // color: Colors.orange,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 44,
          width: 44,
          decoration: BoxDecoration(
              color: AppColor.authFieldColor,
              borderRadius: BorderRadius.circular(6)),
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: cacheImageView(
                circlularPadding: 0,
                image: "${productModel.image}",
              )
              // Image(image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
              ),
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText.customText(
                  title:
                      '${productModel.productdesc} X ${productModel.packsize}',
                  fontWeight: FontWeights.medium,
                  overFlowText: TextOverflow.ellipsis,
                  size: 14),
              AppText.customText(
                  title:
                      '${productModel.cartQuantity} items x AED ${productModel.baseprice}',
                  fontWeight: FontWeights.regular,
                  size: 12,
                  color: AppColor.greyText),
            ],
          ),
        ),
        // Spacer(),
        AppText.customText(
            title: 'AED ${productModel.baseprice * productModel.cartQuantity}',
            fontWeight: FontWeights.medium,
            size: 14,
            color: AppColor.DarkText),
      ],
    ),
  );
}
