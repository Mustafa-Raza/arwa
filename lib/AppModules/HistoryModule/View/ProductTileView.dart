import 'package:arwa/AppModules/HistoryModule/Model/HistoryModel.dart';
import 'package:arwa/AppModules/HistoryModule/Model/OrderEnum.dart';
import 'package:arwa/AppModules/HistoryModule/View/Components/OrderTileImagesList.dart';
import 'package:arwa/AppModules/Utills/AppButtons/primary_button.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';

Widget productTileView({
  required BuildContext context,
required HistoryModel historyModel,
  required VoidCallback callback

}) {
  return GestureDetector(
    onTap: (){
      callback();
    },
    child: Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        height: 140,
        width: AppConfig(context).width - 40,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade300,
                blurRadius: 15,
                // offset: Offset(0, 4),
              )
            ]),
        child: Row(
          children: [
            SizedBox(
              width: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 15, top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppText.customText(
                      title:"OD-" +historyModel.orderid.toString()+"-N",
                      color: AppColor.DarkText,
                      size: 16,
                      fontWeight: FontWeights.medium),
                  SizedBox(
                    height: 5,
                  ),

                  AppText.customText(
                      title: "AED ${historyModel.grossamount}",
                      color: AppColor.primaryColor,
                      size: 16  ,
                      fontWeight: FontWeights.semi_bold),
                  Spacer(),
                  Container(
                    height: 30,
                    width: 90,
                    decoration: BoxDecoration(
                      color: Color(0xFF449FE6),
                      // color: historyModel.orderStatus==OrderStatus.Complete?Color(0xFF75C650):Color(0xFF449FE6),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Center(
                        child: AppText.customText(
                            title: historyModel.status,
                            // title: historyModel.orderStatus==OrderStatus.Complete?"Delivered":"In Progress",
                            color: AppColor.white,
                            size: 12,
                            fontWeight: FontWeights.medium)),
                  ),
                  SizedBox(
                    height: 20,
                  ),

                  // primaryButton(buttonTitle: "In progress", callback: (){},height: 35,width: 90,btnColor: AppColor.greenishText,txtColor:AppColor.white )
                ],
              ),
            ),
           Spacer(),
            OrderTileImagesList(images: historyModel.images),


            SizedBox(width: 10,)
          ],
        ),
      ),
    ),
  );
}
