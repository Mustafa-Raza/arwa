
import 'package:flutter/cupertino.dart';

Widget dateTextView({required String date}){
  return Text(date,style:TextStyle(
    fontFamily: 'Poppins',
    color:Color(0xFF999B9F) ,
    fontWeight: FontWeight.normal,
    fontSize: 14,
  ),);
}