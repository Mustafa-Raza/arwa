import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:arwa/AppModules/Utills/cache_image_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class OrderTileImagesList extends StatelessWidget {
 final List<String> images; OrderTileImagesList({Key? key,required this.images}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
     images.length==1? Container(
        height: AppConfig(context).height / 7,
        width: AppConfig(context).height / 7,
        decoration: BoxDecoration(
          // color: Colors.blue.shade50.withOpacity(0.5),
          color: Color(0xFFF9F9FC),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: cacheImageView(image: images[0],boxfit: BoxFit.contain)
          // Image(
          //   image: AssetImage('assets/home/brands/arwa.png'),
          //   height: 20,
          //   width: 20,
          //   fit: BoxFit.contain,
          // ),
        ),
      ):   images.length==2?
     coupleImages(context,images):  images.length==3?
        tripleImages(context,images):
        tetraImages(context,images)

    );
  }
}

coupleImages(BuildContext context,List<String>images){
  return Row(
    children: [
      Container(
        height: AppConfig(context).height / 7,
        width: AppConfig(context).height / 14,
        decoration: BoxDecoration(
          // color: Colors.blue.shade50.withOpacity(0.5),
          color: Color(0xFFF9F9FC),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child:  cacheImageView(image: images[0],boxfit: BoxFit.contain)
          // Image(
          //   image: AssetImage('assets/home/brands/arwa.png'),
          //   height: 20,
          //   width: 20,
          //   fit: BoxFit.contain,
          // ),
        ),
      ),
      SizedBox(width: 5,),
      Container(
        height: AppConfig(context).height / 7,
        width: AppConfig(context).height / 14,
        decoration: BoxDecoration(
          // color: Colors.blue.shade50.withOpacity(0.5),
          color: Color(0xFFF9F9FC),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child:  cacheImageView(image: images[1],boxfit: BoxFit.contain)
          // Image(
          //   image: AssetImage('assets/home/brands/arwa.png'),
          //   height: 20,
          //   width: 20,
          //   fit: BoxFit.contain,
          // ),
        ),
      ),
    ],
  );
}
tripleImages(BuildContext context,List<String>images){
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              // color: Colors.blue.shade50.withOpacity(0.5),
              color: Color(0xFFF9F9FC),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: cacheImageView(image: images[0],boxfit: BoxFit.contain)
              // Image(
              //   image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              // color: Colors.blue.shade50.withOpacity(0.5),
              color: Color(0xFFF9F9FC),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:  cacheImageView(image: images[1],boxfit: BoxFit.contain)
              // Image(
              //   image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
            ),
          ),
        ],
      ),
      SizedBox(width: 5,),
      Container(
        height: AppConfig(context).height / 7,
        width: AppConfig(context).height / 14,
        decoration: BoxDecoration(
          // color: Colors.blue.shade50.withOpacity(0.5),
          color: Color(0xFFF9F9FC),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child:  cacheImageView(image: images[2],boxfit: BoxFit.contain)
          // Image(
          //   image: AssetImage('assets/home/brands/arwa.png'),
          //   height: 20,
          //   width: 20,
          //   fit: BoxFit.contain,
          // ),
        ),
      ),
    ],
  );
}
tetraImages(BuildContext context,List<String>images){
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              // color: Colors.blue.shade50.withOpacity(0.5),
              color: Color(0xFFF9F9FC),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:  cacheImageView(image: images[0],boxfit: BoxFit.contain)
              // Image(
              //   image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              // color: Colors.blue.shade50.withOpacity(0.5),
              color: Color(0xFFF9F9FC),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:  cacheImageView(image: images[1],boxfit: BoxFit.contain)
              // Image(
              //   image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
            ),
          ),
        ],
      ),
      SizedBox(width: 5,),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              // color: Colors.blue.shade50.withOpacity(0.5),
              color: Color(0xFFF9F9FC),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: cacheImageView(image: images[2],boxfit: BoxFit.contain)
              // Image(
              //   image: AssetImage('assets/home/brands/arwa.png'),
              //   height: 20,
              //   width: 20,
              //   fit: BoxFit.contain,
              // ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: AppConfig(context).height / 14.5,
            width: AppConfig(context).height / 14,
            decoration: BoxDecoration(
              color: Colors.white,
              // color: Color(0xFFF9F9FC),
              border: Border.all(color: Color(0xFFE1E1E1)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(child: AppText.customText(title: "+${images.length-3}",color: AppColor.DarkText,size: 14,fontWeight: FontWeights.medium)),
          ),
        ],
      ),
    ],
  );
}