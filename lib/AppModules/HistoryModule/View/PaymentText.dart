
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';

Widget paymentText({required BuildContext context,required String title,required String pay}){
  return SizedBox(
    width: AppConfig(context).width/1.1,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        AppText.customText(title: title,fontWeight: FontWeights.medium,size: 14,color: AppColor.greyText),
        AppText.customText(title: pay,fontWeight: FontWeights.medium,size: 14),


      ],
    ),
  );
}