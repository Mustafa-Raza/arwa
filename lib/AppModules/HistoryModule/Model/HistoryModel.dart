import 'package:arwa/AppModules/HistoryModule/Model/OrderEnum.dart';

// class HistoryModel{
//   OrderStatus orderStatus;
//   String orderTitle;
//   String orderPrice;
//   List<String> images;
//   HistoryModel({required this.images,required this.orderPrice,required this.orderStatus,required this.orderTitle});
// }

class HistoryModel {
  HistoryModel({
    required this.id,
    required  this.orderid,
    required this.customerid,
    required  this.orderdate,
    required  this.deliverydate,
    required  this.status,
    required  this.placedon,
    required  this.grossamount,
    required  this.addressid,
    required  this.payment,
    required  this.notes,
    required  this.images,
  });

  int id;
  int orderid;
  String customerid;
  DateTime orderdate;
  DateTime deliverydate;
  String status;
  String placedon;
  double grossamount;
  int addressid;
  String payment;
  String notes;
  List<String> images;

  factory HistoryModel.fromJson(Map<String, dynamic> json) => HistoryModel(
    id: json["id"],
    orderid: json["orderid"],
    customerid: json["customerid"],
    orderdate: DateTime.parse(json["orderdate"]),
    deliverydate: DateTime.parse(json["deliverydate"]),
    status: json["status"],
    placedon: json["placedon"],
    grossamount: json["grossamount"],
    addressid: json["addressid"],
    payment: json["payment"]??"",
    notes: json["notes"]??"",
    images: List<String>.from(json["images"].map((x) => x)),
  );

  static List<HistoryModel> jsonToHistoryList(List<dynamic> emote) =>
      emote.map<HistoryModel>((item) => HistoryModel.fromJson(item)).toList();








  Map<String, dynamic> toJson() => {
    "id": id,
    "orderid": orderid,
    "customerid": customerid,
    "orderdate": "${orderdate.year.toString().padLeft(4, '0')}-${orderdate.month.toString().padLeft(2, '0')}-${orderdate.day.toString().padLeft(2, '0')}",
    "deliverydate": "${deliverydate.year.toString().padLeft(4, '0')}-${deliverydate.month.toString().padLeft(2, '0')}-${deliverydate.day.toString().padLeft(2, '0')}",
    "status": status,
    "placedon": placedon,
    "grossamount": grossamount,
    "addressid": addressid,
    "payment": payment,
    "notes": notes,
    "images": List<dynamic>.from(images.map((x) => x)),
  };
}
