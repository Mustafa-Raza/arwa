import 'package:arwa/AppModules/ChangePasswordModule/ViewController/ChnagePasswordViewController.dart';
import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/SettingModule/View/SettingTile.dart';
import 'package:arwa/AppModules/SettingModule/ViewController/AboutUsViewController.dart';
import 'package:arwa/AppModules/SettingModule/ViewController/PrivacyPolicyViewController.dart';
import 'package:arwa/AppModules/SettingModule/ViewController/ReturnPolicyViewController.dart';
import 'package:arwa/AppModules/SettingModule/ViewController/TermsAndConditionView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';

class SettingViewController extends StatefulWidget {
  const SettingViewController({Key? key}) : super(key: key);

  @override
  _SettingViewControllerState createState() => _SettingViewControllerState();
}

class _SettingViewControllerState extends State<SettingViewController> {
  final categoryController = Get.put(HomeCategoryController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    InnerAppBar(title: "Settings"),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 39,
                            ),
                            SettingViewTile(
                              callback: () {
                                Get.to(() => ChangePasswordViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              title: "Change Password",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SettingViewTile(
                              checkColor:  Color(int.parse(
                                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                              callback: () {},
                              title: "Subscribe to Our Product Emails?",
                              isCheckBox: true,
                              callCheckBox: (value) {
                                print(value);
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SettingViewTile(
                              callback: () {
                                Get.to(() => AboutUsViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              title: "About Arwa",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SettingViewTile(
                              callback: () {
                                Get.to(() => TermsAndConditionsView(),
                                    transition: Transition.rightToLeft);
                              },
                              title: "Terms and Policies",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SettingViewTile(
                              callback: () {
                                Get.to(() => PrivacyPolicyViewController(),
                                    transition: Transition.rightToLeft);
                              },
                              title: "Privacy Policy",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SettingViewTile(
                              callback: () {
                                Get.to(() => ReturnPolicyView(),
                                    transition: Transition.rightToLeft);
                              },
                              title: "Return Policy",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
