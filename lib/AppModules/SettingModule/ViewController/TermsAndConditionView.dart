import 'package:arwa/AppModules/HomeModule/ViewModel/HomeCategoryController.dart';
import 'package:arwa/AppModules/Utills/AppBar/InnerAppBar.dart';
import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TermsAndConditionsView extends StatefulWidget {
  const TermsAndConditionsView({Key? key}) : super(key: key);

  @override
  _TermsAndConditionsViewState createState() => _TermsAndConditionsViewState();
}

class _TermsAndConditionsViewState extends State<TermsAndConditionsView> {

  final categoryController = Get.put(HomeCategoryController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(int.parse(
            "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
        body: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            ),
            width: AppConfig(context).width,
            height: AppConfig(context).height,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                children: [
                  InnerAppBar(title: "Terms and Policies"),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 35,
                          ),
                          AppText.customText(
                              title: "Terms and Conditions",
                              color: Color(int.parse(
                                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                              size: 16,
                              fontWeight: FontWeights.semi_bold),
                          SizedBox(
                            height: 20,
                          ),
                          AppText.customText(
                              title:
                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                              color: AppColor.textGREY,
                              size: 12,
                              fontWeight: FontWeights.regular),
                          SizedBox(
                            height: 20,
                          ),
                          AppText.customText(
                              title: "Our Vision",
                              color: Color(int.parse(
                                  "0xFF${categoryController.selectedBrand.value.colorcode.split("#")[1]}")),
                              size: 16,
                              fontWeight: FontWeights.semi_bold),
                          SizedBox(
                            height: 20,
                          ),
                          AppText.customText(
                              title:
                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                              color: AppColor.textGREY,
                              size: 12,
                              fontWeight: FontWeights.regular),
                          SizedBox(
                            height: 20,
                          ),
                          AppText.customText(
                              title:
                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                              color: AppColor.textGREY,
                              size: 12,
                              fontWeight: FontWeights.regular),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
