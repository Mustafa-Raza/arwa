import 'package:arwa/AppModules/Utills/AppText/AppTextView.dart';
import 'package:arwa/AppModules/Utills/Constants/app_color.dart';
import 'package:arwa/AppModules/Utills/Constants/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class SettingViewTile extends StatefulWidget {
  final String title;
  final VoidCallback? callback;
  final Function? callCheckBox;
  final bool isCheckBox;
  final Color? checkColor;

  SettingViewTile(
      {Key? key,
      required this.title,
      this.callback,
      this.checkColor ,
      this.isCheckBox = false,
      this.callCheckBox})
      : super(key: key);

  @override
  _SettingViewTileState createState() => _SettingViewTileState();
}

class _SettingViewTileState extends State<SettingViewTile> {
  bool checkBoxValue = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.callback!();
      },
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.shade200, blurRadius: 15,
                  offset: Offset(3, 8),
                  // color: Color(0xFF0000001A),blurRadius: 15,offset: Offset(0,8),
                ),
              ]),
          width: AppConfig(context).width,
          height: 53,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                AppText.customText(
                    title: widget.title,
                    fontWeight: FontWeights.medium,
                    size: 13,
                    color: AppColor.DarkText),
                Spacer(),
                widget.isCheckBox
                    ?
                Container(
                  child: FlutterSwitch(
                    width: 32.0,
                    activeColor:  widget.checkColor!,
                    height: 21.0,

                    toggleSize: 16.0,
                    value: checkBoxValue,
                    borderRadius: 14.0,
                    padding: 2.5,
                    showOnOff: false,
                    onToggle: (val) {
                      setState(() {
                        checkBoxValue = val;
                      });
                      widget.callCheckBox!(val);
                    },
                  ),
                )
                // Container(
                //   child: FlutterSwitch(
                //     width: 40.0,
                //     activeColor:  widget.checkColor,
                //     height: 20.0,
                //
                //     toggleSize: 30.0,
                //     value: checkBoxValue,
                //     borderRadius: 15.0,
                //     padding: 1.0,
                //     showOnOff: false,
                //     onToggle: (val) {
                //       setState(() {
                //         checkBoxValue = val;
                //       });
                //       widget.callCheckBox!(val);
                //     },
                //   ),
                // )

                    : Icon(
                        Icons.arrow_forward_rounded,
                        color: Color(0xFF999B9F),
                        size: 20,
                      ),
              ],
            ),
          )),
    );
  }
}
